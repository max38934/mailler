FROM python:3.9-slim AS build

WORKDIR /opt/app
# Install Python and external dependencies, including headers and GCC
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3-dev \
    libpq-dev \
    musl-dev \
    gcc \
    libssl-dev \
    libffi-dev \
    default-libmysqlclient-dev \
    && rm -rf /var/lib/apt/lists/*

# Create a virtual environment and activate it
RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH" VIRTUAL_ENV="/opt/venv"

# Install dependencies into the virtual environment with Pipenv
# project dependencies
COPY requirements.txt requirements.txt
COPY requirements-dev.txt requirements-dev.txt

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir wheel && \
    pip install --no-cache-dir -r requirements-dev.txt

FROM python:3.9-slim

WORKDIR /opt/app

# dovecot - # https://command-not-found.com/doveadm
RUN apt-get update && apt-get install -y --no-install-recommends \
    default-libmysqlclient-dev \
    libtiff5-dev \
    libjpeg62-turbo-dev \
    libopenjp2-7-dev \
    zlib1g-dev \
    libfreetype6-dev \
    liblcms2-dev \
    libwebp-dev \
    tcl8.6-dev \
    tk8.6-dev \
    python3-tk \
    libharfbuzz-dev \
    gettext \
    libgettextpo-dev \
    libfribidi-dev && \
    rm -rf /var/lib/apt/lists/*

# Copy the virtual environment from the previous image
COPY --from=build /opt/venv /opt/venv

# Activate the virtual environment
ENV PATH="/opt/venv/bin:$PATH" VIRTUAL_ENV="/opt/venv" HOME=/home/ubuntu
ENV PROJECT_ROOT $HOME/projects/mailler
ENV PYTHONPATH $PYTHONPATH:$PROJECT_ROOT/src/:/usr/local
ENV PYTHONDONTWRITEBYTECODE=1 PYTHONUNBUFFERED=1
# keep this variable to load celery from app directory
ENV PYTHONPATH "${PYTHONPATH}:${PROJECT_ROOT}/src"

WORKDIR $PROJECT_ROOT
COPY ./src $PROJECT_ROOT/src

RUN mkdir -p $HOME \
    && mkdir -p $PROJECT_ROOT/logs \
    && mkdir $PROJECT_ROOT/run
