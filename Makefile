SHELL := /bin/bash

docker_run := docker-compose run --rm
docker_backend := $(docker_run) backend
manage_py := $(docker_backend) python ./src/manage.py

build-dev:
	cp -n .env.example .env && docker-compose up -d --build

makemessages:
	$(manage_py) makemessages \
		-l es \
		-l nl \
		-l de \
		-l fr \
		-l it \
		-l pl \
		-l pt \
		-l ru \
		-l sr \
		-l tr \
		-l uk \
		-l ar \
		-l zh \
		-l cs \
		-l da \
		-l fi \
		-l el \
		-l hu \
		-l id \
		-l ja \
		-l ko \
		-l no \
		-l fa \
		-l ro \
		-l sv \
		-l th \
		-l vi \
		-l hi

compilemessages:
	$(manage_py) compilemessages

migrate:
	$(manage_py) migrate

makemigrations:
	$(manage_py) makemigrations

shell_plus:
	$(manage_py) shell_plus --print-sql

show_urls:
	$(manage_py) show_urls

translate:
	$(manage_py) translate

createsuperuser:
	$(manage_py) createsuperuser

full_translate: makemessages \
	translate \
	compilemessages

test_data:
	$(manage_py) create_emails && $(manage_py) create_mails

logs_backend:
	docker logs -f --tail 200 mailler-backend

logs_celery:
	docker logs -f --tail 200 mailler-celery

logs_celerybeat:
	docker logs -f --tail 200 mailler-celerybeat

build: build-dev migrate
