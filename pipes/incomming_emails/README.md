### install
`sudo pip3 install -r requirements.txt`

##### https://docs.iredmail.org/pipe.incoming.email.for.certain.user.to.external.script.html

##### https://thecodingmachine.io/triggering-a-php-script-when-your-postfix-server-receives-a-mail


#### path

1. scripts and env is stored in `/var/pipes/`
2. Don't forget to `sudo chmod +rx -R /var/pipes/*`
3. Check logs `sudo tail -n 100 -f /var/log/mail.log`
4. No need to reload postfix if you just update script in `/var/pipes/`
