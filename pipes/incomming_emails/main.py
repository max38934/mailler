#! /usr/bin/python3

import os
import sys

import psycopg2
from dotenv import load_dotenv
from datetime import datetime

from contextlib import closing
import email
import uuid

#######################  # CONNECTION
load_dotenv()

DJANGO_POSTGRES_USER = os.getenv('DJANGO_POSTGRES_USER')
DJANGO_POSTGRES_DB = os.getenv('DJANGO_POSTGRES_DB')
DJANGO_POSTGRES_PASSWORD = os.getenv('DJANGO_POSTGRES_PASSWORD')
DJANGO_POSTGRES_HOST = os.getenv('DJANGO_POSTGRES_HOST')
DJANGO_POSTGRES_PORT = os.getenv('DJANGO_POSTGRES_PORT', default='5432')

connection_params = {
    'dbname': DJANGO_POSTGRES_DB,
    'user': DJANGO_POSTGRES_USER,
    'password': DJANGO_POSTGRES_PASSWORD,
    'host': DJANGO_POSTGRES_HOST,
    'port': DJANGO_POSTGRES_PORT,
}


######################## helpers

def parse_email_content():
    b = email.message_from_file(sys.stdin)

    body = b""
    subject = ''

    if b.is_multipart():
        target_content_type = 'text/html' \
            if any(part.get_content_type() == 'text/html' for part in b.walk()) else 'text/plain'

        for part in b.walk():
            ctype = part.get_content_type()
            cdispo = str(part.get('Content-Disposition'))

            if not subject and (sub := part.get('Subject')):
                subject = sub

            # skip any text/plain (txt) attachments
            if ctype == target_content_type and 'attachment' not in cdispo:
                body = part.get_payload(decode=True)  # decode
                break
    # not multipart - i.e. plain text, no attachments, keeping fingers crossed
    else:
        body = b.get_payload(decode=True)
        if not subject and (sub := b.get('Subject')):
            subject = sub

    return (
        subject.strip(),
        body.decode().strip().replace('\n', ''),
        b.as_string(),
    )


def write_to_db(to_email, from_email, subject, body, from_name, raw_content):
    with closing(psycopg2.connect(**connection_params)) as conn:
        with conn.cursor() as cursor:
            hash_ = str(uuid.uuid4())

            created = datetime.now()

            sql = """
                INSERT INTO mails_mails(to_email, from_email, subject, body_legacy, seen, hash, from_name, created, raw_content)
                VALUES(%s, %s, %s, %s, false, %s, %s, %s, %s) RETURNING id;
            """
            cursor.execute(sql, (to_email, from_email, subject, body, hash_, from_name, created, raw_content))
            conn.commit()


def main():
    subject, body, raw_content = parse_email_content()
    body = ''
    from_email = sys.argv[1]
    to_email = sys.argv[3]
    from_name = ''
    write_to_db(
        to_email=to_email,
        from_email=from_email,
        subject=subject,
        body=body,
        from_name=from_name,
        raw_content=raw_content,
    )


main()
