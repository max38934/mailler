-- show running queries (pre 9.2)
SELECT procpid, age(clock_timestamp(), query_start), usename, current_query
FROM pg_stat_activity
WHERE current_query != '<IDLE>' AND current_query NOT ILIKE '%pg_stat_activity%'
ORDER BY query_start desc;

-- show running queries (9.2)
SELECT pid, age(clock_timestamp(), query_start), usename, query
FROM pg_stat_activity
WHERE query != '<IDLE>' AND query NOT ILIKE '%pg_stat_activity%'
ORDER BY query_start desc;

-- kill running query
SELECT pg_cancel_backend(procpid);

-- kill idle query
SELECT pg_terminate_backend(procpid);

-- vacuum command
VACUUM (VERBOSE, ANALYZE);

-- all database users
select * from pg_stat_activity where current_query not like '<%';

-- all databases and their sizes
SELECT
    pg_database.datname,
    pg_size_pretty(pg_database_size(pg_database.datname)) AS size
    FROM pg_database;

-- all databases size
select datname, pg_size_pretty(pg_database_size(datname))
from pg_database
order by pg_database_size(datname) desc;

-- cache hit rates (should not be less than 0.99)
SELECT sum(heap_blks_read) as heap_read, sum(heap_blks_hit)  as heap_hit, (sum(heap_blks_hit) - sum(heap_blks_read)) / sum(heap_blks_hit) as ratio
FROM pg_statio_user_tables;

-- table index usage rates (should not be less than 0.99)
SELECT relname, 100 * idx_scan / (seq_scan + idx_scan) percent_of_times_index_used, n_live_tup rows_in_table
FROM pg_stat_user_tables
ORDER BY n_live_tup DESC;

-- how many indexes are in cache
SELECT sum(idx_blks_read) as idx_read, sum(idx_blks_hit)  as idx_hit, (sum(idx_blks_hit) - sum(idx_blks_read)) / sum(idx_blks_hit) as ratio
FROM pg_statio_user_indexes;

-- disk size of a Postgres / PostgreSQL table and its indexes
SELECT
    table_name,
    pg_size_pretty(table_size) AS table_size,
    pg_size_pretty(indexes_size) AS indexes_size,
    pg_size_pretty(total_size) AS total_size
FROM (
    SELECT
        table_name,
        pg_table_size(table_name) AS table_size,
        pg_indexes_size(table_name) AS indexes_size,
        pg_total_relation_size(table_name) AS total_size
    FROM (
        SELECT ('"' || table_schema || '"."' || table_name || '"') AS table_name
        FROM information_schema.tables
    ) AS all_tables
    ORDER BY total_size DESC
) AS pretty_sizes;

-- list of indexes
\di+ *mailbox*

-- To get total size of all indexes attached to a table, you use the pg_indexes_size() function.
SELECT pg_size_pretty (pg_indexes_size('mails_mails'));

-- Dump database on remote host to file
$ pg_dump -U mailler mailler > mailler.sql
$ pg_dump -Z1 -Fc -U username -h hostname databasename > dump.sql
$ pg_dump -F t databasename > databasename.tar
$ pg_dump -U postgres mailler | gzip > mailler.gz

-- Import dump into existing database
$ psql -d newdb -f dump.sql
$ pg_restore -d mailler tecmintdb.tar
psql -d mailler < mailler.sql
zcat ./mailler.gz | psql -U mailler -W mailler

ln -s /mnt/volume-nbg1-2/postgresql/ /var/lib/

TRUNCATE mails_mails;

CREATE INDEX expired_department_idx ON mailbox (expired, department);
DROP INDEX IF EXISTS name
ALTER TABLE mails_mails ALTER COLUMN raw_content SET COMPRESSION pglz;

docker exec -it mailler-postgres psql -h postgres -U mailler
"public"."mailbox"                                           | 2115 MB    | 6326 MB      | 8441 MB
"public"."forwardings"                                       | 406 MB     | 1326 MB      | 1732 MB

