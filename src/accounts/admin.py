from django.contrib import admin

from accounts.models import User, Feedback, Contact


class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'site', 'date_joined', 'is_active')
    readonly_fields = ('username',)
    filter_horizontal = ('groups', 'user_permissions')
    ordering = ('-id',)
    search_fields = ('email', )


class FeedbackAdmin(admin.ModelAdmin):
    search_fields = ('user__email', 'user__first_name', 'user__last_name')
    list_select_related = ('user',)
    list_display = ('id', 'use_rating', 'recommend_rating', 'created', 'user_id')
    readonly_fields = ('user', 'use_rating', 'recommend_rating', 'improve', 'created', 'deleted')
    list_filter = ('use_rating', 'recommend_rating', 'created')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ContactAdmin(admin.ModelAdmin):
    search_fields = ('user__email', 'user__first_name', 'user__last_name', 'name', 'email', 'subject', 'message')
    list_select_related = ('user',)
    list_display = ('id', 'subject', 'name', 'email', 'created', 'user_id')
    readonly_fields = ('subject', 'message', 'email', 'created', 'name', 'email', 'user')
    list_filter = ('created',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(User, UserAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Contact, ContactAdmin)
