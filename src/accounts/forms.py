from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import mail_managers
from django.template.loader import render_to_string
from django_registration.forms import RegistrationForm, RegistrationFormUniqueEmail

from postfix.models import Mailbox
from utils import get_site_context

from accounts.models import Contact, Feedback, User

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field
from crispy_forms.layout import Layout, Row, Submit, Div
from django import forms
from django.utils.translation import gettext as _g

from mixins.fields.form_input import CrispyField


class ContactForm(forms.ModelForm):
    name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    subject = forms.CharField(required=True)
    message = forms.CharField(widget=forms.Textarea, required=True)

    class Meta:
        model = Contact
        fields = (
            'name',
            'email',
            'subject',
            'phone',
            'message',
        )

        required = (
            'name',
            'email',
            'message',
        )

        placeholders = {}

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

        self.helper = FormHelper()
        extra_context = {'wrapper_ccs_class': 'col-md-6 form-group g-mb-20'}
        extra_context2 = {'wrapper_ccs_class': 'col-md-12 form-group g-mb-20'}
        button_css = 'btn u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase g-rounded-25 g-py-15 g-px-30'
        field = 'form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover ' \
                'rounded-3 g-py-13 g-px-15'
        self.helper.layout = Layout(
            Row(
                CrispyField('name', template='forms/input.html', extra_context=extra_context, css_class=field),
                CrispyField('email', template='forms/input.html', extra_context=extra_context, css_class=field),
                CrispyField('subject', template='forms/input.html', extra_context=extra_context2, css_class=field),
                Field('message', template='forms/textarea.html', css_class=field),
            ),
            Div(Submit('submit', _g('Send Request'), css_class=button_css), css_class='text-center'),
        )

        for field in self.Meta.required:
            self.fields[field].required = True

        for field, value in self.Meta.placeholders.items():
            self.fields[field].widget.attrs['placeholder'] = value

    def send_email(self) -> None:
        # send email using the self.cleaned_data dictionary
        site = Site.objects.get_current()
        context = {
            'name': self.cleaned_data['name'],
            'email': self.cleaned_data['email'],
            'subject': self.cleaned_data['subject'],
            'message': self.cleaned_data['message'],
            'settings': settings,
            'title': f'{settings.BRAND_NAME} contact',
            **get_site_context(site),
        }
        template_text = 'mails/emails/contact/email.txt'
        template_html = 'mails/emails/contact/email.html'
        text_content = render_to_string(template_text, context)
        html_content = render_to_string(template_html, context)

        mail_managers(context['title'], text_content, html_message=html_content)


class FeedbackForm(forms.ModelForm):
    class Meta:
        fields = ('use_rating', 'recommend_rating', 'improve')
        model = Feedback

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request
        self.fields['use_rating'].widget = forms.HiddenInput()
        self.fields['recommend_rating'].widget = forms.HiddenInput()

        self.helper = FormHelper()
        field = 'form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover ' \
                'rounded-3 g-py-13 g-px-15'
        self.helper.layout = Layout(
            Row(Field('message', template='forms/textarea.html', css_class=field)),
        )

    def save(self, commit=True):
        super().save(commit=False)
        self.instance.user = self.request.user
        self.instance.save()
        return self.instance


class UserRegistrationForm(RegistrationFormUniqueEmail):
    class Meta(RegistrationForm.Meta):
        model = User
        fields = [
            User.USERNAME_FIELD,
            "password1",
            "password2",
        ]

    def clean_email(self):
        email = self.cleaned_data['email']

        if Mailbox.objects.filter(username__iexact=email).exists():
            raise forms.ValidationError(_g('Nice Try!'))

        return email


class ResendEmailForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email',)

    def __init__(self, *args, **kwargs):
        self.user = None
        super().__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        self.user = get_object_or_None(
            User,
            site_id=settings.SITE_ID,
            email=email,
        )

        if not self.user:
            raise forms.ValidationError('No such user.')
        elif self.user.is_active:
            raise forms.ValidationError('You are already signed up!')

        return email
