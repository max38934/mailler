
from django.utils.translation import ugettext_lazy as _ul
from django.contrib.auth.models import AbstractUser
from django.contrib.sites.models import Site
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from mixins.models import CreatedModel, CreatedDeleteModel


class User(AbstractUser):

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'site_id']

    site = models.ForeignKey(Site, on_delete=models.CASCADE)
    stripe_customer_id = models.CharField(max_length=128, blank=True, db_index=True, default='')

    class Meta:
        unique_together = ('email', 'site')

    def __str__(self):
        return f'({self.pk}) {self.email}'

    def __repr__(self):
        return self.__str__()


class Feedback(CreatedDeleteModel):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    use_rating = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)],
        verbose_name=_ul('How easy was this to use?'))
    recommend_rating = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)],
        verbose_name=_ul('How likely are you to recommend our service to your friends?'))
    improve = models.TextField(verbose_name=_ul('What can we improve?'))


class Contact(CreatedModel):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=256, null=True, blank=True)
    phone = models.CharField(max_length=22, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    subject = models.CharField(max_length=256)
    message = models.TextField()
