import uuid
from django.conf import settings
from django.db.models.signals import pre_save
from django.dispatch import receiver


from accounts.models import User


@receiver(pre_save, sender=User, dispatch_uid='user_pre_save')
def user_pre_save(sender, instance, *args, **kwargs):
    if not instance.id:
        instance.username = str(uuid.uuid4()).replace('-', '')
        instance.site_id = settings.SITE_ID
        instance.is_active = getattr(instance, 'is_active', False)  # to be able create active users immediately
    instance.email = instance.email.lower()
