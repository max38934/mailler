$(document).on('ready', function () {
    $('#create-email-form').submit(function(e) {
            e.preventDefault();
            let form = $(this);
            let email = $('input[name="email_value"]').val() + "@" + $('select[name="domain"]').val();
            const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;

            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType: "json",
                data: {
                    'email': email,
                    'csrfmiddlewaretoken': csrftoken,
                },
                success: function(data) {
                    location.reload();
                },
                error: function (data) {
                    console.log(data.responseJSON);
                    $.each(data.responseJSON, function (field, errors) {
                        $(errors).each(function (index) {
                            toastr.error(errors[index]);
                        });
                    });
                }
            });
        });
})
