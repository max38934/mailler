$(document).on('ready', function () {
    let mails_container = $('#mails_container');
    const last_email_id_key = 'last-email-id';
    const data_handler = $('#data-handler');

    function longPoll() {
        let url = $(this).attr('href');

        $.ajax({
            url: url,
            type: "GET",
            dataType: "html",
            success: function (data) {

                var current_page = $('.paginator-js').data("current-page");

                if (current_page === 1 || current_page === undefined) {
                    mails_container.html(data);
                }

                // refresh_btn.removeClass('disabled');
            },
            error: function (err) {
                // do whatever you want when error occurs
            }
        });
    }

    function longPollLasID() {
        $.ajax({
            url: '/api/v1/mails/email/last/',
            type: "GET",
            dataType: "json",
            success: function (data) {
                var last_email_id = data['id'];

                if (last_email_id != localStorage.getItem(last_email_id_key)){
                    toastr.success('You Have New email!');
                    localStorage.setItem(
                        last_email_id_key,
                        last_email_id
                    );
                }

                // refresh_btn.removeClass('disabled');
            },
            error: function (err) {
                // do whatever you want when error occurs
            }
        });
    }

    longPoll();
    longPollLasID();
    window.setInterval(function () {
        longPoll();
        longPollLasID();
    }, 5000);

    $('body').on('click', '.paginator a', function (e) {
        e.preventDefault();
        let url = $(this).attr('href');

        $.ajax({
            url: url,
            type: 'get',
            success: function (response) {
                mails_container.html(response);
            }
        });
    })

    // refresh_btn.click(function () {
    //     if (!refresh_btn.hasClass('disabled')) {
    //         refresh_btn.addClass('disabled');
    //         longPoll();
    //     }
    // });
})
