from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import mail_managers
from django.template.loader import render_to_string

from utils import get_site_context
from settings.celeryapp import app


@app.task
def send_contact_email(name: str, email: str, subject: str, message: str) -> None:
    site = Site.objects.get_current()
    context = {
        'name': name,
        'email': email,
        'subject': subject,
        'message': message,
        'settings': settings,
        'title': 'Healthy contact',
        **get_site_context(site),
    }
    template_text = 'pages/emails/contact/email.txt'
    template_html = 'pages/emails/contact/email.html'
    text_content = render_to_string(template_text, context)
    html_content = render_to_string(template_html, context)

    mail_managers(context['title'], text_content, html_message=html_content)
