from django.conf import settings
from django.urls import path
from django.views.decorators.cache import cache_page as cp
from django.views.generic import TemplateView

from accounts import views


CACHE_TIMEOUT = -1 if settings.DEBUG else 60 * 60 * 2

app_name = 'accounts'

urlpatterns = [
    path('register/', views.RegistrationView.as_view(), name='django_registration_register'),
    path('contact/', views.ContactView.as_view(), name='contact'),
    path('feedback/', views.FeedbackView.as_view(), name='feedback'),
    # path('pricing/', cp(CACHE_TIMEOUT)(views.PricingView.as_view()), name='pricing'),
    # path('features/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name='accounts/features.html')), name='features'),

    # static pages
    path('cookies-policy/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name="accounts/cookies_policy.html")),
         name='cookies_policy'),
    path('terms-of-use/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name="accounts/terms_of_use.html")),
         name='terms'),
    path('privacy-policy/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name="accounts/privacy_policy.html")),
         name='privacy'),

    path('faq/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name="accounts/faq.html")),
         name='faq'),

    # registration
    path('registration/resend-email/', views.ResendActivationEmail.as_view(), name='activation_resend_custom'),
    path('registration/confirm/email/<str:activation_key>/', views.ConfirmEmailView.as_view(), name='profile-confirm-email'),

    # account features
    # path('emails/list/', views.EmailsList.as_view(), name='emails-list'),
]
