from django.urls import path
from django.views.decorators.cache import cache_page as cp
from django.views.generic import TemplateView


CACHE_TIMEOUT = 60 * 60 * 2

app_name = 'accounts'

urlpatterns = [
    # path('contact/', views.ContactView.as_view(), name='contact'),
    # path('feedback/', views.FeedbackView.as_view(), name='feedback'),
    # path('features/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name='accounts/features.html')), name='features'),

    # static pages
    path('cookies-policy/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name="accounts/cookies_policy.html")),
         name='cookies_policy'),
    path('terms-of-use/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name="accounts/terms_of_use.html")),
         name='terms'),
    path('privacy-policy/', cp(CACHE_TIMEOUT)(TemplateView.as_view(template_name="accounts/privacy_policy.html")),
         name='privacy'),
]
