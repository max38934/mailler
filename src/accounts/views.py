import urllib.parse

from annoying.functions import get_object_or_None
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.conf import settings
from django.core import signing
from django.core.cache import cache
from django.template.loader import render_to_string
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.views.generic import CreateView, TemplateView

from django_registration.backends.activation.views import RegistrationView as RV
from django_registration.exceptions import ActivationError

from accounts.forms import ContactForm, FeedbackForm, UserRegistrationForm, ResendEmailForm
from accounts.models import Feedback, Contact
from utils import get_site_context

from django.utils.translation import gettext_lazy as _, gettext as _g


REGISTRATION_SALT = getattr(settings, "REGISTRATION_SALT", "registration")


class ContactView(CreateView):
    """
    This view class is for registered users
    """
    form_class = ContactForm
    model = Contact
    success_url = reverse_lazy('mails:index')

    def form_valid(self, form):
        form.send_email()
        messages.add_message(self.request, messages.SUCCESS, _g('Thank you for contacting us!'))
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs


class FeedbackView(LoginRequiredMixin, CreateView):
    model = Feedback
    form_class = FeedbackForm
    success_url = reverse_lazy('mails:index')
    template_name = 'accounts/feedback.html'

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'The form was sent successfully.')
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs


class RegistrationView(RV):
    form_class = UserRegistrationForm
    email_body_template = "django_registration/activation_email_body"

    def get_email_context(self, *args, **kwargs):
        c = super().get_email_context(*args, **kwargs)

        path = reverse("django_registration_activate", args=(c['activation_key'],))
        domain = c['site'].domain
        scheme = c['scheme']
        c['activation_link'] = f'{scheme}://{domain}{path}'
        c.update(get_site_context(c['site']))

        return c

    def get_success_url(self, user=None):
        if user:
            self.request.session['email_registration'] = urllib.parse.quote(user.email)
        return super().get_success_url(user)

    def send_activation_email(self, user, extra_context=None):
        """
        Send the activation email. The activation key is the username,
        signed using TimestampSigner.

        """
        if extra_context is None:
            extra_context = {}

        activation_key = self.get_activation_key(user)
        context = self.get_email_context(activation_key)
        context["user"] = user

        context.update(extra_context)

        subject = render_to_string(
            template_name=self.email_subject_template,
            context=context,
            request=self.request,
        )
        # Force subject to a single line to avoid header-injection
        # issues.
        subject = "".join(subject.splitlines())
        text_content = render_to_string(f'{self.email_body_template}.txt', context)
        html_content = render_to_string(f'{self.email_body_template}.html', context)

        user.email_user(subject, text_content, settings.DEFAULT_FROM_EMAIL, html_message=html_content)


class ConfirmEmailView(TemplateView):
    ALREADY_ACTIVATED_MESSAGE = _("The account you tried to activate has already been activated.")
    BAD_USERNAME_MESSAGE = _("The account you attempted to activate is invalid.")
    EXPIRED_MESSAGE = _("This account has expired.")
    INVALID_KEY_MESSAGE = _("The activation key you provided is invalid.")

    success_url = reverse_lazy("accounts:profile_overall")
    template_name = 'django_registration/activation_failed.html'

    def get(self, *args, **kwargs):
        """
        The base activation logic; subclasses should leave this method
        alone and implement activate(), which is called from this
        method.

        """
        extra_context = {}
        try:
            self.activate(*args, **kwargs)
        except ActivationError as e:
            if e.code == 'already_activated':
                return redirect('login')
            extra_context["activation_error"] = {
                "message": e.message,
                "code": e.code,
                "params": e.params,
            }
        else:
            return HttpResponseRedirect(self.success_url)

        context_data = self.get_context_data()
        context_data.update(extra_context)
        return self.render_to_response(context_data)

    def activate(self, *args, **kwargs):
        new_email = self.validate_key()
        user = self.get_user(new_email)
        user.email = new_email
        user.new_email = None
        user.save(update_fields=('email', 'new_email'))
        return user

    def validate_key(self):
        """
        Verify that the activation key is valid and within the
        permitted activation time window, returning the username if
        valid or raising ``ActivationError`` if not.
        """
        activation_key = self.kwargs.get("activation_key")

        try:
            new_email = signing.loads(
                activation_key,
                salt=REGISTRATION_SALT,
                max_age=settings.ACCOUNT_ACTIVATION_DAYS * 86400,
            )
            return new_email
        except signing.SignatureExpired:
            raise ActivationError(self.EXPIRED_MESSAGE, code="expired")
        except signing.BadSignature:
            raise ActivationError(
                self.INVALID_KEY_MESSAGE,
                code="invalid_key",
                params={"activation_key": activation_key},
            )

    def get_user(self, new_email):
        """
        Given the verified username, look up and return the
        corresponding user account if it exists, or raising
        ``ActivationError`` if it doesn't.
        """

        User = get_user_model()

        if User.objects.filter(new_email__isnull=True, email=new_email).exists():
            raise ActivationError(
                self.ALREADY_ACTIVATED_MESSAGE, code="already_activated"
            )
        user = get_object_or_None(User, new_email=new_email)

        if user is None:
            raise ActivationError(self.BAD_USERNAME_MESSAGE, code="bad_email")

        return user


class ResendActivationEmail(RegistrationView):
    form_class = ResendEmailForm
    template_name = "django_registration/registration_complete.html"
    success_url = reverse_lazy("django_registration_complete")

    def get(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        key = f'resend-email-{self.request.session["email_registration"]}'
        if key in cache:
            cache.incr(key)
        else:
            cache.set(key, 1, 60 * 60)

        if cache.get(key) >= 3:
            messages.add_message(self.request, messages.ERROR, _g('Max retries exceeded. Please try again in 1 hour!'))
            return self.form_invalid(form)

        messages.add_message(self.request, messages.SUCCESS, _g(f'Email was resent to %(email_data)s!') % {"email_data": self.data_email})
        return super().form_valid(form)

    @property
    def data_email(self):
        return urllib.parse.unquote(self.request.session['email_registration'])

    def get_form_kwargs(self):
        data = super().get_form_kwargs()
        data['data'] = {'email': self.data_email}
        return data

    def create_inactive_user(self, form):
        self.send_activation_email(form.user)
        return form.user
