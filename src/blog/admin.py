
from django.contrib import admin

from blog.models import Entry


class EntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'status', 'site', 'pub_date')
    list_filter = ('status', 'site')
    readonly_fields = ('slug',)
    list_select_related = ('site',)
    ordering = ('-id',)


admin.site.register(Entry, EntryAdmin)
