# Generated by Django 3.2.2 on 2021-09-18 09:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(1, 'Published'), (2, 'Draft')], default=1)),
                ('title', models.CharField(max_length=250, unique=True)),
                ('slug', models.SlugField(max_length=250, unique=True)),
                ('template', models.FilePathField(match='blog/templates/blog/templates/*.html')),
                ('pub_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
