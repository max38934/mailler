
import ntpath

from django.conf import settings
from django.contrib.sites.models import Site
from django.db import models
from django.template.defaultfilters import slugify

from blog import choices as mch


def templates_path(template_path, site_id):
    file_name = ntpath.basename(template_path)
    return f'{settings.BASE_DIR}/blog/templates/blog/templates/{site_id}/{file_name}'


class Entry(models.Model):
    status = models.IntegerField(choices=mch.ENTRY_STATUS_CHOICES, default=mch.ENTRY_LIVE_STATUS)
    title = models.CharField(max_length=250, unique=True)
    slug = models.SlugField(max_length=250, unique=True)
    template = models.FilePathField(path=f'{settings.BASE_DIR}/blog/templates/blog/templates/1/')  # see save method
    pub_date = models.DateTimeField(auto_now_add=True)
    site = models.ForeignKey(Site, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def __repr__(self):
        return '<{0} {1}>'.format(self.__class__.__name__, self.id)

    def save(self, *args, **kwargs):
        self.template = templates_path(self.template, self.site_id)
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)
