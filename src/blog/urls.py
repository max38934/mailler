
from django.urls import path

from blog import views


app_name = 'blog'

urlpatterns = [
    # path(
    #     '',
    #     views.EntryList.as_view(),
    #     name='entries',
    # ),
    path(
        '<slug:slug>/',
        views.EntryDetail.as_view(),
        name='entry_detail',
    ),
    # path(
    #     'tag/<int:tag_id>/',
    #     views.EntryList.as_view(),
    #     name='entries_for_tag',
    # ),
]
