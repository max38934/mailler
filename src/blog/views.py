
from django.conf import settings
from django.views import generic

from blog.models import Entry


class BaseEntryView:
    def get_context_data(self, **kwargs):
        """
        Pass "popular entries" to the context.
        """

        context = super().get_context_data(**kwargs)
        # context['popular_entries'] = Entry.objects.filter(status=mch.ENTRY_LIVE_STATUS).order_by('-id')
        return context


class EntryDetail(BaseEntryView, generic.DetailView):
    queryset = Entry.objects.filter(site_id=settings.SITE_ID)
    template_name = 'blog/entry_detail.html'
