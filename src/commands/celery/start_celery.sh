#!/bin/sh

CELERY_PID_FILE="celery.pid"
rm "${CELERY_PID_FILE}"

celery \
    --app settings worker \
    --task-events \
    --loglevel=info \
    -Q mailler.normal \
    --autoscale=0,5 \
    --pidfile="${CELERY_PID_FILE}"
