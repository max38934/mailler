#!/bin/sh

CELERYBEAT_PID_FILE="/tmp/celerybeat.pid"
CELERYBEAT_SCHEDULE_FILE="/tmp/celerybeat-schedule"

rm "${CELERYBEAT_PID_FILE}" "${CELERYBEAT_SCHEDULE_FILE}"

celery -A settings beat \
    --loglevel=info \
    --schedule="${CELERYBEAT_SCHEDULE_FILE}" \
    --pidfile="${CELERYBEAT_PID_FILE}"
