#!/usr/bin/env bash
LOGS=`/usr/bin/docker logs --since 15m -t hms_celery_1 2>&1`
if [ "$LOGS" == "" ]
then
    /usr/bin/docker exec backend ./src/manage.py celery_alert
fi
