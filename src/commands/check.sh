#!/usr/bin/env bash

docker exec -it healthy-backend flake8 ./src &&
docker exec -it healthy-backend pytest ./src/tests -s -vv -x --cov=src --cov-report html --create-db &&
docker exec healthy-backend pip check &&
docker exec healthy-backend ./src/manage.py check &&
docker exec healthy-backend ./src/manage.py collectstatic --noinput --dry-run &&
docker exec healthy-backend ./src/manage.py makemigrations --check --dry-run &&
docker exec healthy-backend ./src/manage.py validate_templates &&
docker-compose -f docker-compose.yml config --quiet &&
docker exec healthy-nginx nginx -t
