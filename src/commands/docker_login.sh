#!/usr/bin/env bash
DIR=registry.hotelmanagementsolution.com:5000
su -c "mkdir -p /etc/docker/certs.d/${DIR}" root
su -c "cp registry.hotelmanagementsolution.com.crt /etc/docker/certs.d/${DIR}" root
docker login ${DIR}
