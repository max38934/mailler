#! /bin/sh

gunicorn -w $WSGI_WORKERS \
         -b 0.0.0.0:${WSGI_PORT:-8001} \
         --chdir $PROJECT_ROOT/src settings.wsgi \
         --timeout 30 \
         --log-level error \
         --max-requests 10000
