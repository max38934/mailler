
from django.contrib import admin

from mails.models import Mails


class MailsAdmin(admin.ModelAdmin):
    list_display = ('hash', 'to_email', 'from_email', 'subject', 'seen', 'created')
    ordering = ('-id',)
    list_filter = ('created', 'seen')
    search_fields = ('hash', 'to_email', 'from_email')


admin.site.register(Mails, MailsAdmin)
