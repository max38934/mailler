import django_filters

from mails.models import Mails, UserEmail


class MailsFilter(django_filters.FilterSet):

    class Meta:
        model = Mails
        fields = {
            'to_email': ['iexact', 'istartswith', 'iendswith', 'icontains'],
            'from_email': ['iexact', 'istartswith', 'iendswith', 'icontains'],
            'created': ['gte', 'lte'],
        }


class UserEmailFilter(django_filters.FilterSet):

    class Meta:
        model = UserEmail
        fields = {
            'email': ['iexact', 'istartswith', 'iendswith', 'icontains'],
        }
