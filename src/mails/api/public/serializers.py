from django.utils.timesince import timesince
from rest_framework import serializers
from rest_framework.fields import empty

from django.conf import settings

from mails.models import Mails, UserEmail
from postfix.models import Mailbox, Domain

from django.utils.translation import gettext_lazy as _


class MailSerializer(serializers.ModelSerializer):
    body = serializers.ReadOnlyField()

    class Meta:
        model = Mails
        fields = (
            'hash',
            'from_email',
            'from_name',
            'subject',
            'seen',
            'created',
            'body',
            'raw_content',
            'seen',
        )


class UserEmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserEmail
        fields = (
            'created',
            'email',
            'id',
        )
        extra_kwargs = {
            'id': {'read_only': True},
            'created': {'read_only': True},
        }

    def __init__(self, instance=None, data=empty, *args, **kwargs):
        super().__init__(instance, data, *args, **kwargs)
        self.request = kwargs['context']['request']

    def validate_email(self, attr):

        if Mailbox.objects.filter(username=attr).exists():
            raise serializers.ValidationError(
                _('Email Already exists.'),
                code='email_exists'
            )

        return attr

    def validate(self, attrs):
        count_emails = UserEmail.objects.filter(user=self.request.user).count()

        if count_emails >= settings.USER_MAXIMUM_EMAILS_ALLOWED:
            raise serializers.ValidationError(
                f'Maximum active emails cannot exceed {settings.USER_MAXIMUM_EMAILS_ALLOWED}.',
                code='email_count_exceed'
            )

        return attrs


class DomainSerializer(serializers.ModelSerializer):
    class Meta:
        model = Domain
        fields = (
            'domain',
            'created',
            'active',
        )

# class LastEmailSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Mails
#         fields = (
#             'id',
#         )
