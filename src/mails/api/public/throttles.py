from rest_framework.throttling import UserRateThrottle


class EmailsThrottle(UserRateThrottle):
    scope = 'mails'
    THROTTLE_RATES = '60/min'


class MailboxThrottle(UserRateThrottle):
    scope = 'mailboxes'
    THROTTLE_RATES = '60/min'
