from django.urls import path
from rest_framework.routers import DefaultRouter

from mails.api.public import views


app_name = 'mails_api_public'

router = DefaultRouter()

router.register('mails', views.EmailsViewSet, basename='mails')
router.register('mailboxes', views.MailboxesViewSet, basename='mailboxes')
router.register('domains', views.DomainsViewSet, basename='domains')

urlpatterns = [
    path('authorization/', views.AuthorizationExample.as_view(), name='authorization-example'),
]

urlpatterns += router.urls
