from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, generics
from rest_framework.permissions import IsAuthenticated
from oauth2_provider.contrib.rest_framework import OAuth2Authentication
from rest_framework.views import APIView

from mails.api.public.filters import MailsFilter, UserEmailFilter
from mails.api.public.paginators import EmailsPagination, UserEmailPagination, DomainPagination
from mails.api.public.serializers import MailSerializer, UserEmailSerializer, DomainSerializer
from mails.api.public.throttles import EmailsThrottle, MailboxThrottle
from mails.models import Mails, UserEmail
from postfix.models import Domain
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend


class EmailsViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'delete', 'head', 'options', 'trace']
    authentication_classes = (OAuth2Authentication, )
    permission_classes = (IsAuthenticated, )

    serializer_class = MailSerializer
    queryset = Mails.objects.all()
    lookup_url_kwarg = 'hash'
    lookup_field = 'hash'
    pagination_class = EmailsPagination
    filterset_class = MailsFilter
    search_fields = ['from_email', 'to_email', 'from_name', 'subject', 'raw_content']
    ordering_fields = ['from_email', 'to_email', 'from_name', 'subject']
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    throttle_classes = (EmailsThrottle, )

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(
            to_email__in=UserEmail.objects.filter(user=self.request.user).values_list('email', flat=True))
        return queryset

    def get_object(self):
        obj = super().get_object()

        if not obj.seen:
            obj.seen = True
            obj.save(update_field=('seen', ))

        return obj


class MailboxesViewSet(viewsets.ModelViewSet):
    authentication_classes = (OAuth2Authentication, )
    permission_classes = (IsAuthenticated, )
    http_method_names = ['get', 'delete', 'post', 'head', 'options', 'trace']
    serializer_class = UserEmailSerializer
    queryset = UserEmail.objects.all()
    pagination_class = UserEmailPagination
    filterset_class = UserEmailFilter
    search_fields = ['email']
    ordering_fields = ['email', 'id']
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    throttle_classes = (MailboxThrottle, )

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset


class DomainsViewSet(viewsets.ModelViewSet):
    authentication_classes = (OAuth2Authentication, )
    permission_classes = (IsAuthenticated, )
    http_method_names = ['get', 'head', 'options', 'trace']
    queryset = Domain.objects.all()
    pagination_class = DomainPagination
    serializer_class = DomainSerializer
    lookup_url_kwarg = 'domain'
    lookup_field = 'domain'

    @method_decorator(cache_page(60 * 60 * 24))  # 1 day
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class AuthorizationExample(APIView):
    @swagger_auto_schema(
        operation_description=f"""
        <p>
        To get your API keys <a target="_blank" href="/inboxes/api-keys/">click here</a>
        </p>
        <br>
        <code>$ curl -H "Authorization: Bearer {{YOUR_API_KEY}}" /api/v1/p/domains/</code>
        """
    )
    def get(self, request):
        return JsonResponse({})
