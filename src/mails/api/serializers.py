from django.utils.timesince import timesince
from rest_framework import serializers
from rest_framework.fields import empty

from django.conf import settings

from mails.models import Mails, UserEmail
from postfix.models import Mailbox

from django.utils.translation import gettext_lazy as _


class MailSerializer(serializers.ModelSerializer):
    created_ago = serializers.SerializerMethodField()

    class Meta:
        model = Mails
        fields = (
            'hash',
            'from_email',
            'from_name',
            'subject',
            'seen',
            'created_ago',
        )

    def get_created_ago(self, obj):
        return timesince(obj.created)


class CreateNewUserEmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserEmail
        fields = (
            'email',
        )

    def __init__(self, instance=None, data=empty, *args, **kwargs):
        super().__init__(instance, data, *args, **kwargs)
        self.request = kwargs['context']['request']

    def validate_email(self, attr):

        if Mailbox.objects.filter(username=attr).exists():
            raise serializers.ValidationError(
                _('Email Already exists.'),
                code='email_exists'
            )

        return attr

    def validate(self, attrs):
        count_emails = UserEmail.objects.filter(user=self.request.user).count()

        if count_emails >= settings.USER_MAXIMUM_EMAILS_ALLOWED:
            raise serializers.ValidationError(
                f'Maximum active emails cannot exceed {settings.USER_MAXIMUM_EMAILS_ALLOWED}.',
                code='email_count_exceed'
            )

        return attrs


class LastEmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mails
        fields = (
            'id',
        )
