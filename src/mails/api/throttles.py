
from rest_framework.throttling import AnonRateThrottle


class InboxRateThrottle(AnonRateThrottle):
    rate = '60/min'
    scope = 'inbox'
