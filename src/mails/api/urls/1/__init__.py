from django.urls import path
from rest_framework.routers import DefaultRouter

from mails.api import views


app_name = 'mails_api'

router = DefaultRouter()
router.register('', views.MailViewSet, basename='mails')

urlpatterns = [
    path('qr-code/', views.QRCodeView.as_view(), name='qr-code'),
    path('email/get/', views.MailGetView.as_view(), name='mail-get'),

    path('email/create/', views.CreateNewUserEmail.as_view(), name='create-email'),
    path('email/last/', views.LastEmailIDView.as_view(), name='last-email'),


]

urlpatterns += router.urls
