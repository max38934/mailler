from django.urls import path
from rest_framework.routers import DefaultRouter

from mails.api import views


app_name = 'mails_api'

router = DefaultRouter()
router.register('', views.MailViewSet, basename='mails')


urlpatterns = [
    path('qr-code/', views.QRCodeView.as_view(), name='qr-code'),
    path('inbox/', views.MailGetView.as_view(), name='mail-get'),
]

urlpatterns += router.urls
