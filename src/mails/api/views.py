import qrcode
import io
import urllib.parse
import base64
from datetime import timedelta

from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse, JsonResponse, Http404
from django.core.cache import cache
from django.conf import settings
from django.utils import timezone

from rest_framework import viewsets, views, generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from mails.models import Mails, UserEmail
from mails.api.serializers import MailSerializer, CreateNewUserEmailSerializer, LastEmailSerializer
from mails.utils import get_session_email


class MailPagination(PageNumberPagination):
    page_size = 20


class MailViewSet(viewsets.ModelViewSet):
    schema = None
    http_method_names = ['get', 'head', 'options', 'trace']
    serializer_class = MailSerializer
    queryset = Mails.objects.defer('body_legacy', 'raw_content').order_by('-id')
    pagination_class = MailPagination
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'mails/parts/inbox.html'
    throttle_scope = 'inbox'

    def get_queryset(self):
        # exclude old emails from inbox
        max_age = timedelta(seconds=settings.SESSION_COOKIE_AGE)
        now = timezone.now()
        till = now - max_age
        queryset = super().get_queryset().filter(created__gte=till)
        email_value = self.request.session.get('email')
        if email_value:
            return queryset.filter(to_email=email_value)
        return queryset.none()


class QRCodeView(views.View):

    def get(self, request):
        email_value = self.request.session.get('email')

        if not email_value:
            return HttpResponse('Bad Request', status=400)

        cache_key = f'QRCodeView::{email_value}'
        response_data = {
            'content': cache.get(cache_key),
        }

        if response_data['content']:
            return JsonResponse(response_data)

        query_params = urllib.parse.urlencode({'email': email_value})

        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_H,
            box_size=10,
            border=4,
        )

        site = get_current_site(request)

        qr.add_data(f'https://{site.domain}/?{query_params}')
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white").convert('RGB')

        with io.BytesIO() as output:
            img.save(output, format="JPEG")
            output.seek(0)
            content = output.read()
            content = base64.b64encode(content).decode()
            response_data['content'] = content

        cache.set(cache_key, content, settings.SESSION_COOKIE_AGE)  # 10 min

        return JsonResponse(response_data)


class MailGetView(views.View):

    def get(self, request):
        response_data = {
            'email': get_session_email(self.request)
        }
        return JsonResponse(response_data)


class CreateNewUserEmail(generics.CreateAPIView):
    schema = None
    serializer_class = CreateNewUserEmailSerializer
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class LastEmailIDView(generics.RetrieveAPIView):
    schema = None
    queryset = Mails.objects.all()
    serializer_class = LastEmailSerializer
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(
            to_email__in=UserEmail.objects.filter(user=self.request.user).values_list('email', flat=True))

        return queryset.order_by('-id')

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())

        obj = queryset.first()

        if not obj:
            raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)

        self.check_object_permissions(self.request, obj)

        return obj
