from django.apps import AppConfig
from django.conf import settings
from django.db import connection


class MailsConfig(AppConfig):
    name = 'mails'

    def ready(self):
        from django.contrib.sites.models import Site

        all_tables = connection.introspection.table_names()

        # check if table exists
        # table could be absent before initial migration
        if 'django_site' in all_tables:
            Site.objects.get_or_create(
                id=settings.SITE_ID,
                defaults={
                    "domain": f'domain-{settings.SITE_ID}',
                    "name": f'domain-{settings.SITE_ID}',
                },
            )
