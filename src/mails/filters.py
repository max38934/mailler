import django_filters

from mails.models import Mails


class UserMailsFilter(django_filters.FilterSet):

    class Meta:
        model = Mails
        fields = {
            'to_email': ['iexact'],
        }
