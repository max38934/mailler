from annoying.functions import get_object_or_None
from django import forms
from django.db.models.functions import Length

from postfix.models import Mailbox, Domain


class GetOrCreateMailboxForm(forms.ModelForm):
    target_email = forms.CharField(required=True, max_length=30)

    class Meta:
        model = Mailbox
        fields = (
            'target_email',
        )

    def clean_target_email(self):
        target_email = self.cleaned_data['target_email']
        result_email = ''

        # this is the simplest way to prevent sql injection.
        # TODO find better way to prevent sql injection
        for char in target_email:
            if char.isalnum() or char in ('@', '.'):
                result_email += char

        return result_email

    def save(self, commit=True):
        email = self.cleaned_data['target_email']
        domain = None

        if '@' in email:
            email, domain_name = email.split('@')[:2]

            if domain_name:
                domain = get_object_or_None(Domain, domain=domain_name)

        if not domain:
            domain = Domain.objects.annotate(domain_len=Length('domain')).order_by('domain_len').first()

        full_email = f'{email}@{domain.domain}'
        mailbox = Mailbox.objects.filter(active=1, username__iexact=full_email).last()

        if not mailbox:
            mailbox = Mailbox.objects.create(full_email)

        mailbox.department = 'inuse'
        mailbox.save(update_fields=('department',))

        return mailbox
