
from faker import Faker
from django.core.management.base import BaseCommand

from mails.models import Mails
from postfix.models import Mailbox
import random


fake = Faker()


class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        to_emails = list(Mailbox.objects.order_by('?').values_list('username', flat=True))
        mails = [Mails(
            to_email=random.choice(to_emails),
            from_email=fake.email(),
            from_name=fake.name(),
            subject=fake.city(),
        ) for _ in range(1000)]
        Mails.objects.bulk_create(mails)
