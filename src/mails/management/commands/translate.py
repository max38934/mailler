from django.core.management.base import BaseCommand
from django.conf import settings

import boto3
import polib
import tqdm
import re


class Command(BaseCommand):
    help = ''
    REGEX = re.compile(r'(%\(.*?\)[sd]?|{.*?})')

    def translate_phrase(self, phrase, target_language):

        translate = boto3.client(
            service_name='translate',
            region_name='us-east-1',
            use_ssl=True,
            aws_access_key_id='AKIAU66AQN2VKFEWOS45',
            aws_secret_access_key='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',
        )

        result = translate.translate_text(
            Text=phrase,
            SourceLanguageCode="en",
            TargetLanguageCode=target_language,
        )
        return result.get('TranslatedText')

    def handle(self, *args, **options):
        base_path = settings.BASE_DIR / 'translations' / 'locale'

        for locale_dir in tqdm.tqdm(list(base_path.iterdir())):
            locale_name = locale_dir.name

            target_language = {
                'zh-hans': 'zh',
            }.get(locale_name) or locale_name

            po_path = locale_dir / 'LC_MESSAGES' / 'django.po'

            po = polib.pofile(po_path)
            new_po = polib.POFile()

            new_po.metadata = po.metadata
            for entry in po:
                if not entry.msgstr or entry.fuzzy:
                    if entry.fuzzy:
                        entry.flags.remove('fuzzy')
                    original_vars = self.REGEX.findall(entry.msgid)
                    if original_vars:
                        target_phrase = self.REGEX.sub('{}', entry.msgid)
                        entry.msgstr = self.translate_phrase(target_phrase, target_language)
                        try:
                            entry.msgstr = entry.msgstr.format(*original_vars)
                        except:
                            pass
                    else:
                        entry.msgstr = self.translate_phrase(entry.msgid, target_language)

                new_po.append(entry)

            new_po.save(locale_dir / 'LC_MESSAGES' / 'django.po')
