
import os
import re
import json
from pathlib import Path

from django.core.management.base import BaseCommand


reg1 = re.compile('@media.*\{')


class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        with open('./src/accounts/static/index/Coverage-20210414T205256.json', 'r') as f:
            content = f.read()
            json_content = json.loads(content)

            for item in json_content:
                if item['url'].endswith('.css'):
                    css_file_name = Path(item['url']).name
                    css_file_content = item['text']
                    new_css_file_content = ''

                    for r in item['ranges']:
                        used_css = css_file_content[r['start']: r['end']]
                        used_css2 = css_file_content[r['start'] - 2: r['end']]
                        if used_css2.startswith('  .'):
                            start_index = r['start']
                            while start_index > 0:
                                if css_file_content[start_index: r['end']].startswith('@media'):
                                    media = reg1.match(css_file_content[start_index: r['end']]).group()
                                    used_css = media + '\n' + used_css + '\n}\n'
                                    break
                                start_index -= 1
                        new_css_file_content += '\n' + used_css

                    parent_dir = Path(item['url']).parent.name
                    if parent_dir != 'css':
                        Path(f'./src/accounts/static/index/min/{parent_dir}').mkdir(parents=True, exist_ok=True)
                        css_file_name = parent_dir + '/' + css_file_name
                    with open(f'./src/accounts/static/index/min/{css_file_name}', 'a') as f2:
                        f2.write('/* ======== ' + css_file_name + ' ======== */\n')
                        f2.write(new_css_file_content)

        # styles.css should be compiled in a proper order
        # with open(f'./src/accounts/static/index/styles.css', 'w') as f3:
        #     with open(f'./src/accounts/static/index/fonts.css', 'r') as f5:
        #         f3.write(f5.read())
        #         f3.write('\n')
        #
        #     for filename in os.listdir('./src/accounts/static/index/min/'):
        #         if filename.endswith(".css"):
        #             with open(os.path.join('./src/accounts/static/index/min/', filename), 'r') as f4:
        #                 f3.write(f4.read())
        #                 f3.write('\n')
