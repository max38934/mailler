# Generated by Django 3.2.2 on 2021-06-06 07:59

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mails', '0010_usercreatedemail'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='UserCreatedEmail',
            new_name='UserEmail',
        ),
    ]
