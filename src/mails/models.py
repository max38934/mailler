import uuid

from django.db import models
from django.utils.timesince import timesince

from django_lifecycle import LifecycleModel, hook, BEFORE_CREATE

from mails.utils import parse_email_content
from mixins.models import CreatedModel, CreatedDeleteModel


class SessionEmailAccounts(CreatedModel):
    """
    Link between mail account and django session.
    """

    uuid = models.UUIDField(help_text='Uuid values from the session.', unique=True)
    email = models.EmailField(help_text='FK on email account in the vmail DB.')

    def delete(self, using=None, keep_parents=False):
        from postfix.models import Mailbox

        Mailbox.objects.filter(username=self.email).update(active=False)
        return super().delete(using=using, keep_parents=keep_parents)

    def __str__(self):
        return f'({self.pk}) {self.email}'

    def __repr__(self):
        return self.__str__()


class UserEmail(LifecycleModel, CreatedDeleteModel):  # do not use delete hook
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    email = models.EmailField()

    @hook(BEFORE_CREATE)
    def create_mailbox_in_postfix(self):
        if not getattr(self, '_skip_mailbox_create', None):
            from postfix.models import Mailbox
            m = Mailbox.objects.create(username=self.email)
            m.department = 'inuse'
            m.save(update_fields=('department',))


class Mails(CreatedModel):
    hash = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, db_index=True)
    to_email = models.EmailField(db_index=True)
    from_email = models.EmailField()
    from_name = models.CharField(max_length=256)
    subject = models.CharField(max_length=256)
    body_legacy = models.TextField()
    seen = models.BooleanField(default=False)
    raw_content = models.TextField(default='', blank=True)

    class Meta:
        verbose_name_plural = 'Mails'

    def __str__(self):
        return f'({self.pk}) {self.to_email}'

    def __repr__(self):
        return self.__str__()

    def created_since(self):
        return timesince(self.created)

    @property
    def initials(self):
        return ''.join(i[0].upper() for i in self.from_name.split() if i)

    @property
    def body(self):
        return parse_email_content(self.raw_content)
