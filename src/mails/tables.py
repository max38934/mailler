import django_tables2 as tables
from django_tables2.utils import A

from mails.models import Mails


class MailsTable(tables.Table):

    class Meta:
        model = Mails
        template_name = "django_tables2/bootstrap.html"
        fields = ('to_email', 'count', 'seen_count', 'first_created')


class UserMailsTable(tables.Table):
    email_link = tables.LinkColumn("mails:mail-detail-inbox", text=lambda record: record.to_email, args=[A("hash")])

    class Meta:
        model = Mails
        template_name = "mails/tables/bootstrap4.html"
        fields = ('id', 'email_link', 'subject', 'created')
