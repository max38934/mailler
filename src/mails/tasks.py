from datetime import timedelta
from time import sleep

from django.conf import settings
from django.core.management import call_command
from django.core.mail import mail_managers
from django.template.loader import render_to_string
from constance import config
from django.utils import timezone

from settings.celeryapp import app


@app.task
def generate_new_mailboxes():
    from postfix.models import Mailbox
    available_mailboxes = Mailbox.objects.available().count()

    if available_mailboxes < config.MAILBOX_AVAILABLE_THRESHOLD:

        call_command('create_emails', num=config.MAILBOX_CREATE_NUMBER)

        context = {
            'available_mailboxes': available_mailboxes,
            'threshold': config.MAILBOX_AVAILABLE_THRESHOLD,
            'available_mailboxes_after': Mailbox.objects.available().count(),
        }

        template_text = 'mails/emails/generate_new_mailboxes/email.txt'
        template_html = 'mails/emails/generate_new_mailboxes/email.html'

        text_content = render_to_string(template_text, context)
        html_content = render_to_string(template_html, context)

        mail_managers("new mailboxes were generated", text_content, html_message=html_content)


@app.task
def release_mailboxes(delay=0.5):
    from postfix.models import Mailbox
    from mails.models import Mails

    pk_field = 'username'
    chunk_size = 1000
    current_index = 0
    max_created = timezone.now() - timedelta(seconds=settings.SESSION_COOKIE_AGE)
    queryset = Mailbox.objects.filter(active=1, department='inuse', created__lt=max_created).order_by(pk_field)
    ids = set(queryset[:chunk_size].values_list(pk_field, flat=True))

    while ids:
        email_boxes_with_emails = Mails.objects.filter(to_email__in=ids)\
            .values_list('to_email', flat=True)\
            .distinct('to_email')

        empty_ids = ids - set(email_boxes_with_emails)

        filter_kwargs = {
            f'{pk_field}__in': empty_ids,
        }

        queryset.filter(**filter_kwargs).update(department='')

        current_index += chunk_size
        ids = set(queryset[current_index:current_index + chunk_size].values_list(pk_field, flat=True))
        sleep(delay)


@app.task
def clean_mails():
    from mails.models import Mails

    chunk_size = 1000
    queryset = Mails.objects.exclude(body_legacy='').order_by('id')
    ids = set(queryset[:chunk_size].values_list('id', flat=True))

    while ids:
        Mails.objects.filter(id__in=ids).update(body_legacy='')

        ids = set(queryset[:chunk_size].values_list('id', flat=True))


@app.task
def delete_mails():
    from mails.models import Mails

    chunk_size = 1000
    queryset = Mails.objects.filter(seen=False).order_by('id')
    ids = set(queryset[:chunk_size].values_list('id', flat=True))

    while ids:
        queryset.filter(id__in=ids).delete()
        ids = set(queryset[:chunk_size].values_list('id', flat=True))
