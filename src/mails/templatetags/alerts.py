from django import template
from django.contrib.messages import constants

MESSAGE_TAGS = {
    constants.DEBUG: {
        'level': 'info',
    },
    constants.INFO: {
        'level': 'info',
    },
    constants.SUCCESS: {
        'level': 'success',
    },
    constants.WARNING: {
        'level': 'warning',
    },
    constants.ERROR: {
        'level': 'error',
    },
}

register = template.Library()


@register.filter
def message_context(message):
    return MESSAGE_TAGS[message.level]
