from django import template

register = template.Library()


# https://www.iso.org/obp/ui/#search
LANG_COUNTRY_MAPPER = {
    'en': 'us',
    'uk': 'ua',
    'hi': 'in',
    'cs': 'cz',
    'da': 'dk',
    'el': 'gr',
    'ja': 'jp',
    'ko': 'kr',
    'zh-hans': 'cn',
}


def get_country_code(lang_code):
    return LANG_COUNTRY_MAPPER.get(lang_code) or lang_code


register.filter('get_country_code', get_country_code)
