import urllib.parse

from django import template
from django.conf import settings


register = template.Library()


@register.simple_tag()
def decode_get_parameter(value):
    return urllib.parse.unquote(value)


@register.simple_tag()
def static_url(domain):
    url: str = settings.STATIC_URL
    if url.startswith('http'):
        return url

    return f'https://{domain}{url}'


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)
