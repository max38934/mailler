from django.conf import settings
from django.urls import path
from django.views.decorators.cache import cache_page

from mails import views

app_name = 'mails'

INDEX_PAGE_TIMEOUT = -1 if settings.DEBUG else 60 * 60 * 24  # 1 day

urlpatterns = [
    path('', cache_page(INDEX_PAGE_TIMEOUT)(views.IndexView.as_view()), name='index'),

    path('delete/', views.EmailDeleteView.as_view(), name='email-delete'),
    path('view/<uuid:hash>/', views.MailDetailView.as_view(), name='mail-detail'),
    path('group/', views.EmailsGroupView.as_view(), name='group'),
    path('get-create-email/', views.GetOrCreateMailboxView.as_view(), name='get-or-create-email'),

    # profile urls
    path('inboxes/list/', views.UserInboxes.as_view(), name='user-inboxes'),
    path('inboxes/create/random/', views.UserInboxesCreateRandom.as_view(), name='user-inboxes-create-random'),
    path('view/inbox/<uuid:hash>/', views.MailDetailInboxView.as_view(), name='mail-detail-inbox'),
    path('inboxes/delete/<int:pk>/', views.DeleteUserEmail.as_view(), name='mail-delete-inbox'),

    path('inboxes/api-keys/', views.APIKeysView.as_view(), name='api_keys'),
    path('inboxes/api-keys/delete/<int:pk>/', views.APIKeysDeleteView.as_view(), name='api_keys_delete'),
]
