from django.conf import settings
from django.urls import path
from django.views.decorators.cache import cache_page

from mails import views

app_name = 'mails'

INDEX_PAGE_TIMEOUT = -1 if settings.DEBUG else 60 * 60 * 24  # 1 day

urlpatterns = [
    path('', cache_page(INDEX_PAGE_TIMEOUT)(views.IndexView.as_view()), name='index'),

    path('delete/', views.EmailDeleteView.as_view(), name='email-delete'),
    path('details/<uuid:hash>/', views.MailDetailView.as_view(), name='mail-detail'),
    path('get-email/', views.GetOrCreateMailboxView.as_view(), name='get-or-create-email'),
]
