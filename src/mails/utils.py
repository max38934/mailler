import email

from django.db.models.functions import Length
from django.utils import timezone

from postfix.models import Domain


def _get_domain():
    return Domain.objects.annotate(domain_len=Length('domain')).order_by('domain_len').first()


def get_session_email(request):
    from postfix.models import Mailbox

    if email_param := request.GET.get('email'):

        # prevent sql injection
        email_value = ''
        for char in email_param:
            if char.isalnum() or char in ('@', '.'):
                email_value += char

        if '@' in email_value:
            domain_name = email_value.split('@')[1]
            value = email_value.split("@")[0]

            if not Domain.objects.filter(domain=domain_name).exists():
                domain = _get_domain()
                email_value = f'{value}@{domain.domain}'
        else:
            domain = _get_domain()
            email_value = f'{email_value}@{domain.domain}'

        mailbox = Mailbox.objects.filter(active=1, username__iexact=email_value).last()

        if not mailbox:
            mailbox = Mailbox.objects.create(email_value)

        mailbox.department = 'inuse'
        mailbox.save(update_fields=('department',))

        request.session['email'] = mailbox.username

    else:
        email = request.session.get('email')

        if not email:
            email_account = Mailbox.objects.available().only('username', 'department')[0]
            email_account.department = 'inuse'
            email_account.modified = timezone.now()
            email_account.save(update_fields=('department', 'modified'))
            request.session['email'] = email_account.username

    return request.session['email']


def parse_email_content(raw_content: str) -> str:
    b = email.message_from_string(raw_content)

    body = b""

    if b.is_multipart():
        target_content_type = 'text/html' \
            if any(part.get_content_type() == 'text/html' for part in b.walk()) else 'text/plain'

        for part in b.walk():
            ctype = part.get_content_type()
            cdispo = str(part.get('Content-Disposition'))

            # skip any text/plain (txt) attachments
            if ctype == target_content_type and 'attachment' not in cdispo:
                body = part.get_payload(decode=True)  # decode
                break
    # not multipart - i.e. plain text, no attachments, keeping fingers crossed
    else:
        body = b.get_payload(decode=True)


    return body.decode().strip().replace('\n', '')
