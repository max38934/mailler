import json
import requests
import base64
from django.contrib import messages

from django.contrib.auth.mixins import UserPassesTestMixin
from django.db.models.functions import Cast
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, RedirectView, DetailView, CreateView, DeleteView, View
from django.db.models import Count, Min, IntegerField, Sum
from django_tables2 import SingleTableView
from django_filters.views import FilterView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from oauth2_provider.models import get_application_model, get_access_token_model

from mails.filters import UserMailsFilter
from postfix.models import Mailbox, Domain
from django.core.serializers.json import DjangoJSONEncoder

from mails.models import Mails, UserEmail
from mails.forms import GetOrCreateMailboxForm
from mails.tables import MailsTable
from mails.utils import get_session_email
from blog import choices as blog_mch
from blog.models import Entry


@method_decorator(csrf_exempt, name='dispatch')
class IndexView(TemplateView):
    template_name = "mails/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entries'] = Entry.objects\
            .filter(site_id=settings.SITE_ID, status=blog_mch.ENTRY_LIVE_STATUS)\
            .order_by('-id')
        return context


class EmailDeleteView(RedirectView):
    pattern_name = 'mails:index'

    def get_redirect_url(self, *args, **kwargs):
        if 'email' in self.request.session:
            del self.request.session['email']

        return super().get_redirect_url(*args, **kwargs)


class GetOrCreateMailboxView(CreateView):
    model = Mailbox
    success_url = reverse_lazy('mails:index')
    form_class = GetOrCreateMailboxForm

    def get(self, request, *args, **kwargs):

        if not request.GET.get('target_email'):
            return redirect('mails:index')

        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
            'data': self.request.GET,
        }
        return kwargs

    def get_success_url(self):
        self.request.session['email'] = self.object.username
        return super().get_success_url()


class MailDetailView(DetailView):
    slug_field = 'hash'
    slug_url_kwarg = 'hash'
    model = Mails

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)

        if not obj.seen:
            obj.seen = True
            obj.save(update_fields=('seen',))

        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email'] = get_session_email(self.request)
        context['json_display'] = self.get_json_display()
        return context

    def get_json_display(self):
        return json.dumps(
            {
                'hash': self.object.hash,
                'to_email': self.object.to_email,
                'from_email': self.object.from_email,
                'subject': self.object.subject,
                'body': self.object.body,
                'seen': self.object.seen,
                'created': self.object.created,
                'raw': self.object.raw_content,
            },
            sort_keys=True,
            indent=4,
            cls=DjangoJSONEncoder,
        )

class MailDetailInboxView(MailDetailView):
    template_name = 'mails/mails_details_auth.html'


class EmailsGroupView(UserPassesTestMixin, SingleTableView):
    template_name = 'mails/group.html'
    table_class = MailsTable

    queryset = Mails.objects.values('to_email')\
        .annotate(count=Count('id'),
                  first_created=Min('created'),
                  seen_count=Sum(Cast('seen', output_field=IntegerField()))) \
        .order_by('-first_created', '-count')

    def test_func(self):
        return self.request.user.is_staff or self.request.user.is_superuser


class UserInboxes(LoginRequiredMixin, FilterView):
    template_name = 'mails/user_inboxes.html'
    queryset = Mails.objects.all()
    paginate_by = 10
    filterset_class = UserMailsFilter

    def get_template_names(self):
        if self.request.is_ajax():
            return ['mails/parts/user_inbox.html']

        return super().get_template_names()

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(
            to_email__in=UserEmail.objects.filter(user=self.request.user).values_list('email', flat=True))

        return queryset.order_by('-id')

    def _create_mailbox_if_none(self):
        count_emails = UserEmail.objects.filter(user=self.request.user).count()

        if not count_emails:
            email_account = Mailbox.objects.available().only('username', 'department').first()
            email_account.department = 'inuse'
            email_account.save(update_fields=('department',))

            email = UserEmail(user=self.request.user, email=email_account.username)
            email._skip_mailbox_create = True
            email.save()

    def get_context_data(self, **kwargs):
        self._create_mailbox_if_none()
        context = super().get_context_data(**kwargs)
        context['domains'] = Domain.objects.all()
        context['inboxes'] = UserEmail.objects.filter(user=self.request.user)

        return context


class DeleteUserEmail(LoginRequiredMixin, DeleteView):
    queryset = UserEmail.objects.all()
    http_method_names = ['post', 'head', 'options', 'trace']
    success_url = reverse_lazy('mails:user-inboxes')

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset


class UserInboxesCreateRandom(LoginRequiredMixin, View):
    def get(self, request):

        count_emails = UserEmail.objects.filter(user=self.request.user).count()

        if count_emails >= settings.USER_MAXIMUM_EMAILS_ALLOWED:
            messages.error(self.request, f'Maximum active emails cannot exceed {settings.USER_MAXIMUM_EMAILS_ALLOWED}.')
            return redirect('mails:user-inboxes')

        email_account = Mailbox.objects.available().only('username', 'department').first()
        email_account.department = 'inuse'
        email_account.save(update_fields=('department',))

        email = UserEmail(user=self.request.user, email=email_account.username)
        email._skip_mailbox_create = True
        email.save()

        return redirect('mails:user-inboxes')


class APIKeysView(LoginRequiredMixin, CreateView):
    queryset = get_access_token_model().objects.all()
    template_name = 'mails/api_keys.html'
    fields = ()

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """

        count_tokens = self.get_queryset().count()
        if count_tokens >= settings.USER_MAXIMUM_TOKEN_ALLOWED:
            messages.error(self.request, f'Maximum tokens cannot exceed {settings.USER_MAXIMUM_TOKEN_ALLOWED}.')
            return redirect('mails:api_keys')

        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        token = self.create_token()
        self.queryset.filter(token=token['access_token']).update(user=self.request.user)
        return redirect('mails:api_keys')

    def create_token(self) -> dict:
        headers = {
            'Authorization': f'Basic {self.get_oauth_token().decode()}',
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        data = {
            'grant_type': 'client_credentials',
        }
        response = requests.post(settings.OAUTH_VERIFICATION_ENDPOINT, headers=headers, data=data)
        response.raise_for_status()
        return response.json()

    def get_oauth_token(self):
        ApplicationModel = get_application_model()
        app = ApplicationModel.objects.get(name__iexact='api')
        credential = "{0}:{1}".format(app.client_id, app.client_secret)
        return base64.b64encode(credential.encode("utf-8"))

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = self.get_queryset()
        context['allow_create'] = self.get_queryset().count() < settings.USER_MAXIMUM_TOKEN_ALLOWED
        context['max_tokens_allowed'] = settings.USER_MAXIMUM_TOKEN_ALLOWED
        return context


class APIKeysDeleteView(LoginRequiredMixin, DeleteView):
    queryset = get_access_token_model().objects.all()
    template_name = 'mails/api_keys.html'
    success_url = reverse_lazy('mails:api_keys')

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset
