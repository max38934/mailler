from crispy_forms.layout import Field
from crispy_forms.utils import TEMPLATE_PACK


class CrispyField(Field):
    def __init__(self, *args, **kwargs):
        self.extra_context = kwargs.pop('extra_context', {})
        super().__init__(*args, **kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, extra_context=None, **kwargs):
        if self.extra_context:
            extra_context = extra_context.update(self.extra_context) if extra_context else self.extra_context

        return super().render(form, form_style, context, template_pack, extra_context, **kwargs)
