from django.db import models, connections
from django.db.models import QuerySet, Manager
from django.utils import timezone


class DeleteQuerySet(QuerySet):
    """
    Prevents objects from being hard-deleted. Instead, sets the
    ``deleted``, effectively soft-deleting the object.
    """

    def delete(self):
        self.update(deleted=timezone.now())

        # for obj in self:
        #     obj.deleted = timezone.now()
        #     obj.save(update_fields=['deleted'])

    def undelete(self):
        """
        Used for relations.
        """

        self.update(deleted=timezone.now())

        # for obj in self:
        #     obj.deleted = None
        #     obj.save()


class DeleteManager(models.Manager):
    """
    Only exposes objects that have NOT been soft-deleted.
    """

    def get_queryset(self):
        return DeleteQuerySet(self.model, using=self._db).filter(deleted__isnull=True)


class EstimateCountQuerySet(models.QuerySet):

    def count(self):
        if self.query.where:
            return super().count()

        cursor = connections[self.db].cursor()
        cursor.execute("SELECT reltuples FROM pg_class "
                       "WHERE relname = '%s';" % self.model._meta.db_table)
        return int(cursor.fetchone()[0])


EstimateCountManager = Manager.from_queryset(EstimateCountQuerySet)
