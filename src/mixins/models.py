from django.db import models, transaction
from django.db.models import signals
from django.db.models.signals import pre_save, post_save
from django.utils import timezone

from mixins.context_managers import DisableSignals
from mixins.managers import DeleteManager


class CreatedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class CreatedModifiedModel(CreatedModel):
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class DeleteModel(models.Model):
    deleted = models.DateTimeField(null=True, blank=True)

    objects = DeleteManager()
    original_objects = models.Manager()

    class Meta:
        abstract = True

    def delete(self, using=None, keep_parents=False):
        with transaction.atomic(using=self._state.db, savepoint=False):
            signals.pre_delete.send(
                sender=self.__class__, instance=self, using=self._state.db
            )
            # do not trigger save signals under delete method
            with DisableSignals([pre_save, post_save]):
                self.deleted = timezone.now()
                self.save(update_fields=['deleted'])

            signals.post_delete.send(
                sender=self.__class__, instance=self, using=self._state.db
            )

    def undelete(self):
        """
        Used for relations.
        """

        self.deleted = None
        self.save(update_fields=['deleted'])


class CreatedDeleteModel(CreatedModel, DeleteModel):
    class Meta:
        abstract = True


class CreatedModifiedDeleteModel(CreatedModifiedModel, DeleteModel):
    class Meta:
        abstract = True
