from django.apps import AppConfig


class PostfixConfig(AppConfig):
    name = 'postfix'
