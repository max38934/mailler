from functools import cached_property
import random
import unicodedata

from django.db import transaction
from django.db.utils import ProgrammingError
from unidecode import unidecode
from tqdm import tqdm

from django.core.management.base import BaseCommand
from postfix.models import Mailbox, Domain

import os


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument("-n", "--num", type=int)

    def handle(self, *args, **options):
        counter = options["num"] if options["num"] else 100

        for _ in tqdm(range(counter)):
            username = unicodedata.normalize('NFKD', self.random_username)\
                .replace("'", "'") \
                .replace('"', '"') \
                .replace('-', '') \
                .replace('`', '')

            for domain in Domain.objects.all():
                email = f'{username}@{domain.domain}'

                try:
                    if not Mailbox.objects.filter(username=email).exists():
                        Mailbox.objects.create(email)
                except (UnicodeDecodeError, ProgrammingError) as exc:  # TODO
                    print(email)
                    print(exc)
                    break

    @property
    def numbers(self):
        return tuple(i for i in range(1, 100_000))

    @cached_property
    def first_names(self):
        first_names_filename = os.path.join(os.path.dirname(__file__), 'firstNames.txt')
        with open(first_names_filename, 'r', errors='ignore', encoding='utf8') as r:
            return tuple(r.read().strip().split('\n'))

    @cached_property
    def last_names(self):
        last_names_filename = os.path.join(os.path.dirname(__file__), 'lastNames.txt')
        with open(last_names_filename, 'r', errors='ignore', encoding='utf8') as r:
            return tuple(r.read().strip().split('\n'))

    @property
    def random_username(self):
        samples = [
            self.first_names,
            self.last_names,
            self.numbers,
        ]

        k = random.randint(2, 3)
        sample = random.sample(samples, k)

        value = ''
        for samp in sample:
            value += str(random.choice(samp))

        return unidecode(value)
