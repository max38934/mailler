# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

import subprocess
import uuid

from django.conf import settings
from django.db import models
from django.db.models.functions import Length

from django.utils import timezone


class Admin(models.Model):
    username = models.CharField(primary_key=True, max_length=255)
    password = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    language = models.CharField(max_length=5)
    passwordlastchange = models.DateTimeField()
    settings = models.TextField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'admin'


class Alias(models.Model):
    address = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255)
    accesspolicy = models.CharField(max_length=30)
    domain = models.CharField(max_length=255)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'alias'


class AliasDomain(models.Model):
    alias_domain = models.CharField(primary_key=True, max_length=255)
    target_domain = models.CharField(max_length=255)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'alias_domain'


class AnyoneShares(models.Model):
    from_user = models.CharField(primary_key=True, max_length=255)
    dummy = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'anyone_shares'


class DeletedMailboxes(models.Model):
    timestamp = models.DateTimeField()
    username = models.CharField(max_length=255)
    domain = models.CharField(max_length=255)
    maildir = models.CharField(max_length=255)
    admin = models.CharField(max_length=255)
    delete_date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'deleted_mailboxes'


class Domain(models.Model):
    domain = models.CharField(primary_key=True, max_length=255)
    description = models.TextField()
    disclaimer = models.TextField()
    aliases = models.BigIntegerField()
    mailboxes = models.BigIntegerField()
    maillists = models.BigIntegerField()
    maxquota = models.BigIntegerField()
    quota = models.BigIntegerField()
    transport = models.CharField(max_length=255)
    settings = models.TextField()
    backupmx = models.SmallIntegerField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'domain'


class DomainAdmins(models.Model):
    username = models.CharField(primary_key=True, max_length=255)
    domain = models.CharField(max_length=255)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'domain_admins'
        unique_together = (('username', 'domain'),)


class Forwardings(models.Model):
    address = models.CharField(max_length=255)
    forwarding = models.CharField(max_length=255)
    domain = models.CharField(max_length=255)
    dest_domain = models.CharField(max_length=255)
    is_maillist = models.SmallIntegerField()
    is_list = models.SmallIntegerField()
    is_forwarding = models.SmallIntegerField()
    is_alias = models.SmallIntegerField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'forwardings'
        unique_together = (('address', 'forwarding'),)


class MailboxManager(models.Manager):
    def create(self, username):
        password = str(uuid.uuid4())
        script_path = settings.BASE_DIR / 'postfix' / 'create_mail_user.sh'

        command = [
            script_path,
            username,
            password,
        ]

        result = subprocess.run(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )

        if result.returncode == 0:
            sql = result.stdout.replace('\n', ' ')

            # TODO find better way to trigger raw sql
            try:
                tuple(self.raw(sql))  # trigger query
            except TypeError:
                pass

            return self.get(username=username)

    def available(self):
        now = timezone.now()
        # .annotate(email_len=Length('username')) \
        # .order_by('email_len')
        return self.get_queryset() \
           .filter(active=1, expired__gt=now, department='').order_by()


class Mailbox(models.Model):
    objects = MailboxManager()

    username = models.EmailField(primary_key=True, max_length=255)
    password = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    language = models.CharField(max_length=5)
    mailboxformat = models.CharField(max_length=50)
    mailboxfolder = models.CharField(max_length=50)
    storagebasedirectory = models.CharField(max_length=255)
    storagenode = models.CharField(max_length=255)
    maildir = models.CharField(max_length=255)
    quota = models.BigIntegerField()
    domain = models.CharField(max_length=255)
    transport = models.CharField(max_length=255)
    department = models.CharField(max_length=255)
    rank = models.CharField(max_length=255)
    employeeid = models.CharField(max_length=255, blank=True, null=True)
    isadmin = models.SmallIntegerField()
    isglobaladmin = models.SmallIntegerField()
    enablesmtp = models.SmallIntegerField()
    enablesmtpsecured = models.SmallIntegerField()
    enablepop3 = models.SmallIntegerField()
    enablepop3secured = models.SmallIntegerField()
    enablepop3tls = models.SmallIntegerField()
    enableimap = models.SmallIntegerField()
    enableimapsecured = models.SmallIntegerField()
    enableimaptls = models.SmallIntegerField()
    enabledeliver = models.SmallIntegerField()
    enablelda = models.SmallIntegerField()
    enablemanagesieve = models.SmallIntegerField()
    enablemanagesievesecured = models.SmallIntegerField()
    enablesieve = models.SmallIntegerField()
    enablesievesecured = models.SmallIntegerField()
    enablesievetls = models.SmallIntegerField()
    enableinternal = models.SmallIntegerField()
    enabledoveadm = models.SmallIntegerField()
    enablelib_storage = models.SmallIntegerField(
        db_column='enablelib-storage')  # Field renamed to remove unsuitable characters.
    enablequota_status = models.SmallIntegerField(
        db_column='enablequota-status')  # Field renamed to remove unsuitable characters.
    enableindexer_worker = models.SmallIntegerField(
        db_column='enableindexer-worker')  # Field renamed to remove unsuitable characters.
    enablelmtp = models.SmallIntegerField()
    enabledsync = models.SmallIntegerField()
    enablesogo = models.SmallIntegerField()
    allow_nets = models.TextField(blank=True, null=True)
    lastlogindate = models.DateTimeField()
    lastloginipv4 = models.GenericIPAddressField()
    lastloginprotocol = models.CharField(max_length=255)
    disclaimer = models.TextField()
    allowedsenders = models.TextField()
    rejectedsenders = models.TextField()
    allowedrecipients = models.TextField()
    rejectedrecipients = models.TextField()
    settings = models.TextField()
    passwordlastchange = models.DateTimeField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'mailbox'


class Maillists(models.Model):
    address = models.CharField(unique=True, max_length=255)
    domain = models.CharField(max_length=255)
    transport = models.CharField(max_length=255)
    accesspolicy = models.CharField(max_length=30)
    maxmsgsize = models.BigIntegerField()
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    mlid = models.CharField(unique=True, max_length=36)
    is_newsletter = models.SmallIntegerField()
    settings = models.TextField(blank=True, null=True)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'maillists'


class Moderators(models.Model):
    address = models.CharField(max_length=255)
    moderator = models.CharField(max_length=255)
    domain = models.CharField(max_length=255)
    dest_domain = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'moderators'
        unique_together = (('address', 'moderator'),)


class RecipientBccDomain(models.Model):
    domain = models.CharField(primary_key=True, max_length=255)
    bcc_address = models.CharField(max_length=255)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'recipient_bcc_domain'


class RecipientBccUser(models.Model):
    username = models.CharField(primary_key=True, max_length=255)
    bcc_address = models.CharField(max_length=255)
    domain = models.CharField(max_length=255)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'recipient_bcc_user'


class SenderBccDomain(models.Model):
    domain = models.CharField(primary_key=True, max_length=255)
    bcc_address = models.CharField(max_length=255)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'sender_bcc_domain'


class SenderBccUser(models.Model):
    username = models.CharField(primary_key=True, max_length=255)
    bcc_address = models.CharField(max_length=255)
    domain = models.CharField(max_length=255)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    expired = models.DateTimeField()
    active = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'sender_bcc_user'


class SenderRelayhost(models.Model):
    account = models.CharField(unique=True, max_length=255)
    relayhost = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'sender_relayhost'


class ShareFolder(models.Model):
    from_user = models.CharField(primary_key=True, max_length=255)
    to_user = models.CharField(max_length=255)
    dummy = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'share_folder'
        unique_together = (('from_user', 'to_user'),)


class UsedQuota(models.Model):
    username = models.CharField(primary_key=True, max_length=255)
    bytes = models.BigIntegerField()
    messages = models.BigIntegerField()
    domain = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'used_quota'
