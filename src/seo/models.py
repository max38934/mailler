import os


def _get_picture_path(instance, filename):
    return os.path.join('manual_review', str(instance.site_id), filename)
