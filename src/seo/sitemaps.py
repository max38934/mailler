
from django.conf import settings
from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from blog.models import Entry
from blog import choices as blog_mch


class BlogViewSitemap(Sitemap):
    priority = 0.6
    protocol = 'https'

    def items(self):
        return Entry.objects.filter(site_id=settings.SITE_ID, status=blog_mch.ENTRY_LIVE_STATUS).order_by('-id')

    @staticmethod
    def lastmod(obj):
        return obj.pub_date

    def location(self, obj):
        return reverse('blog:entry_detail', args=(obj.slug,))
