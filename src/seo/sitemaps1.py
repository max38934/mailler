
import os
from datetime import datetime

from django.conf import settings
from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class StaticViewSitemap(Sitemap):
    priority = 0.5
    protocol = 'https'
    # changefreq = 'monthly'

    def items(self):
        return ['accounts:faq', 'accounts:contact']

    @staticmethod
    def lastmod(obj):
        app, filename = obj.split(':')
        file_path = os.path.join(settings.BASE_DIR, app, 'templates', str(settings.SITE_ID), app, f'{filename}.html')
        return datetime.fromtimestamp(os.path.getmtime(file_path))

    def location(self, obj):
        return reverse(obj)


class IndexViewSitemap(StaticViewSitemap):
    priority = 0.9

    def items(self):
        return 'mails:index',


class IndexI18nViewSitemap(StaticViewSitemap):
    priority = 0.7
    i18n = True

    def items(self):
        return 'mails:index',
