from settings.base import *  # noqa
from django.urls import reverse_lazy


if IS_PRODUCTION:  # noqa
    ALLOWED_HOSTS = ['fakemail.io', '127.0.0.1']

LOGIN_REDIRECT_URL = reverse_lazy('mails:user-inboxes')
LOGOUT_REDIRECT_URL = reverse_lazy('mails:index')

SERVER_EMAIL = DEFAULT_FROM_EMAIL = CONTACT_EMAIL = 'support@fakemail.io'
