from settings.base import *  # noqa


BRAND_NAME = 'Admin'

if IS_PRODUCTION:  # noqa
    ALLOWED_HOSTS = ['admin.fakemail.io', '127.0.0.1']

SERVER_EMAIL = DEFAULT_FROM_EMAIL = CONTACT_EMAIL = 'support@fakemail.io'
