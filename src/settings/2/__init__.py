from settings.base import *  # noqa


BRAND_NAME = 'RandoMail'

if IS_PRODUCTION:  # noqa
    ALLOWED_HOSTS = ['randomail.io', '127.0.0.1']

SERVER_EMAIL = DEFAULT_FROM_EMAIL = CONTACT_EMAIL = 'support@randomail.io'
