import os
import socket

from celery import Celery
from celery.schedules import crontab
from celery.signals import task_failure
from django.core.mail import mail_admins
from kombu import Queue, Exchange

from django.conf import settings  # noqa


SITE_ID = int(os.getenv('SITE_ID', 1))

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'settings.{SITE_ID}')


app = Celery('mailler')

# Using a string here means the worker doesn't have to serialize
# the configuration object.
app.config_from_object('django.conf:settings')

# load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.timezone = 'UTC'
app.conf.enable_utc = True

# TODO beat_schedule task should go to their specific queue. atm they use default queue
# check https://stackoverflow.com/questions/16962449/how-to-send-periodic-tasks-to-specific-queue-in-celery
app.conf.beat_schedule = {
    'generate_new_mailboxes': {
        'task': 'mails.tasks.generate_new_mailboxes',
        'schedule': crontab(minute='1'),
        'options': {'queue': 'mailler.normal'},
    },
    'release_mailboxes': {
        'task': 'mails.tasks.release_mailboxes',
        'schedule': crontab(hour='12', minute='0'),
        'options': {'queue': 'mailler.normal'},
    },
}

app.conf.broker_url = 'amqp://{0}:{1}@{2}:{3}/{4}'.format(
    os.environ.get('RABBITMQ_DEFAULT_USER'),
    os.environ.get('RABBITMQ_DEFAULT_PASS'),
    os.environ.get('RABBITMQ_DEFAULT_HOST'),
    os.environ.get('RABBITMQ_DEFAULT_PORT', '5672'),
    os.environ.get('RABBITMQ_DEFAULT_VHOST', '/'),
)
# app.conf.result_backend = 'django-db'

exchange_normal = Exchange('mailler', type='direct')

app.conf.task_routes = {
}

app.conf.task_default_queue = 'mailler'
app.conf.task_queues = (
    Queue('mailler', exchange=exchange_normal, routing_key='mailler.#'),
)
app.conf.task_default_exchange = 'mailler'
# app.conf.task_default_exchange_type = 'topic'
app.conf.task_default_routing_key = 'mailler.normal'

app.conf.task_create_missing_queues = True


@task_failure.connect
def celery_task_failure_email(sender=None, task_id=None, exception=None, args=None, kwargs=None,
                              traceback=None, einfo=None, **akwargs):
    """ celery 4.0 onward has no method to send emails on failed tasks
    so this event handler is intended to replace it
    """

    subject = "[{queue_name}@{host}] Error: Task {sender.name} ({task_id}): {exception}".format(
        queue_name='celery',
        host=socket.gethostname(),
        sender=sender,
        task_id=task_id,
        exception=exception
    ).replace('\n', '')

    message = """Task {sender.name} with id {task_id} raised exception:
{exception!r}


Task was called with args: {args} kwargs: {kwargs}.

The contents of the full traceback was:

{einfo}
    """.format(sender=sender, task_id=task_id, exception=exception, args=args, kwargs=kwargs, einfo=einfo)

    mail_admins(subject, message)
