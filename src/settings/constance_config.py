CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
CONSTANCE_DATABASE_CACHE_BACKEND = 'default'

CONSTANCE_CONFIG = {
    'MAILBOX_AVAILABLE_THRESHOLD': (
        50_000,
        'Under which number of available inboxes periodic task will generate new Mailboxes',
    ),
    'MAILBOX_CREATE_NUMBER': (
        50_000,
        'How many new inboxes will be generated when the threshold is reached',
    ),
}
