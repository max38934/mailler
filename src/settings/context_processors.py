import time
from datetime import datetime

from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site


def pass_data(request):
    site = get_current_site(request)

    return {
        'DEBUG': settings.DEBUG,
        'BRAND_NAME': settings.BRAND_NAME,
        'CONTACT_EMAIL': settings.CONTACT_EMAIL,
        'SUPPORT_PHONE': settings.SUPPORT_PHONE,
        'ADDRESS': settings.ADDRESS,
        'IS_PRODUCTION': settings.IS_PRODUCTION,
        'site': site,
        'full_domain': ('http://' if settings.DEBUG else 'https://') + site.domain,
        'LANGUAGES': settings.LANGUAGES,
        'LANGUAGE_CODE': request.LANGUAGE_CODE,
        'LONG_POLLING': settings.LONG_POLLING,
    }


def index_page_counters(request):
    timestamp = int(time.time() - datetime(2020, 1, 1).timestamp())
    return {
        'EMAILS_RECEIVED': timestamp // 20,
        'SPAM_DETECTED_EMAILS': timestamp // 46,
        'PROJECTS_TESTED': timestamp // 36500,
    }
