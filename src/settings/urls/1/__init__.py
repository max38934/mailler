
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib.flatpages.sitemaps import FlatPageSitemap
from django.contrib.sitemaps.views import sitemap
from django.views.decorators.cache import cache_page
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from seo.sitemaps import BlogViewSitemap
from seo.sitemaps1 import IndexViewSitemap, IndexI18nViewSitemap, StaticViewSitemap

CACHE_PAGE_TIMEOUT = -1 if settings.DEBUG else 60 * 60 * 24  # 1 day

sitemaps = {
    'index': IndexViewSitemap,
    'index-i18n': IndexI18nViewSitemap,
    'flatpages': FlatPageSitemap,
    'static': StaticViewSitemap,
    'blog': BlogViewSitemap,
}

urlpatterns = []

if settings.DEBUG:
    urlpatterns.append(path('admin/', admin.site.urls))

urlpatterns += [
    # path('accounts/', include('django.contrib.auth.urls')),
    # path('accounts/', include(f'accounts.urls.{settings.SITE_ID}')),
    path('blog/', include(f'blog.urls')),

    # seo
    path('robots.txt', include('robots.urls')),
    path('sitemap.xml', cache_page(CACHE_PAGE_TIMEOUT)(sitemap), {'sitemaps': sitemaps}, name='sitemaps'),

    path('api/v1/o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('api/v1/p/', include(f'mails.api.public.urls'))
]

urlpatterns += i18n_patterns(
    path('', include(f'mails.urls.{settings.SITE_ID}')),
    path('api/v1/mails/', include(f'mails.api.urls.{settings.SITE_ID}')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include(f'accounts.urls.{settings.SITE_ID}')),
    path('', include('django_registration.backends.activation.urls')),
    prefix_default_language=False
)

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description=f"{settings.BRAND_NAME} Api",
      terms_of_service="https://fakemail.io/accounts/terms-of-use/",
      contact=openapi.Contact(email=settings.CONTACT_EMAIL),
      # license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
   # patterns=[path("api/v1/", include((public_router.urls, "api"), namespace="v1"))],
)

CACHE_TIMEOUT_REDOC = 0 if settings.DEBUG else 60 * 60 * 24

urlpatterns.extend([
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    # re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=CACHE_TIMEOUT_REDOC), name='schema-redoc'),
])


if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path(r'__debug__/', include(debug_toolbar.urls)),
    ]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    if 'rosetta' in settings.INSTALLED_APPS:
        urlpatterns += [
            re_path(r'^rosetta/', include('rosetta.urls'))
        ]
