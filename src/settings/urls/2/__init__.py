
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib.sitemaps.views import sitemap
from django.views.decorators.cache import cache_page

from seo.sitemaps import BlogViewSitemap
from seo.sitemaps2 import IndexViewSitemap, IndexI18nViewSitemap

CACHE_PAGE_TIMEOUT = -1 if settings.DEBUG else 60 * 60 * 24  # 1 day

sitemaps = {
    'index': IndexViewSitemap,
    'index-i18n': IndexI18nViewSitemap,
    'blog': BlogViewSitemap,
}


urlpatterns = [
    path('blog/', include(f'blog.urls')),
    path('', include(f'accounts.urls.{settings.SITE_ID}')),

    # seo
    path('robots.txt', include('robots.urls')),
    path('sitemap.xml', cache_page(CACHE_PAGE_TIMEOUT)(sitemap), {'sitemaps': sitemaps}, name='sitemaps'),
]

urlpatterns += i18n_patterns(
    path('', include(f'mails.urls.{settings.SITE_ID}')),
    path('api/', include(f'mails.api.urls.{settings.SITE_ID}')),
    prefix_default_language=False
)


if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path(r'__debug__/', include(debug_toolbar.urls)),
    ]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    if 'rosetta' in settings.INSTALLED_APPS:
        urlpatterns += [
            re_path(r'^rosetta/', include('rosetta.urls'))
        ]
