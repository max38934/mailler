"""
WSGI config for mailler project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application


SITE_ID = int(os.getenv('SITE_ID', 1))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'settings.{SITE_ID}')

application = get_wsgi_application()
