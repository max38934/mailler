
from django.template.loaders.app_directories import Loader as AppLoader

from amp_tools import get_amp_detect
from amp_tools.settings import settings


class Loader(AppLoader):

    def get_template_sources(self, template_name):
        template_name = f'{get_amp_detect()}/{template_name}'

        if settings.AMP_TOOLS_TEMPLATE_PREFIX:
            template_name = settings.AMP_TOOLS_TEMPLATE_PREFIX + template_name

        return super().get_template_sources(template_name)
