import os
from django.conf import settings
from django.template.loaders.app_directories import Loader as AppLoader


class Loader(AppLoader):

    def get_template_sources(self, *args, **kwargs):
        template_name = args[0]
        default_dir = getattr(settings, 'MULTISITE_DEFAULT_TEMPLATE_DIR', 'default')
        for tname in (os.path.join(str(settings.SITE_ID), template_name),
                      os.path.join(default_dir, template_name)):
            args = [tname]
            for item in super().get_template_sources(*args, **kwargs):
                yield item
