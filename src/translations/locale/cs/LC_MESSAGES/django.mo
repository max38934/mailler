��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  �  R;  �   �<  �   x=  �   >  �   �>  ,  F?  �  s@  �   1B  �   �B  �   �C  �   PD  �   �D  e   �E  �   �E  �   �F  �   ,G  �   �G  �   �H  �  �I  �  RN  O   S  �   eS     	T     T     T     1T     8T     HT  0   cT     �T  X   �T     �T  %   U     EU     UU     ]U     uU     ~U     �U     �U     �U     �U  
   �U  .   �U  	   V     V     *V     0V     7V     HV     XV     oV     �V     �V     �V     �V      �V     �V     �V     W     W     .W     @W     DW     SW     jW     �W     �W  	   �W  5   �W  '   �W     �W     X     X  .   $X  "   SX  !   vX  #   �X  3   �X     �X  .   Y     2Y     DY     PY     bY     tY  (   �Y     �Y     �Y     �Y     �Y     �Y     Z  L   (Z     uZ  �   �Z    ~[     �\     �\  ;   �\     �\  !   �\  -   ]     4]     D]  N   ^]     �]     �]     �]  4   �]  L   ^  .   [^     �^     �^     �^  P   �^     _     !_     2_     K_  -   X_     �_  	   �_     �_  a   �_  	   `     $`     4`     E`     Z`     m`     �`  (   �`     �`  8   �`  ?   a  (   Ka  ^   ta  8   �a  !   b    .b  \   >c     �c     �c      �c  F   �c  !   d  $   Ad  $   fd  "   �d      �d     �d  F   �d  H   4e  |   }e     �e  !    f  X   "f  $   {f  >   �f     �f     �f     g  4   
g  �   ?g  ,   /h  �   \h  �   !i  X   �i     7j     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 
                                            100%% Soukromé poštovní schránky s plným vlastnictvím!
                                         
                                            Generování falešných e-mailů jedním kliknutím
                                         
                                            Skryjte se před spamem díky rozšířenému soukromí a zabezpečení
                                         
                                        Klikněte na „Odstranit“ na domovské stránce a dostanete nový dočasný e-mail.
                                     
                                        E-mailová adresa je platná, dokud ji neodstraníte nebo dokud ji služba nevymaže.
 Životnost závisí na mnoha různých faktorech.
 Můžete mít dočasné e-maily s doživotností až 1 ROK s registrovaným
 účtu.
                                     
                                        Mnoho aplikací a webových stránek vás zavazuje poskytnout osobní e-mailovou adresu bez důvodu
 kromě shromažďování osobních údajů o vás.
 Dočasná e-mailová adresa umožňuje oddělit vaši osobní e-mailovou adresu
 používáte pro bankovnictví, nákupy kreditních karet a další osobní identifikaci
 transakce ze všech ostatních míst venku.
                                     
                                        Odesílání e-mailů je zcela zakázáno a
 nebude nikdy implementována kvůli podvodům a spamu.
                                     
                                        Zobrazují se pod vaším jednorázovým emailem.
 Pokud jste nedostali příchozí e-maily příliš dlouho - klikněte na tlačítko „Obnovit“.
                                     
                                        Můžete jej sdílet pomocí QR kódu na hlavní stránce.
                                     
                                    Můžete mít maximální počet žetonů %(max_tokens_allowed)s.
                                     
                                Nemůžeme doručit e-mail na tuto adresu. (Obvykle kvůli firewally firewally
 nebo filtrování.)
                                 
                                Proč potřebuji %(BRAND_NAME)s účet?
                             
                                Omylem jste nám dal jinou e-mailovou adresu. (Obvykle pracovní nebo osobní
 místo toho, který jste myslel.)
                                 
                            Pokud nevidíte e-mail od nás během několika minut, několik
 Mohly se stát věci:
                             
                            Poslali jsme vám e-mailem pokyny pro nastavení hesla, pokud je účet
 existuje s e-mailem, který jste zadali. Ty
 by měl obdržet krátce.
                         
                        Pokud e-mail neobdržíte, ujistěte se, že jste zadali adresu, kterou jste
 registrovaný
 a zkontrolujte
 spamová složka.
                         
 Platforma UptiMemetrics je navržena tak, aby sledovala stav webových stránek. Zahrnuje takové
 funkce: dostupnost webových stránek,
 dostupnost serveru,
 Kontrola platnosti certifikátu SSL webu a ověření HTML.
  
                    %(BRAND_NAME)s je služba pro jednorázovou e-mailovou adresu. Když navštívíte %(BRAND_NAME)s nový e-mail
 adresa je generována právě pro vás. Vygenerovaná e-mailová adresa může okamžitě přijímat jakékoli e-maily.
 Veškerý e-mail, který je přijat, se okamžitě zobrazí na hlavní stránce. Nikdo jiný než ty neuvidí
 emaily
 které jsou přijaty. Je důležité si uvědomit, že e-mailová adresa vyprší po 24 hodinách. Když je
 e-mailem
 adresa vypršela, e-mailová adresa a všechny přijaté e-maily budou pryč. Nová e-mailová adresa
 bude
 být generovány při návštěvě těchto webových stránek. Existuje mnoho důvodů pro použití jednorázového e-mailu.
 Možná budete chtít někomu dát e-mailovou adresu, aniž byste odhalili vaši identitu. Nebo možná budete chtít
 zaregistrujte se na webové stránky nebo webové služby, ale máte obavy, že webová stránka vám pošle spam
 Budoucnost. <br/>
 Pro používání této služby MUSÍ povolit soubory cookie a javascript, cookie je pouze pro záznam vaší relace
 id
 a jazykové preference, vaše soukromí se vztahuje naše zásady ochrany osobních údajů a souborů cookie.
                 
                    %(BRAND_NAME)s je jednorázová e-mailová adresa. Když navštívíte %(BRAND_NAME)s nový e-mail
 adresa je generována právě pro vás. Vygenerovaná e-mailová adresa může okamžitě přijímat jakékoli e-maily.
 Jakýkoli e-mail, který je přijat, se okamžitě zobrazí na hlavní stránce. Nikdo jiný než vy uvidíte e-maily
 které jsou přijaty. Je důležité si uvědomit, že e-mailová adresa vyprší po 24 hodinách. Když je e-mail
 adresa vypršela, e-mailová adresa a všechny přijaté e-maily budou pryč. Nová e-mailová adresa
 být generovány při návštěvě této webové stránky. Existuje mnoho důvodů pro použití jednorázového e-mailu.
 Možná budete chtít někomu poskytnout e-mailovou adresu, aniž byste odhalili vaši identitu. Nebo možná budete chtít
 zaregistrujte se na webovou stránku nebo webovou službu, ale obáváte se, že vám webová stránka v budoucnu zašle spam. <br />
 Pro používání této služby MUSÍ povolit soubor cookie a javascript, soubor cookie je pouze pro záznam vašeho ID relace
 a jazykové preference, vaše soukromí se vztahuje naše zásady ochrany osobních údajů a souborů cookie.
                 
                Kliknutím na tento odkaz aktivujte svůj účet:
             
            Díky za registraci!
 Váš účet byl vytvořen, můžete se přihlásit poté, co jste aktivovali svůj účet stisknutím URL
 níže.
             Klávesy API O nás Přístupový token Adresa Všechny emaily Všechna práva vyhrazena. Vše, co potřebujete vědět o dočasné pošty Už máte účet? Při pokusu o přihlášení prostřednictvím účtu sociální sítě došlo k chybě. Opravdu chcete odstranit e-mail Opravdu chcete odstranit tento token. Zpět na seznam Změnit Zkontrolujte si e-mail. Zavřít Potvrdit heslo Kontaktujte nás Zásady souborů cookie Zásady cookies Zkopírováno! Kopírovat Kopírovat e-mailovou adresu z horního vstupu Vytvořit Vytvořit e-mail Datum Smazat Odstranit e-mail Odstranit token Demo Doručená pošta Nedostal jste e-mail? Jednorázový e-mail Doména Nemáte účet? E-mail Aktivace e-mailu byla dokončena E-mail Již existuje. Přijaté e-maily Zadejte e-mailovou adresu Stránka Záznamy Platnost vyprší FAQ zpětná vazba Zapomněli jste heslo? Často kladené dotazy Od JÍT Generovat Získejte svůj dočasný e-mail zdarma ještě dnes! Získejte dočasný e-mail ještě dnes Získat mou e-mailovou adresu Jít Ahoj, rád bych Jak prodloužím životnost e-mailové adresy? Jak mohu sdílet dočasný e-mail? Jak snadné to bylo použitelné? Jak zkontrolovat přijaté e-maily? Jak odstranit jednorázový e-mail a získat nový? Jak poslat e-mail? Jak používat jednorázový dočasný e-mail? Jak ji používat Přijímám Doručená pošta Doručená pošta Pojďme si promluvit Načítání dočasné e-mailové adresy Přihlásit se Přihlásit se Přihlaste se ke svému účtu Odhlášení Další staré položky Novější položky Nyní se můžete přihlásit do svého účtu a začít používat službu. Naše kontakty Naše robustní a bezpečné služby vám umožní získat zcela anonymní a soukromé
 e-mailovou adresu, kterou
 může okamžitě použít.
 Ve skutečnosti si můžete vytvořit vlastní dočasnou e-mailovou adresu a používat ji kdykoli
 Chci. Naše robustní a bezpečná služba vám umožní získat zcela anonymní a soukromou e-mailovou adresu, kterou jste
 lze použít okamžitě.
 Ve skutečnosti si můžete vytvořit vlastní dočasnou e-mailovou adresu a používat ji kdykoli budete chtít. Heslo Reset hesla Přejděte na následující stránku a zvolte nové heslo: Populární články Zásady ochrany osobních údajů Projekty testované s falešnými schránkami Obsah surového Znovu odeslat aktivační Přečtěte si příchozí e-maily na této stránce v sekci Doručená pošta Obnovit Registrace dokončena Obnovit heslo Resetovat heslo odkaz bude odeslán na váš e-mail. Ušetřete čas tím, že nikdy nebudete muset vytvářet e-mailovou adresu. Naskenujte QR kód pomocí mobilního telefonu Odeslat zpětnou vaz Odesílatel Sdílená schránka Ukažte nám trochu lásky doporučením vaší oblíbené sítě nebo komunity! Zaregistrujte se Zaregistrujte se Zaregistrujte se zdarma! Přihláška Selhání přihlášení do sociální sítě Zjištěny spam e-maily Bez spamu Začněte nyní Zastavte příjem odpadkových e-mailů a zapomeňte na tlačítka „filtry“ a „odhlásit“ Předmět Povězte o nás Dočasný e-mail Dočasná e-mailová Smluvní podmínky Podmínky použití Díky za registraci! Děkujeme za použití našich stránek! Tým %(site_name)s Účet, který jste se pokusili aktivovat, je neplatný. Účet, který jste se pokusili aktivovat, již byl aktivován. Zadaný aktivační klíč je neplatný. Zadaná e-mailová adresa měla chybu nebo překlepy. Stává se to i těm nejlepším z nás. E-mail je ve složce spamu. Někdy se tam věci ztratí. Platnost tohoto účtu vypršela. Tento trend dokazuje, že pomocí odkazů získávají operátoři škodlivých kampaní více kliknutí a infekcí ve srovnání s klasickou technikou připojení souborů k e-mailům. Je zřejmé, že uživatelé se stali nedůvěrou k jakýmkoli přílohám v poště. Použijte tuto možnost pro registraci na webových stránkách, sociálních médiích atd. Užitečné odkazy Zobrazit Čekání na příchozí e-maily Zaměřujeme se na to, abychom vás udrželi v bezpečí na internetu. Opravdu si toho vážíme, díky! Co je jednorázový dočasný e-mail Kdo jste a jak vám můžeme pomoci? Proč potřebuji dočasnou poštu? Doporučili byste naše služby? Momentálně nemáš žetony. Nechcete sdílet svou osobní e-mailovou adresu? Jste unaveni z
 Spam? Nechcete sdílet svou osobní e-mailovou adresu? Jste unavený ze spamu? Tento e-mail obdržíte, protože jste požádali o obnovení hesla pro svůj uživatelský účet na adrese
 %(site_name)s. Váš Váš bezplatný dočasný e-mail Vaši přátelé a kolegové mohou získat okamžité zobrazení příchozích e-mailů. Vaše doručená pošta je prázdná Vaše uživatelské jméno, v případě, že jste zapomněli: před miliardy poštovních schránek kód navržen tak, aby vyřešil všechny tyto problémy. odborníci spočítali, že 87%% veškerého e-mailového spamu odeslaného ve druhém čtvrtletí roku 2020 (duben, květen a červen měsíců) obsahovalo odkazy ke stažení škodlivých souborů, ale ne přílohy se samotnými soubory. bezplatné monitorování webových stránek je adresa, která je vydána na krátkou dobu: od několika minut do celého měsíce a pak bude smazána se všemi příchozích dopisů a příloh nebo prostě nedostupná pro další použití. platforma je speciálně navržena tak, aby vám pomohla zapomenout na spam, reklamní zásilky, hackování a útočení robotů. Udržujte svou skutečnou schránku čistou a bezpečnou. poskytuje dočasnou, bezpečnou, anonymní, bezplatnou, jednorázovou e-mailovou adresu. Přihlaš se 