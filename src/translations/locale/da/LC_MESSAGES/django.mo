��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  B  R;  �   �<  ~   =  �   �=  �   *>  /  �>  �  �?  �   �A  �   aB  �   #C  z   �C  �   !D  l   �D  �   CE  �   �E  �   �F  �   lG  �   "H  7  I  G  DM  L   �Q  �   �Q     �R     �R     �R     �R     �R     �R  +   �R     �R  S   S  )   mS  /   �S     �S     �S     �S     �S     �S  
   
T     T     $T  	   4T     >T  /   ET     uT     {T     �T     �T     �T  
   �T     �T     �T     �T     �T     �T     U     
U     "U     :U     KU     bU     qU     zU     �U     �U     �U     �U     �U     �U  )   �U  !   
V     ,V     CV     GV  4   ^V  %   �V     �V  #   �V  0   �V     *W  .   DW     sW     �W     �W  	   �W     �W  -   �W     �W     �W     �W     
X     X     &X  @   3X     tX  �   �X  �   gY     PZ     \Z  2   pZ     �Z     �Z  %   �Z     �Z     �Z  :   [     P[     X[     o[  8   �[  7   �[     �[     \      \     )\  X   7\     �\     �\     �\     �\  #   �\     �\      ]     ]  e   ]     {]     �]     �]     �]     �]     �]     �]  %   ^     '^  0   <^  B   m^  2   �^  W   �^  H   ;_     �_    �_  A   �`     �`     	a     a  O   *a  &   za  "   �a  &   �a  *   �a     b  #   5b  K   Yb  C   �b  x   �b     bc     fc  I   �c     �c  "   �c     d     d     $d  +   )d  �   Ud     )e  �   He  �   f  A   �f      g     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                            100%% Private postkasser med fuldt ejerskab!
                                         
                                            Generer falske e-mail med et enkelt klik
                                         
                                            Skjul dig selv fra spam med forbedret privatliv og sikkerhed
                                         
                                        Klik på „Slet“ på hjemmesiden, og du vil få ny midlertidig e-mail.
                                     
                                        E-mail-adressen er gyldig, indtil du sletter den, eller indtil tjenesten sletter den.
 Levetiden afhænger af mange forskellige faktorer.
 Du kan have midlertidige e-mails med levetid på op til 1 ÅR med registreret
 -konto.
                                     
                                        Mange apps og websteder forpligter dig til at give personlig e-mailadresse uden grund
 andet end at indsamle personlige oplysninger om dig.
 Midlertidig e-mailadresse giver dig mulighed for at adskille din personlige e-mailadresse
 du bruger til bank-, kreditkortkøb og anden personlig identifikation
 transaktioner fra alle de andre steder derude.
                                     
                                        Afsendelse af e-mail er helt deaktiveret og
 det vil aldrig blive gennemført på grund af svig og spam spørgsmål.
                                     
                                        De vises under din engangs email.
 Hvis du ikke fik indgående e-mails for længe - klik på knappen „Opdater“.
                                     
                                        Du kan dele det ved at bruge QR-kode på hovedsiden.
                                     
                                    Du kan have maks. %(max_tokens_allowed)s tokens.
                                     
                                Vi kan ikke levere e-mailen til denne adresse. (Normalt på grund af virksomhedernes firewalls
 eller filtrering.)
                                 
                                Hvorfor har jeg brug for %(BRAND_NAME)s konto?
                             
                                Du gav os ved et uheld en anden e-mailadresse. (Normalt et arbejde eller en personlig
 i stedet for den, du mente.)
                                 
                            Hvis du ikke kan se en e-mail fra os inden for få minutter, kan du få et par
 ting kunne være sket:
                             
                            Vi har sendt dig instruktioner til at indstille din adgangskode, hvis en konto
 findes med den e-mail, du har indtastet. Dig
 bør modtage dem om kort tid.
                         
                        Hvis du ikke modtager en e-mail, skal du sørge for, at du har indtastet den adresse, du
 registreret
 med, og tjek din
 spammappe.
                         
 UpTimeMetrics platform er designet til at overvåge status for webstederne. Det omfatter sådanne
 funktioner: Webstedets tilgængelighed
 Servertilgængelighed,
 Gyldighedskontrol af webstedets SSL-certifikat og HTML-validering.
  
                    %(BRAND_NAME)s er en engangs e-mail-adressetjeneste. Når du besøger %(BRAND_NAME)s en ny e-mail
 adresse genereres kun for dig. Den genererede e-mail-adresse kan straks modtage e-mails.
 Enhver e-mail, der modtages, vises på hovedsiden med det samme. Ingen andre end du vil se
 e-mails
 der modtages. Det er vigtigt at bemærke, at en e-mail-adresse udløber efter 24 timer. Når en
 e-mail
 adresse er udløbet, e-mail-adressen og eventuelle modtagne e-mails vil være væk. En ny e-mailadresse
 vilje
 blive genereret ved at besøge denne hjemmeside. Der er mange grunde til at bruge en engangs e-mail.
 Du ønsker måske at give nogen en e-mail-adresse uden at afsløre din identitet. Eller du måske ønsker at
 tilmeld dig en hjemmeside eller webservice, men du er bekymret for, at hjemmesiden vil sende dig spam i
 fremtiden. <br/>
 For at bruge denne tjeneste skal du aktivere cookie og javascript, cookie er bare at registrere din session
 id
 og sprogpræferencer, dit privatliv er omfattet af vores Privatlivs- og cookiepolitik.
                 
                    %(BRAND_NAME)s er en engangs e-mailadresse tjeneste. Når du besøger %(BRAND_NAME)s en ny e-mail
 -adressen genereres kun for dig. Den genererede e-mail-adresse kan straks modtage e-mails.
 Enhver e-mail, der modtages, vises straks på hovedsiden. Ingen andre end dig vil se e-mailene
 , der modtages. Det er vigtigt at bemærke, at en e-mail-adresse udløber efter 24 timer. Når en e-mail
 -adressen er udløbet, vil e-mailadressen og eventuelle modtagne e-mails være væk. En ny e-mailadresse vil
 genereres, når du besøger denne hjemmeside. Der er mange grunde til at bruge en engangs email.
 Du ønsker måske at give nogen en e-mailadresse uden at afsløre din identitet. Eller du vil måske
 tilmelde dig en hjemmeside eller en webtjeneste, men du er bekymret for, at hjemmesiden vil sende dig spam i fremtiden. <br />
 For at bruge denne tjeneste, SKAL du aktivere cookie og javascript, cookie er blot at optage dit sessions-id
 og sprogpræferencer, er dit privatliv dækket af vores politik om beskyttelse af personlige oplysninger og cookies.
                 
                Klik på dette link for at aktivere din konto:
             
            Tak fordi du tilmeldte dig!
 Din konto er blevet oprettet, du kan logge ind efter du har aktiveret din konto ved at trykke på url
 nedenfor.
             API-nøgler Om os Adgang token Adresse Alle e-mails Alle rettigheder forbeholdes. Alt du behøver at vide om midlertidig post Har du allerede en konto? Der opstod en fejl under forsøget på at logge ind via din sociale netværkskonto. Er du sikker på, at du vil slette e-mail Er du sikker på, at du vil slette dette token. Tilbage til listen Skift Tjek din e-mail. Luk Bekræft adgangskode Kontakt os Cookie politik Cookies politik Kopieret! Kopier Kopiér e-mail-adresse fra den øverste indgang Opret Opret e-mail Dato Slet Slet e- mail Slet token Demo Indbakke Fik du ikke en e-mail? Email til engangsbrug Domæne Har du ikke en konto? E-mail Aktivering af e-mail er E-mail findes allerede. Modtagne e-mails Indtast e-mail-adresse Siden Indgange Udløber OFTE STILLEDE SPØRGSMÅL tilbagemelding Glemt adgangskode? Ofte stillede spørgsmål Fra GÅ Generer Få din gratis midlertidige e-mail i dag! Få din midlertidige e-mail i dag Hent min e-mailadresse Gå Hej der, Jeg vil gerne Hvordan forlænger jeg levetiden for e-mailadressen? Hvordan deler jeg midlertidig e-mail? Hvor nemt var det at bruge? Sådan tjekker du modtagne e-mails? Sådan sletter du engangs email og får den nye? Hvordan sender du e-mail? Hvordan man bruger engangs midlertidig e-mail? Sådan bruger du det Jeg accepterer Indbakke Indbakker Lad os tale Indlæsning af din midlertidige e-mailadresse Log ind Log ind Log ind på din konto Logout Flere gamle indgange Nyere poster Nu kan du logge ind på din konto og begynde at bruge tjenesten. Vores Kontakter Vores robuste og sikre service giver dig mulighed for at blive helt anonym og privat
 e-mailadresse, som du
 kan bruge med det samme.
 Faktisk kan du oprette din egen midlertidige e-mail-adresse og bruge den, når du
 ønsker. Vores robuste og sikre service giver dig mulighed for at få fuldstændig anonym og privat en e-mailadresse, som du
 kan anvendes med det samme.
 Faktisk kan du oprette din egen midlertidige e-mail-adresse og bruge den, når du vil. Adgangskode Nulstil adgangskode Gå til følgende side og vælg en ny adgangskode: Populære artikler Privatlivspolitik Projekter testet med falske indbakker Råt indhold Send aktiveringsmail igen Læs indgående e-mails på denne side i indbakken sektion Opdater Registrering fuldført Nulstil adgangskode Nulstil adgangskode link vil blive sendt på din e-mail. Spar tid ved aldrig at skulle oprette en e-mailadresse. Scan QR-kode med mobiltelefon Send feedback Afsender Delt indbakke Vis os lidt kærlighed ved at anbefale os til dit foretrukne netværk eller fællesskab! Tilmeld dig Tilmeld dig Tilmeld dig gratis! Tilmeld Fejl ved login til sociale netværk Spa-e-mails registreret Spam gratis Start nu Stop med at modtage papirkurven e-mails og glem alt om indbakken „filtre“ og „afmeld“ knapper Emne Fortæl om os Midlertidig e-mail Midlertidig e-mail-tjeneste Vilkår og betingelser Vilkår for brug Tak fordi du tilmeldte dig! Tak fordi du bruger vores hjemmeside! %(site_name)s holdet Den konto, du forsøgte at aktivere, er ugyldig. Den konto, du forsøgte at aktivere, er allerede blevet aktiveret. Den aktiveringsnøgle, du har angivet, er ugyldig. Den indtastede e-mailadresse havde en fejl eller tastefejl. (Sker for de bedste af os.) E-mailen findes i din spammappe. Nogle gange går tingene tabt derinde.) Denne konto er udløbet. Denne tendens viser, at operatører af ondsindede kampagner ved hjælp af links modtager flere klik og infektioner sammenlignet med den klassiske teknik til at vedhæfte filer til e-mails. Det er klart, at brugerne er blevet mistroiske over for eventuelle vedhæftede filer i posten. Brug dette til at tilmelde dig på websteder, sociale medier osv. Nyttige Links Se Venter på indgående e-mails Vi sigter højt mod at være fokuseret på at holde dig sikker på internettet. Vi sætter virkelig pris på det, tak! Hvad er engangs midlertidig e-mail Hvem er du, og hvordan kan vi hjælpe? Hvorfor har jeg brug for midlertidig post? Vil du anbefale vores service? Du har ikke poletter i øjeblikket. Du ønsker ikke at dele din personlige e-mailadresse? Er du træt af
 spam? Vil du ikke dele din personlige e-mailadresse? Er du træt af spam? Du modtager denne e-mail, fordi du har anmodet om en nulstilling af adgangskoden til din brugerkonto på
 %(site_name)s. Din Din gratis midlertidige e-mail Dine venner og kolleger kan få øjeblikkelig visning af indgående mail. Din indbakke er tom Dit brugernavn, hvis du har glemt: siden milliarder af indbakker kode designet til at løse alle disse problemer. eksperter beregnede, at 87%% af al e-mail-spam, der blev sendt i andet kvartal af 2020 (april, maj og juni måned), indeholdt links til download af ondsindede filer, men ikke vedhæftede filer med selve filerne. gratis hjemmeside overvågning er en adresse, der udstedes i kort tid: fra et par minutter til en hel måned, og så bliver den slettet med alle indgående breve og vedhæftede filer eller simpelthen utilgængelig til videre brug. -platformen er specielt designet til at hjælpe dig med at glemme alt om spam, reklameforsendelser, hacking og angribende robotter. Hold din rigtige postkasse ren og sikker. giver midlertidig, sikker, anonym, gratis, engangs e-mailadresse. tilmeld dig 