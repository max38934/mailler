��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  B  R;  �   �<  �   =  �   �=  �   @>  C  �>  �  2@  �   B  �   �B  �   �C  �   cD  �   �D  i   �E  �   F  �   �F  �   |G  �   fH    .I  �  2J  �  �N  \   �S  �   T     �T  	   �T     �T     �T     �T     U  5   U     SU  [   lU  3   �U  9   �U     6V     HV     PV  
   jV     uV     �V     �V     �V     �V     �V  3   �V  	   W     W      W     &W     /W     ?W     NW     _W     ~W     �W     �W     �W      �W     �W     �W       X     !X  	   7X     AX     EX     NX     bX     {X     X  
   �X  <   �X  0   �X     �X     Y      Y  7   3Y  !   kY      �Y  '   �Y  4   �Y     Z  *   &Z     QZ     dZ     wZ     �Z     �Z  (   �Z  	   �Z  	   �Z     �Z     �Z     [     [  E   +[     q[    �[    �\     �]     �]  J   �]     �]     ^  *   (^  
   S^  !   ^^  F   �^     �^     �^     �^  G   _  N   J_  ,   �_     �_     �_     �_  k   �_     a`     o`     }`     �`  $   �`     �`  	   �`     �`  t   �`     ga     oa     a     �a      �a     �a     �a  %   b     'b  6   >b  ?   ub  >   �b  o   �b  W   dc     �c     �c  I   �d     De     Ue     ]e  P   ze  ,   �e      �e  '   f  "   Af  ,   df     �f  X   �f  W   	g  p   ag     �g  !   �g  B   �g     =h  ,   Vh     �h     �h     �h  ,   �h  �   �h  !   �i  �   �i  �   �j  Q   sk  	   �k     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                            100%% private Postfächer mit vollem Besitz!
                                         
                                            Generieren Sie gefälschte E-Mails mit einem Klick
                                         
                                            Verstecken Sie sich mit verbessertem Datenschutz und Sicherheit vor Spam
                                         
                                        Klicken Sie auf der Startseite auf „Löschen“ und Sie erhalten eine neue temporäre E-Mail.
                                     
                                        Die E-Mail-Adresse ist gültig, bis Sie sie löschen oder bis der Dienst sie löscht.
 Die Lebensdauer hängt von vielen verschiedenen Faktoren ab.
 Sie können temporäre E-Mails mit einer Lebensdauer von bis zu 1 Jahr mit registrierten
 konto.
                                     
                                        Viele Apps und Websites verpflichten Sie, ohne Grund eine persönliche E-Mail-Adresse anzugeben
 außer um personenbezogene Daten über Sie zu sammeln.
 Temporäre E-Mail-Adresse ermöglicht es Ihnen, Ihre persönliche E-Mail-Adresse zu trennen
 Sie verwenden für Bankgeschäfte, Kreditkartenkäufe und andere persönliche Identifizierung
 Transaktionen von allen anderen Websites da draußen.
                                     
                                        Das Versenden von E-Mails ist vollständig deaktiviert und
 es wird aufgrund von Betrugs- und Spam-Problemen niemals implementiert.
                                     
                                        Sie werden unter Ihrer Einweg-E-Mail angezeigt.
 Wenn Sie eingehende E-Mails nicht zu lange erhalten haben, klicken Sie auf die Schaltfläche „Aktualisieren“.
                                     
                                        Sie können es teilen, indem Sie QR-Code auf der Hauptseite verwenden.
                                     
                                    Sie können maximal %(max_tokens_allowed)s Token haben.
                                     
                                Wir können die E-Mail nicht an diese Adresse liefern. (Normalerweise aufgrund von Unternehmens-Firewalls
 oder filtern.)
                                 
                                Warum brauche ich ein %(BRAND_NAME)s Konto?
                             
                                Sie haben uns versehentlich eine andere E-Mail-Adresse gegeben. (Normalerweise ein geschäftlicher oder persönlicher
 statt dem, den du meintest.)
                                 
                            Wenn Sie innerhalb weniger Minuten keine E-Mail von uns sehen,
 Dinge hätten passieren können:
                             
                            Wir haben Ihnen Anweisungen zum Festlegen Ihres Passworts per E-Mail zugesandt, wenn ein Konto
 existiert mit der von Ihnen eingegebenen E-Mail. Du
 sollte sie in Kürze erhalten.
                         
                        Wenn Sie keine E-Mail erhalten, stellen Sie bitte sicher, dass Sie die Adresse eingegeben haben
 registriert
 mit und überprüfe deine
 Spam-Ordner.
                         
 Die uptiMETrics-Plattform wurde entwickelt, um den Status der Websites zu überwachen. Es enthält solche
 Funktionen: Verfügbarkeit der Website,
 Verfügbarkeit von Servern,
 Gültigkeitsprüfung des SSL-Zertifikats und der HTML-Validierung der Website.
  
                    %(BRAND_NAME)s ist ein Einweg-E-Mail-Adressdienst. Wenn du eine neue E-Mail besuchst
 Adresse wird nur für Sie generiert. Die generierte E-Mail-Adresse kann sofort alle E-Mails erhalten.
 Jede eingegangene E-Mail wird sofort auf der Hauptseite angezeigt. Niemand außer dir wird es sehen
 die E-Mails
 die empfangen werden. Es ist wichtig zu beachten, dass eine E-Mail-Adresse nach 24 Stunden abläuft. Wenn ein
 E-Mail senden
 Adresse ist abgelaufen, die E-Mail-Adresse und alle empfangenen E-Mails sind verschwunden. Eine neue E-Mail-Adresse
 Testament
 beim Besuch dieser Website generiert werden. Es gibt viele Gründe, eine Einweg-E-Mail zu verwenden.
 Möglicherweise möchten Sie jemandem eine E-Mail-Adresse geben, ohne Ihre Identität preiszugeben. Oder du möchtest vielleicht
 melden Sie sich für eine Website oder einen Webdienst an, aber Sie befürchten, dass die Website Ihnen Spam sendet
 die Zukunft. <br/>
 Um diesen Dienst nutzen zu können, MÜSSEN Sie Cookie und Javascript aktivieren, Cookie dient nur dazu, Ihre Sitzung aufzuzeichnen
 id
 und Sprachpräferenz, Ihre Privatsphäre wird durch unsere Datenschutz- und Cookie-Richtlinie abgedeckt.
                 
                    %(BRAND_NAME)s ist ein Wegwerf-E-Mail-Adressdienst. Wenn Sie %(BRAND_NAME)s eine neue E-Mail besuchen
 Adresse wird nur für Sie generiert. Die generierte E-Mail-Adresse kann sofort alle E-Mails erhalten.
 Jede E-Mail, die empfangen wird, wird sofort auf der Hauptseite angezeigt. Niemand anderes als Sie wird die E-Mails sehen
 die erhalten werden. Es ist wichtig zu beachten, dass eine E-Mail-Adresse nach 24 Stunden abläuft. Wenn eine E-Mail
 Die Adresse ist abgelaufen, die E-Mail-Adresse und alle empfangenen E-Mails sind verschwunden. Eine neue E-Mail-Adresse wird
 beim Besuch dieser Website generiert werden. Es gibt viele Gründe, eine Einweg-E-Mail zu verwenden.
 Möglicherweise möchten Sie jemandem eine E-Mail-Adresse geben, ohne Ihre Identität preiszugeben. Oder du möchtest vielleicht
 melden Sie sich für eine Website oder einen Webdienst an, aber Sie befürchten, dass die Website Ihnen in Zukunft Spam zusenden wird. <br />
 Für die Nutzung dieses Dienstes MÜSSEN Sie Cookie und Javascript aktivieren, Cookie dient nur zum Aufzeichnen Ihrer Sitzungs-ID
 und Ihre bevorzugte Sprache, Ihre Privatsphäre wird unter unsere Datenschutz- und Cookie-Richtlinie abgedeckt.
                 
                Bitte klicken Sie auf diesen Link, um Ihr Konto zu aktivieren:
             
            Danke fürs Anmelden!
 Ihr Konto wurde erstellt, Sie können sich anmelden, nachdem Sie Ihr Konto aktiviert haben, indem Sie die URL drücken
 unten.
             API-Schlüssel Über uns Zugreifen auf Token Adresse Alle E-Mails Alle Rechte vorbehalten. Alles was Sie über temporäre E-Mails wissen müssen Hast du schon ein Konto? Beim Versuch, sich über Ihr soziales Netzwerkkonto anzumelden, ist ein Fehler aufgetreten. Sind Sie sicher, dass Sie E-Mails löschen möchten Sind Sie sicher, dass Sie dieses Token löschen möchten? Zurück zur Liste ändern Überprüfe deine E-Mail. Schliessen Passwort bestätigen Kontaktiere uns Cookie Richtlinie Cookies-Richtlinie Kopiert! Kopieren Kopiere die E-Mail-Adresse von der obersten Eingabe Erstellen E-Mail erstellen Datum löschen E-Mail löschen Token löschen Demo-Posteingang Hast du keine E-Mail bekommen? Wegwerf-E-Mail Domäne Haben Sie kein Konto? E-mail E-Mail-Aktivierung abgeschlossen E-Mail existiert bereits. Empfangene E-Mails Geben Sie die E-Mail-Adresse ein Seite „Einträge“ Läuft ab FAQ Feedback Passwort vergessen? Häufig gestellte Fragen Von GEHEN Generieren Holen Sie sich noch heute Ihre kostenlose temporäre E-Mail! Holen Sie sich noch heute Ihre temporäre E-Mail Hol dir meine E-Mail-Adresse Gehe Hi da, ich möchte Wie verlängere ich die Lebensdauer der E-Mail-Adresse? Wie teile ich temporäre E-Mails? Wie einfach war das zu benutzen? Wie überprüfe ich empfangene E-Mails? Wie lösche ich Einweg-E-Mails und bekomme die neue? Wie sende ich eine E-Mail? Wie benutzt man temporäre Einweg-E-Mails? Wie benutzt man es Ich akzeptiere das Posteingang Posteingänge Lass uns reden Laden Sie Ihre temporäre E-Mail-Adresse Einloggen Einloggen Melde dich bei deinem Konto an Logout Weitere alte Einträge Neuere Einträge Jetzt können Sie sich in Ihrem Konto anmelden und den Dienst nutzen. Unsere Kontakte Unser robuster und sicherer Service ermöglicht es Ihnen, völlig anonym und privat zu werden
 E-Mail-Adresse, die Sie
 kann sofort verwendet werden.
 Eigentlich können Sie Ihre eigene temporäre E-Mail-Adresse erstellen und diese jederzeit verwenden
 wollen. Unser robuster und sicherer Service ermöglicht es Ihnen, eine E-Mail-Adresse, die Sie haben, völlig anonym und privat zu erhalten
 kann sofort verwenden.
 Eigentlich können Sie Ihre eigene temporäre E-Mail-Adresse erstellen und verwenden, wann immer Sie möchten. Passwort Passwort zurücksetzen Bitte gehen Sie auf die folgende Seite und wählen Sie ein neues Passwort: Populäre Artikel Datenschutz-Bestimmungen Projekte, die mit gefälschten Posteingän Roh-Inhalt Aktivierungs-E-Mail erneut senden Lesen Sie eingehende E-Mails auf dieser Seite im Abschnitt Posteingang Auffrischen Anmeldung abgeschlossen Passwort zurücksetzen Der Link zum Zurücksetzen des Passworts wird auf Ihre E-Mail gesendet. Sparen Sie Ihre Zeit, indem Sie niemals eine E-Mail-Adresse erstellen müssen. Scannen Sie den QR-Code mit dem Mobiltelefon Feedback senden Absender Geteilter Posteingang Zeigen Sie uns etwas Liebe, indem Sie uns Ihrem Lieblingsnetzwerk oder Ihrer Lieblings-Community empfehlen! Melde dich an Melde dich an Melde dich kostenlos an! Anmelden Anmeldefehler in sozialen Netzwerken Erkannte Spam-E-Mails Spam-frei Beginne jetzt Hören Sie auf, Mülleimer-E-Mails zu erhalten, und vergessen Sie die Schaltflächen „Filter“ und „Abmelden“ Betreff Erzähl von uns Vorübergehende E-Mail Vorübergehender E-Mail-Service Allgemeine Geschäftsbedingungen Nutzungsbedingungen Danke fürs Anmelden! Danke für die Nutzung unserer Seite! Das %(site_name)s Team Das Konto, das Sie aktivieren möchten, ist ungültig. Das Konto, das Sie aktivieren wollten, wurde bereits aktiviert. Der von Ihnen angegebene Aktivierungsschlüssel ist ungültig. Die von Ihnen eingegebene E-Mail-Adresse hatte einen Fehler oder Tippfehler. (Das passiert den Besten von uns.) Die E-Mail befindet sich in Ihrem Spam-Ordner. (Manchmal gehen Dinge da drin verloren.) Dieses Konto ist abgelaufen. Dieser Trend beweist, dass Betreiber bösartiger Kampagnen durch die Verwendung von Links mehr Klicks und Infektionen erhalten als mit der klassischen Technik, Dateien an E-Mails anzuhängen. Offensichtlich sind die Benutzer misstrauisch gegenüber allen Anhängen in der E-Mail geworden. Verwenden Sie dies, um sich auf Websites, sozialen Medien usw. anzumelden nützliche Links Ansicht Warte auf eingehende E-Mails Wir zielen darauf ab, uns darauf zu konzentrieren, Sie im Internet zu schützen. Wir wissen das wirklich zu schätzen, danke! Was ist temporäre Einweg-E-Mail Wer bist du und wie können wir helfen? Warum brauche ich temporäre Post? Würden Sie unseren Service weiterempfehlen? Im Moment hast du keine Token. Sie möchten Ihre persönliche E-Mail-Adresse nicht teilen? Bist du müde von der
 Spam? Sie möchten Ihre persönliche E-Mail-Adresse nicht teilen? Bist du müde von dem Spam? Sie erhalten diese E-Mail, weil Sie ein Zurücksetzen des Kennworts für Ihr Benutzerkonto unter
 %(site_name)s. Deine Ihre kostenlose temporäre E-Mail Ihre Freunde und Kollegen können sofort eingehende E-Mails sehen. Ihr Posteingang ist leer Ihr Benutzername, falls Sie vergessen haben: vor Milliarden von Posteingängen Code entwickelt, um all diese Probleme zu lösen. Experten berechneten, dass 87%% des gesamten E-Mail-Spams, der im zweiten Quartal 2020 (April, Mai und Juni) gesendet wurde, Links zum Herunterladen bösartiger Dateien enthielt, jedoch keine Anhänge mit den Dateien selbst. kostenlose Webseiten-Überwachung ist eine Adresse, die für kurze Zeit ausgestellt wird: von ein paar Minuten bis zu einem ganzen Monat, und dann wird sie mit allen eingehenden Briefen und Anhängen gelöscht oder ist einfach nicht für die weitere Verwendung verfügbar. Die Plattform wurde speziell entwickelt, um Ihnen zu helfen, Spam, Werbemailings, Hacker- und Angriffsroboter zu vergessen. Halte dein echtes Postfach sauber und sicher. bietet eine temporäre, sichere, anonyme, kostenlose, verfügbare E-Mail-Adresse. Anmeldung 