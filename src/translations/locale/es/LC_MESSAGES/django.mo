��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  B  R;  �   �<  �   =  �   �=  �   ,>  T  �>    4@  �   KB  �   C  �   D  �   �D  �   #E  l   �E  �   OF  �   G  �   �G  �   �H  	  qI  "  {J  H  �O  M   �T  �   5U  
   �U     �U     �U  
   �U     	V     )V  4   HV     }V  Z   �V  >   �V  3   /W     cW     uW     |W     �W     �W     �W     �W     �W  
   �W     �W  B    X     CX     IX     cX     iX     pX     �X     �X  &   �X     �X     �X     Y     Y  -   Y  !   KY     mY  ,   �Y     �Y     �Y     �Y     �Y     �Y     Z     -Z     3Z     6Z  <   >Z  0   {Z  ,   �Z     �Z     �Z  G   �Z  1   7[     i[  4   �[  F   �[  &   \  <   *\     g\  	   t\     ~\     �\     �\  6   �\     �\     �\     ]     "]     1]     H]  K   `]     �]    �]    �^     �_     �_  :   `     B`     W`  1   o`     �`  +   �`  g   �`  
   La     Wa     oa  M   �a  H   �a  -   b     Lb  	   _b     ib  O   �b     �b     �b     �b     c  ,   c  %   >c     dc     rc  �   �c     d     d     -d  (   Jd     sd     �d     �d  !   �d     �d  2   �d  3   $e  ;   Xe  �   �e  l   f     �f  +  �f  A   �g     h     !h  )   %h  @   Oh  #   �h  2   �h  %   �h  $   i  "   2i      Ui  e   vi  `   �i  �   =j     �j  (   �j  \   �j  "   Mk  6   pk     �k  (   �k     �k  .   �k  �   l      �l  �   	m  �   �m  d   xn     �n     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                            100%% Buzones privados con plena propiedad.
                                         
                                            Genera correos electrónicos falsos con un clic
                                         
                                            Escóndete del spam con privacidad y seguridad mejoradas
                                         
                                        Haz clic en «Eliminar» en la página de inicio y recibirás un nuevo correo electrónico temporal.
                                     
                                        La dirección de correo electrónico es válida hasta que la elimines o hasta que el servicio la elimine.
 La vida útil depende de muchos factores diferentes.
 Puede tener correos electrónicos temporales con una vida útil de hasta 1 AÑO con registro
 cuenta.
                                     
                                        Muchas aplicaciones y sitios web le obligan a proporcionar una dirección de correo electrónico personal sin motivo alguno.
 aparte de recopilar información personal sobre usted.
 La dirección de correo electrónico temporal le permite separar su dirección de correo electrónico personal
 que utiliza para operaciones bancarias, compras con tarjeta de crédito y otras identificaciones personales
 transacciones de todos los demás sitios que existen.
                                     
                                        El envío de correo electrónico está completamente desactivado y
 nunca se implementará debido a problemas de fraude y spam.
                                     
                                        Se muestran bajo su correo electrónico desechable.
 Si no recibiste correos electrónicos entrantes durante demasiado tiempo, haz clic en el botón «Actualizar».
                                     
                                        Puedes compartirlo utilizando el código QR en la página principal.
                                     
                                    Puedes tener un máximo de %(max_tokens_allowed)s fichas.
                                     
                                No podemos enviar el correo electrónico a esta dirección. (Normalmente debido a los cortafuegos corporativos
 o filtrado.)
                                 
                                ¿Por qué necesito una cuenta %(BRAND_NAME)s?
                             
                                Nos diste accidentalmente otra dirección de correo electrónico. (Por lo general, un trabajo o personal)
 en lugar del que quiso decir.)
                                 
                            Si no ves un correo electrónico de nosotros en unos minutos, algunos
 podrían haber ocurrido cosas:
                             
                            Te hemos enviado por correo electrónico instrucciones para configurar tu contraseña, si es una cuenta
 existe con el correo electrónico que has introducido. Tú
 debería recibirlos en breve.
                         
                        Si no recibes un correo electrónico, asegúrate de haber introducido la dirección que
 registrado
 con, y compruebe su
 carpeta de spam.
                         
 La plataforma UptimeMetrics está diseñada para monitorear el estado de los sitios web. Incluye tales
 características: disponibilidad del sitio web,
 Disponibilidad del servidor,
 Comprobación de validez del certificado SSL del sitio web y validación HTML.
  
                    %(BRAND_NAME)s es un servicio de dirección de correo electrónico desechable. Cuando visitas %(BRAND_NAME)s un correo electrónico nuevo
 la dirección se genera solo para usted. La dirección de correo electrónico generada puede recibir inmediatamente cualquier correo electrónico.
 Cualquier correo electrónico que se reciba aparecerá en la página principal de inmediato. Nadie más que tú lo verá
 los correos
 que se reciben. Es importante tener en cuenta que una dirección de correo electrónico caduca después de 24 horas. Cuando un
 correo
 la dirección ha caducado, la dirección de correo electrónico y los correos electrónicos recibidos desaparecerán. Una nueva dirección de correo electrónico
 se
 generarse al visitar este sitio web. Hay muchas razones para usar un correo electrónico desechable.
 Es posible que quieras dar a alguien una dirección de correo electrónico sin revelar tu identidad. O puede que quieras
 se registra en un sitio web o servicio web, pero le preocupa que el sitio web le envíe spam en
 el futuro. <br/>
 Para usar este servicio, DEBE habilitar las cookies y javascript, la cookie es solo para grabar su sesión
 id
 y preferencia de idioma, su privacidad está cubierta por nuestra política de privacidad y cookies.
                 
                    %(BRAND_NAME)s es un servicio de direcciones de correo electrónico desechables. Cuando visitas %(BRAND_NAME)s un nuevo correo electrónico
 la dirección se genera solo para ti. La dirección de correo electrónico generada puede recibir inmediatamente cualquier correo electrónico.
 Cualquier correo electrónico que se reciba aparecerá inmediatamente en la página principal. Nadie más que tú verá los correos electrónicos
 que se reciben. Es importante tener en cuenta que una dirección de correo electrónico caduca transcurridas 24 horas. Cuando un correo electrónico
 la dirección ha caducado, la dirección de correo electrónico y los correos electrónicos recibidos desaparecerán. Una nueva dirección de correo electrónico
 se generarán al visitar este sitio web. Existen muchas razones para utilizar un correo electrónico desechable.
 Es posible que quieras darle a alguien una dirección de correo electrónico sin revelar tu identidad. O tal vez quieras
 suscribirse a un sitio web o servicio web pero le preocupa que el sitio web le envíe spam en el futuro. <br />
 Para utilizar este servicio, DEBE habilitar la cookie y javascript, la cookie es solo para registrar su id de sesión
 y preferencia de idioma, su privacidad está cubierta por nuestra Política de privacidad y cookies.
                 
                Haz clic en este enlace para activar tu cuenta:
             
            ¡Gracias por registrarte!
 Se ha creado tu cuenta, puedes iniciar sesión después de activar tu cuenta pulsando la url
 abajo.
             Claves API Acerca de nosotros Token de acceso Dirección Todos los correos electrónicos Reservados todos los derechos. Todo lo que necesitas saber sobre el correo temporal ¿Ya tienes una cuenta? Se ha producido un error al intentar iniciar sesión a través de su cuenta de red social. ¿Estás seguro de que quieres eliminar el correo electrónico ¿Estás seguro de que quieres eliminar este token? Volver a la lista Cambio Revisa tu correo electrónico. Cerrar Confirmar contraseña Contáctanos Política de cookies Política de Cookies ¡Copiado! Copia Copiar la dirección de correo electrónico de la entrada superior Crear Crear correo electrónico Fecha Borrar Eliminar correo electrónico Eliminar token Bandeja de entrada demo ¿No recibiste un correo electrónico? Correo electrónico desechable Dominio ¿No tienes cuenta? Correo Activación de correo electrónico completada El correo electrónico Ya existe. Correo electrónico recibido Introducir dirección de correo electrónico Página de entradas Caduca PREGUNTAS FRECUENTES Retroalimentación ¿Has olvidado tu contraseña Preguntas frecuentes Desde IR Generar ¡Recibe tu correo electrónico temporal gratuito hoy mismo! Reciba su correo electrónico temporal hoy mismo Obtener mi dirección de correo electrónico Ir Hola, me gustaría ¿Cómo prolongo la vida útil de la dirección de correo electrónico? ¿Cómo comparto el correo electrónico temporal? ¿Qué tan fácil fue usarlo? ¿Cómo revisar los correos electrónicos recibidos? ¿Cómo eliminar el correo electrónico desechable y obtener el nuevo? ¿Cómo enviar un correo electrónico? ¿Cómo utilizar el correo electrónico temporal desechable? Cómo usarlo Acepto el Bandeja de entrada Buzones de entrada Hablemos Carga de tu dirección de correo electrónico temporal Iniciar sesión Inicio de sesión Inicie sesión en su cuenta Cerrar sesión Más entradas antiguas Entradas más recientes Ahora puedes iniciar sesión en tu cuenta y empezar a utilizar el servicio. Nuestros Contactos Nuestro servicio robusto y seguro le permite obtener un servicio completamente anónimo y privado
 dirección de correo electrónico en la que
 se puede usar de inmediato.
 En realidad, puede crear su propia dirección de correo electrónico temporal y usarla siempre que
 quiero. Nuestro servicio robusto y seguro le permite obtener una dirección de correo electrónico completamente anónima y privada que
 se puede usar de inmediato.
 De hecho, puedes crear tu propia dirección de correo electrónico temporal y usarla cuando quieras. Contraseña Restablecimiento de contraseña Vaya a la siguiente página y elija una nueva contraseña: Artículos populares Política de privacidad Proyectos probados con bandejas de entrada falsas Contenido sin procesar Reenviar correo electrónico de activación Leer los correos electrónicos entrantes en esta página dentro de la sección de la bandeja de entrada Actualizar Inscripción completada Restablecer contraseña El enlace para restablecer contraseña se enviará en su correo electrónico. Ahorra tiempo sin tener que crear una dirección de correo electrónico. Escanea el código QR con el teléfono móvil Enviar comentarios Remitente Bandeja de entrada compartida ¡Demuéstranos un poco de amor recomendándonos a tu red o comunidad favorita! Regístrese Regístrese ¡Regístrate gratis! Regístrate Error de inicio de sesión en redes sociales Correos electrónicos spam detectados Libre de spam Empezar ahora Deja de recibir correos electrónicos de basura y olvídate de los botones «filtros» y «cancelar suscripción» de la bandeja de entrada Asunto Cuéntanos sobre nosotros Correo electrónico temporal Servicio de correo electrónico temporal Términos y condiciones Términos de uso ¡Gracias por registrarte! ¡Gracias por usar nuestro sitio! El equipo %(site_name)s La cuenta que has intentado activar no es válida. La cuenta que intentaste activar ya se ha activado. La clave de activación que ha proporcionado no es válida. La dirección de correo electrónico que has introducido ha cometido un error o error tipográfico. (Le pasa a los mejores de nosotros). El correo electrónico se encuentra en la carpeta de correo no deseado. (A veces las cosas se pierden ahí.) Esta cuenta ha caducado. Esta tendencia demuestra que mediante el uso de enlaces, los operadores de campañas maliciosas reciben más clics e infecciones en comparación con la técnica clásica de adjuntar archivos a correos electrónicos. Obviamente, los usuarios han desconfiado de cualquier archivo adjunto en el correo. Utiliza esto para registrarte en sitios web, redes sociales, etc. Enlaces útiles Ver Esperando correos electrónicos entrantes Nuestro objetivo es centrarnos en mantenerte seguro en Internet. Te lo agradecemos mucho, ¡gracias! Qué es el correo electrónico temporal desechable ¿Quién eres y cómo podemos ayudar? ¿Por qué necesito correo temporal? ¿Recomendarías nuestro servicio? No tienes fichas por el momento. ¿No quieres compartir tu dirección de correo electrónico personal? ¿Estás cansado de la
 ¿spam? ¿No quieres compartir tu dirección de correo electrónico personal? ¿Estás cansado del spam? Recibes este correo electrónico porque has solicitado un restablecimiento de contraseña para tu cuenta de usuario en
 %(site_name)s. Su Su correo electrónico temporal gratuito Tus amigos y colegas pueden obtener una vista instantánea del correo electrónico entrante. Su bandeja de entrada está vacía Su nombre de usuario, en caso de que lo haya olvidado: hace miles de millones de bandejas de entrada código diseñado para resolver todos estos problemas. calcularon que el 87%% de todo el correo no deseado enviado en el segundo trimestre de 2020 (meses de abril, mayo y junio) contenía enlaces para descargar archivos maliciosos, pero no adjuntos con los propios archivos. monitoreo gratuito de sitios web es una dirección que se emite por un corto tiempo: de unos minutos a un mes entero, y luego se eliminará con todas las cartas entrantes y archivos adjuntos, o simplemente no disponible para su uso posterior. está específicamente diseñada para ayudarle a olvidarse de spam, correos publicitarios, hackear y atacar robots. Mantenga su buzón real limpio y seguro. proporciona una dirección de correo electrónico temporal, segura, anónima, gratuita y desechable. inscribirse 