��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  B  R;  �   �<  �   +=  �   �=  �   ]>  9  �>  �  1@  �   B  �   �B  �   �C     JD  �   �D  d   }E  �   �E  �   �F  �   HG  �   H  �   �H  �  �I  �  ON  L   S  �   _S     �S     T     T     )T     0T     ET  >   dT     �T  S   �T  '   U  5   5U     kU     U     �U     �U     �U     �U     �U     �U  	   �U     �U  2   V     8V     <V     MV     ]V     dV     xV     �V     �V     �V     �V     �V     �V     �V     W     9W     OW     gW     wW     �W     �W     �W     �W     �W     �W     �W  4   �W  (   X     8X     UX     ZX  ;   qX  -   �X  %   �X  -   Y  <   /Y  !   lY  A   �Y     �Y  	   �Y  	   �Y     �Y     Z  ,   Z     BZ     TZ     fZ     yZ     �Z     �Z  ;   �Z     �Z    [  �   \      ]     	]  4   !]     V]     j]  '   �]     �]  #   �]  D   �]  	   !^     +^     A^  6   R^  K   �^  !   �^     �^     _     _  U   /_     �_     �_     �_     �_  1   �_     �_     `  
   "`  j   -`     �`     �`     �`  !   �`     �`     �`     a  #   )a     Ma  <   aa  9   �a  +   �a  W   b  B   \b     �b  *  �b  I   �c     0d     Hd  $   Pd  J   ud  !   �d  5   �d  '   e  %   @e     fe  ,   �e  W   �e  [   f  n   df     �f  -   �f  [   g     cg  *   �g     �g     �g     �g  0   �g  �   h      �h  �    i  �    j  d   �j     k     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                            100%% Yksityiset postilaatikot, joilla on täysi omistusoikeus!
                                         
                                            Luo väärennetty sähköposti yhdellä napsautuksella
                                         
                                            Piilota itsesi roskapostista parannetulla yksityisyydellä ja turvallisuudella
                                         
                                        Klikkaa ”Poista” kotisivulla ja saat uuden väliaikaisen sähköpostin.
                                     
                                        Sähköpostiosoite on voimassa, kunnes poistat sen tai kunnes palvelu poistaa sen.
 Elinikä riippuu monista eri tekijöistä.
 Voit saada tilapäisiä sähköposteja, joiden käyttöikä on enintään 1 VUOI rekisteröityä
 -tilille.
                                     
                                        Monet sovellukset ja sivustot velvoittavat sinua antamaan henkilökohtaisen sähköpostiosoitteen ilman syytä
 muuta kuin kerätä henkilökohtaisia tietoja sinusta.
 Väliaikainen sähköpostiosoite mahdollistaa henkilökohtaisen sähköpostiosoitteesi erottamisen
 käytät pankkitoimintaan, luottokorttiostoksiin ja muuhun henkilökohtaiseen tunnistamiseen
 liiketoimia kaikista muista sivustoista siellä.
                                     
                                        Sähköpostin lähettäminen on täysin poistettu käytöstä
 sitä ei koskaan toteuteta petosten ja roskapostin vuoksi.
                                     
                                        Ne näkyvät kertakäyttöisen sähköpostin alla.
 Jos et saanut saapuvia sähköpostiviestejä liian kauan - napsauta ”Päivitä” -painiketta.
                                     
                                        Voit jakaa sen käyttämällä QR-koodia pääsivulla.
                                     
                                    Voit olla enintään %(max_tokens_allowed)s kuponkia.
                                     
                                Emme voi toimittaa sähköpostia tähän osoitteeseen. (Yleensä koska yritysten palomuurit
 tai suodattamalla.)
                                 
                                Miksi tarvitsen %(BRAND_NAME)s -tilin?
                             
                                Annoit meille vahingossa toisen sähköpostiosoitteen. (Yleensä työ tai henkilökohtainen
 sen sijaan, jota tarkoitit.)
                                 
                            Jos et näe meiltä lähettämäänsähköpostia muutamassa minuutissa, muutama
 Asioita olisi voinut tapahtua:
                             
                            Olemme lähettäneet sinulle ohjeet salasanan asettamisesta, jos tili
 on olemassa antamasi sähköpostin kanssa. Sinä
 pitäisi saada ne pian.
                         
                        Jos et saa sähköpostia, varmista, että olet syöttänyt osoitteen
 rekisteröity
 kanssa, ja tarkista
 roskapostikansio.
                         
 UptimeMeTrics-alusta on suunniteltu seuraamaan sivustojen tilaa. Se sisältää tällaisia
 ominaisuudet: Verkkosivuston saatavuus,
 Palvelimen saatavuus
 Verkkosivuston SSL-varmenteen ja HTML-validoinnin voimassaolotarkistus.
  
                    %(BRAND_NAME)s on kertakäyttöinen sähköpostiosoitepalvelu. Kun vierailet %(BRAND_NAME)s uudessa sähköpostissa
 osoite luodaan juuri sinulle. Luotu sähköpostiosoite voi heti vastaanottaa sähköposteja.
 Kaikki vastaanotetut sähköpostit näkyvät heti pääsivulla. Kukaan muu kuin sinä näet
 sähköpostit
 jotka vastaanotetaan. On tärkeää huomata, että sähköpostiosoite vanhenee 24 tunnin kuluttua. Kun
 sähköposti
 osoite on vanhentunut, sähköpostiosoite ja kaikki vastaanotetut sähköpostit ovat poissa. Uusi sähköpostiosoite
 tahtoa
 luodaan käydessään tällä verkkosivustolla. Kertakäyttöisen sähköpostin käyttämiseen on monia syitä.
 Haluat ehkä antaa jollekulle sähköpostiosoitteen paljastamatta henkilöllisyyttäsi. Tai haluat ehkä
 rekisteröidy verkkosivustolle tai verkkopalveluun, mutta olet huolissasi siitä, että verkkosivusto lähettää sinulle roskapostia
 tulevaisuuteen. <br/>
 Tämän palvelun käyttämiseksi sinun on otettava käyttöön eväste ja javascript, eväste on vain istunnon tallentaminen
 id
 ja kieliasetukset, yksityisyytesi kuuluu tietosuoja- ja evästekäytäntömme piiriin.
                 
                    %(BRAND_NAME)s on kertakäyttöinen sähköpostiosoitepalvelu. Kun vierailet %(BRAND_NAME)s uusi sähköposti
 osoite luodaan juuri sinua varten. Luotu sähköpostiosoite voi välittömästi vastaanottaa sähköpostiviestejä.
 Kaikki vastaanotetut sähköpostiviestit näkyvät pääsivulla välittömästi. Kukaan muu kuin sinä ei näe sähköposteja
 jotka on vastaanotettu. On tärkeää huomata, että sähköpostiosoite vanhenee 24 tunnin kuluttua. Kun sähköposti
 osoite on vanhentunut, sähköpostiosoite ja vastaanotetut sähköpostit ovat poissa. Uusi sähköpostiosoite
 luodaan, kun vierailet tällä sivustolla. Kertakäyttöisen sähköpostin käyttämiseen on monia syitä.
 Haluat ehkä antaa jollekulle sähköpostiosoitteen paljastamatta henkilöllisyyttäsi. Tai saatat haluta
 rekisteröityä verkkosivustoon tai verkkopalveluun, mutta olet huolissasi siitä, että verkkosivusto lähettää sinulle roskapostia tulevaisuudessa. <br />
 Jotta voit käyttää tätä palvelua, sinun täytyy ottaa eväste ja javascript käyttöön, eväste on vain tallentaa istunnon tunnus
 ja kieliasetuksesi, Tietosuoja kuuluu Tietosuoja- ja evästekäytäntömme piiriin.
                 
                Klikkaa tätä linkkiä aktivoidaksesi tilisi:
             
            Kiitos rekisteröitymisestä!
 Tilisi on luotu, voit kirjautua sisään, kun olet aktivoinut tilisi painamalla URL-osoitetta
 alla.
             API-näppäimet Tietoa meistä Access Token osoite Kaikki sähköpostit Kaikki oikeudet pidätetään. Kaikki mitä sinun tarvitsee tietää väliaikaisesta postista Onko sinulla jo tili? Virhe tapahtui, kun yrität kirjautua sisään sosiaalisen verkoston tilisi kautta. Haluatko varmasti poistaa sähköpostin Oletko varma, että haluat poistaa tämän tunnuksen. Takaisin luetteloon Muutos Tarkista sähköpostisi. Sulje Vahvista salasana Ota yhteyttä Evästeiden käytäntö Evästeiden käytäntö Kopioitu! Kopio Kopioi sähköpostiosoite ylimmästä syötteestä Luo Luo sähköposti Päivämäärä Poista Poista sähköposti Poista Token Demo Saapuneet Etkö saanut sähköpostia? Kertakäyttöiset sähkö Verkkotunnus Eikö sinulla ole tiliä? Sähköposti Sähköpostiaktivointi valmis Sähköposti on jo olemassa. Sähköpostit vastaan Anna sähköpostiosoite Merkinnät sivu Vanhenee USEIN KYSYTTYÄ Palaute Unohdin salasanan? Usein kysyttyjä kysymyksiä alkaen MENNÄ Luo Hanki ilmainen väliaikainen sähköposti tänään! Hanki tilapäinen sähköposti tänään Hanki sähköpostiosoitteeni Mene Hei siellä, haluaisin Miten voin pidentää sähköpostiosoitteen käyttöikää? Miten voin jakaa väliaikaisen sähköpostin? Kuinka helppoa tätä oli käyttää? Kuinka tarkistaa vastaanotetut sähköpostit? Kuinka poistaa kertakäyttöinen sähköposti ja saada uusi? Kuinka lähettää sähköpostia? Kuinka käyttää kertakäyttöistä väliaikaista sähköpostia? Kuinka käyttää sitä Hyväksyn Saapuneet Sapostilaatikot Puhutaan Tilapäisen sähköpostiosoitteen lataaminen Kirjaudu sisään Kirjaudu sisään Kirjaudu tilillesi Kirjaudu ulos Lisää vanhoja merkintöjä Tuoreempia merkintöjä Nyt voit kirjautua tilillesi ja aloittaa palvelun käytön. Yhteystiedot Vankan ja turvallisen palvelumme avulla voit saada täysin nimettömän ja yksityisen
 sähköpostiosoite, jonka sinä
 voi käyttää välittömästi.
 Itse asiassa voit luoda oman väliaikaisen sähköpostiosoitteen ja käyttää sitä aina, kun
 haluavat. Vahvan ja turvallisen palvelun avulla voit saada täysin nimettömän ja yksityisen sähköpostiosoitteen, jonka
 voi käyttää välittömästi.
 Itse asiassa voit luoda oman väliaikaisen sähköpostiosoitteen ja käyttää sitä milloin haluat. salasana Salasanan palauttaminen Siirry seuraavalle sivulle ja valitse uusi salasana: Suosittu Artikkelit Tietosuojakäytäntö Projektit testattu väärennetyillä Sa Raaka sisältö Lähetä aktivointiviesti uudelleen Lue saapuvat sähköpostit tällä sivulla Saapuneet-kansion osiossa Päivitä Rekisteröinti valmis Palauta salasana Palauta salasana linkki lähetetään sähköpostiisi. Säästä aikaa, kun sinun ei tarvitse koskaan luoda sähköpostiosoitetta. Skannaa QR-koodi matkapuhelimella Lähetä palautetta Lähettäjä Jaettu Saapuneet kansi Osoita meille rakkautta suosittelemalla meitä suosikkiverkostoosi tai yhteisöllesi! Rekisteröidy Rekisteröidy Rekisteröidy maksutta! Rekisteröidy Sosiaalisen verkoston kirjautumisepäonnistuminen Roskapostit havaittu Roskaposti Ilmainen Aloita nyt Lopeta roskapostien vastaanottaminen ja unohda Saapuneet-kansion ”suodattimet” ja ”peruuta tilaus” Kohde Kerro meistä Väliaikainen sähköposti Väliaikainen sähköpostipalvelu Säännöt ja ehdot Käyttöehdot Kiitos rekisteröitymisestä! Kiitos, että käytät sivustoamme! %(site_name)s tiimi Käyttämäsi tili, jonka yritit aktivoida, on virheellinen. Käyttäjätili, jonka yritit aktivoida, on jo aktivoitu. Toimittamasi aktivointinäppäin ei kelpaa. Antamasi sähköpostiosoite oli virhe tai kirjoitusvirhe. (Tapahtuu parhaalle meistä.) Sähköposti on roskapostikansiossa. (Joskus sinne eksyy asioita.) Tämä tili on vanhentunut. Tämä suuntaus osoittaa, että linkkien avulla haitallisten kampanjoiden operaattorit saavat enemmän napsautuksia ja infektioita verrattuna klassiseen tekniikkaan tiedostojen liittämisestä sähköposteihin. On selvää, että käyttäjät ovat tulleet epäluuloisiksi sähköpostin liitteisiin. Tämän avulla voit rekisteröityä sivustoille, sosiaaliseen mediaan jne Hyödyllisiä linkkejä Näytä Saapuvien sähköpostien odottaminen Pyrimme korkealle keskittymään pitämään sinut turvassa internetissä. Arvostamme sitä todella, kiitos! Mikä on kertakäyttöinen väliaikainen sähköposti Kuka sinä olet ja miten voimme auttaa? Miksi tarvitsen väliaikaista postia? Suosittelisitko palveluamme? Sinulla ei ole rahakkeita tällä hetkellä. Et halua jakaa henkilökohtaista sähköpostiosoitettasi? Oletko väsynyt
 Roskapostia? Etkö halua jakaa henkilökohtaista sähköpostiosoitettasi? Oletko väsynyt roskapostista? Saat tämän sähköpostin, koska pyysit salasanan palautusta käyttäjätilillesi osoitteessa
 %(site_name)s. Sinun Ilmainen väliaikainen sähköpostiosoitteesi Ystäväsi ja työtoverisi voivat saada välittömän näkymän saapuvasta sähköpostista. Saapuneet kansiosi on tyhjä Käyttäjätunnuksesi, jos olet unohtanut: sitten miljardeja postilaatikoita koodi suunniteltu ratkaisemaan kaikki nämä ongelmat. asiantuntijat laskivat, että 87%% kaikista vuoden 2020 toisella neljänneksellä (huhti-, touko- ja kesäkuussa) lähetetyistä sähköpostisähköpostista sisälsi linkkejä haittatiedostojen lataamiseen, mutta ei liitetiedostoja itse tiedostoihin. ilmainen verkkosivuston seuranta on osoite, joka annetaan lyhyeksi ajaksi: muutamasta minuutista koko kuukauteen, ja sitten se poistetaan kaikkien saapuvien kirjeiden ja liitteiden kanssa tai yksinkertaisesti ei ole käytettävissä jatkokäyttöä varten. alusta on suunniteltu erityisesti auttamaan sinua unohtamaan roskapostin, mainospostit, hakkerointi ja hyökkää robotteja. Pidä todellinen postilaatikkosi puhtaana ja turvallisena. tarjoaa väliaikaisen, turvallisen, nimettömän, ilmaisen, kertakäyttöisen sähköpostiosoitteen. rekisteröityä 