��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  A  R;  �   �<  y   +=  �   �=  �   O>  d  �>  �  d@  �   [B  �   +C  �   D  �   �D  �   8E  q   �E  �   fF  �   QG  �   �G  �   �H    �I  �  �J  �  �O  N   dT  �   �T  	   jU     tU     �U     �U     �U     �U  8   �U     �U  _   V  .   }V  *   �V     �V     �V     �V     W     W     &W     5W     TW  	   nW     xW  0   W     �W     �W     �W  	   �W     �W     �W  &   �W  !   %X     GX     VX     ^X     {X     �X     �X     �X     �X     �X     �X     �X     Y     Y     $Y     8Y     ;Y  	   AY  ;   KY  +   �Y     �Y     �Y     �Y  A   �Y  3   *Z  )   ^Z  '   �Z  ?   �Z     �Z  4   [     B[     U[     b[     w[     �[  -   �[     �[  	   �[     �[     �[     \     "\  Z   :\     �\  �   �\  �   �]     �^     �^  H   �^     �^     _  5   $_     Z_  (   g_  [   �_     �_     �_     `  K   -`  =   y`  /   �`     �`      a     a  Z   +a  
   �a  
   �a     �a  
   �a  %   �a  "   �a  	   b     b  �   .b     �b     �b     �b      �b     c     c     2c     Qc     pc  :   �c  A   �c  <   d  {   Bd  [   �d     e  A  /e  N   qf     �f     �f     �f  U   �f  5   Fg  )   |g  5   �g  1   �g  $   h  )   3h  f   ]h  a   �h  �   &i     �i     �i  Q   �i  #   ,j  :   Pj     �j  &   �j     �j  *   �j  �   �j     �k  �   �k  �   �l  Q   �m  
   �m     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
                                            Boîtes aux lettres privées à 100 %% en pleine propriété !
                                         
                                            Générez un faux e-mail en un clic
                                         
                                            Cachez-vous des spams grâce à une confidentialité et une sécurité améliorées
                                         
                                        Cliquez sur « Supprimer » sur la page d'accueil et vous recevrez un nouvel e-mail temporaire.
                                     
                                        L'adresse e-mail est valide jusqu'à ce que vous la supprimiez ou jusqu'à ce que le service la supprime.
 La durée de vie dépend de nombreux facteurs différents.
 Vous pouvez recevoir des e-mails temporaires d'une durée de vie allant jusqu'à 1 AN avec inscription
 compte.
                                     
                                        De nombreuses applications et sites Web vous obligent à fournir une adresse e-mail personnelle sans raison.
 autre que pour collecter des informations personnelles vous concernant.
 L'adresse e-mail temporaire vous permet de séparer votre adresse e-mail personnelle
 que vous utilisez pour les opérations bancaires, les achats par carte de crédit et autres identifiants personnels
 transactions de tous les autres sites.
                                     
                                        L'envoi d'e-mails est complètement désactivé et
 il ne sera jamais mis en œuvre en raison de problèmes de fraude et de spam.
                                     
                                        Ils sont affichés sous votre e-mail jetable.
 Si vous n'avez pas reçu de courriels entrants pendant trop longtemps, cliquez sur le bouton « Actualiser ».
                                     
                                        Vous pouvez le partager en utilisant le code QR sur la page principale.
                                     
                                    Vous pouvez avoir un maximum de %(max_tokens_allowed)s jetons.
                                     
                                Nous ne pouvons pas envoyer l'e-mail à cette adresse. (Généralement à cause des pare-feu d'entreprise)
 ou filtrage.)
                                 
                                Pourquoi ai-je besoin d'un compte %(BRAND_NAME)s ?
                             
                                Vous nous avez accidentellement donné une autre adresse e-mail. (Habituellement une activité professionnelle ou personnelle)
 au lieu de celui que vous vouliez dire.)
                                 
                            Si vous ne voyez pas d'e-mail de notre part en quelques minutes, quelques
 des choses auraient pu se produire :
                             
                            Nous vous avons envoyé par e-mail des instructions pour définir votre mot de passe, s'il s'agit d'un compte
 existe avec l'e-mail que vous avez entré. Vous
 devraient les recevoir sous peu.
                         
                        Si vous ne recevez pas d'e-mail, assurez-vous d'avoir saisi l'adresse que vous avez saisie
 inscrit
 avec, et vérifiez votre
 dossier de spam.
                         
 La plateforme Uptimetrics est conçue pour surveiller l'état des sites Web. Il comprend de tels
 caractéristiques : Disponibilité du site Web,
 Disponibilité des serveurs,
 Vérification de validité du certificat SSL du site Web et validation HTML.
  
                    %(BRAND_NAME)s est un service d'adresse e-mail jetable. Lorsque vous consultez %(BRAND_NAME)s un nouvel e-mail
 l'adresse est générée juste pour vous. L'adresse e-mail générée peut recevoir immédiatement tous les e-mails.
 Tout e-mail reçu apparaîtra immédiatement sur la page principale. Personne d'autre que vous ne le verrez
 les e-mails
 qui sont reçus. Il est important de noter qu'une adresse e-mail expire au bout de 24 heures. Quand un
 courriel
 l'adresse a expiré, l'adresse e-mail et tous les e-mails reçus disparaîtront. Une nouvelle adresse e-mail
 volonté
 être généré lors de la visite de ce site Web. Il existe de nombreuses raisons d'utiliser un e-mail jetable.
 Vous pouvez donner une adresse e-mail à quelqu'un sans révéler votre identité. Ou vous voudrez peut-être
 vous inscrivez à un site Web ou à un service Web, mais vous craignez que le site Web ne vous envoie du spam
 le futur. <br/>
 Pour utiliser ce service, vous DEVEZ activer les cookies et javascript, le cookie sert simplement à enregistrer votre session
 identifiant
 et votre préférence linguistique, votre vie privée est couverte par notre politique de confidentialité et de cookies.
                 
                    %(BRAND_NAME)s est un service d'adresses e-mail jetables. Lorsque vous visitez %(BRAND_NAME)s un nouvel e-mail
 l'adresse est générée uniquement pour vous. L'adresse e-mail générée peut recevoir immédiatement tous les e-mails.
 Tout e-mail reçu apparaîtra immédiatement sur la page principale. Personne d'autre que vous ne verrez les e-mails
 qui sont reçus. Il est important de noter qu'une adresse e-mail expire après 24 heures. Quand un e-mail
 l'adresse a expiré, l'adresse e-mail et les e-mails reçus disparaîtront. Une nouvelle adresse e-mail va
 être généré lors de la visite de ce site Web. Il existe de nombreuses raisons d'utiliser un e-mail jetable.
 Vous voudrez peut-être donner une adresse e-mail à quelqu'un sans révéler votre identité. Ou vous voudrez peut-être
 inscrivez-vous à un site Web ou à un service Web, mais vous craignez que le site Web vous enverra du spam à l'avenir. <br />
 Pour utiliser ce service, vous DEVEZ activer les cookies et javascript, le cookie est juste pour enregistrer votre identifiant de session
 et les préférences linguistiques, votre confidentialité est couverte par notre politique de confidentialité et de cookies.
                 
                Cliquez sur ce lien pour activer votre compte :
             
            Merci de vous être inscrit !
 Votre compte a été créé, vous pouvez vous connecter après avoir activé votre compte en appuyant sur l'URL
 ci-dessous.
             Clés API À propos de nous jeton d'accès Adresse Tous les e-mails Tous droits réservés. Tout ce que vous devez savoir sur le courrier temporaire Vous avez déjà un compte ? Une erreur s'est produite lors de la tentative de connexion via votre compte de réseau social. Êtes-vous sûr de vouloir supprimer un e-mail Voulez-vous vraiment supprimer ce jeton ? Retour à la liste Changer Vérifiez votre adresse e-mail. Fermer Confirmer le Contactez-nous Politique relative aux cookies Politique sur les cookies Copié ! Copier Copier l'adresse e-mail de l'entrée supérieure Créer Créer un e-mail Date Supprimer Supprimer un e-mail Supprimer le jeton Boîte de réception de démonstration Vous n'avez pas reçu d'e-mail ? E-mail jetable Domaine Vous n'avez pas de compte ? Courriel Activation par e-mail terminée L'e-mail existe déjà. Courriels reçus Saisissez une adresse e-mail Page Entrées Expire FAQ Feedback Mot de passe oublié ? Foire aux questions De ALLER Générer Recevez votre e-mail temporaire gratuit dès aujourd'hui ! Recevez votre e-mail temporaire aujourd'hui Obtenir mon adresse e-mail Aller Bonjour, j'aimerais Comment puis-je prolonger la durée de vie de l'adresse e-mail ? Comment puis-je partager des e-mails temporaires ? Est-ce que c'était facile à utiliser ? Comment vérifier les e-mails reçus ? Comment supprimer les e-mails jetables et obtenir le nouveau ? Comment envoyer un e-mail ? Comment utiliser les e-mails temporaires jetables ? Comment l'utiliser J'accepte le Boîte de réception Boîtes de réception Parlons Chargement de votre adresse e-mail temporaire Connectez-vous Connexion Connectez-vous à votre compte Déconnexion Plus d'anciennes entrées Entrées plus récentes Vous pouvez maintenant vous connecter à votre compte et commencer à utiliser le service. Nos Contacts Notre service robuste et sûr vous permet d'obtenir un
 adresse e-mail que vous
 peut être utilisé immédiatement.
 En fait, vous pouvez créer votre propre adresse e-mail temporaire et l'utiliser chaque fois que vous
 vouloir. Notre service robuste et sécurisé vous permet d'obtenir une adresse e-mail totalement anonyme et privée
 peut être utilisé immédiatement.
 En fait, vous pouvez créer votre propre adresse e-mail temporaire et l'utiliser quand vous le souhaitez. mot de passe réinitialisation du mot Veuillez aller à la page suivante et choisir un nouveau mot de passe : Articles populaires Politique de confidentialité Projets testés avec de fausses boîtes de réception Contenu brut Envoyer à nouveau l'e-mail d'activation Lire les e-mails entrants sur cette page à l'intérieur de la section boîte de réception Rafraîchir Inscription terminée Réinitialiser le mot de passe Le lien de réinitialisation du mot de passe sera envoyé sur votre e-mail. Gagnez du temps en n'ayant jamais à créer d'adresse e-mail. Scanner le code QR avec un téléphone portable Envoyer des commentaires Expéditeur Boîte de réception partagée Montrez-nous de l'amour en nous recommandant à votre réseau ou communauté préféré ! S'inscrire S'inscrire Inscrivez-vous gratuitement ! S'inscrire Échec de connexion au réseau social Courriels indésirables détectés Sans spam Commencez maintenant Arrêtez de recevoir des courriels corbeille et oubliez les boutons « filtres » et « désabonnement » de la boîte de réception Sujet Parlez-nous de nous E-mail temporaire Service de messagerie temporaire Termes et conditions Conditions d'utilisation Merci de vous être inscrit ! Merci d'utiliser notre site ! L'équipe %(site_name)s Le compte que vous avez tenté d'activer n'est pas valide. Le compte que vous avez essayé d'activer a déjà été activé. La clé d'activation que vous avez fournie n'est pas valide. L'adresse e-mail que vous avez saisie comportait une erreur ou une faute de frappe. (Cela arrive au meilleur d'entre nous.) L'e-mail se trouve dans votre dossier de spam. (Parfois, des choses se perdent là-dedans.) Ce compte a expiré. Cette tendance prouve qu'en utilisant des liens, les opérateurs de campagnes malveillantes reçoivent plus de clics et d'infections par rapport à la technique classique consistant à joindre des fichiers aux e-mails. Évidemment, les utilisateurs sont devenus méfiants à l'égard des pièces jointes dans le courrier. Utilisez ceci pour vous inscrire sur des sites Web, des réseaux sociaux, etc. Liens utiles Voir En attente d'e-mails entrants Nous visons tout particulièrement à vous concentrer sur la sécurité sur Internet. Nous vous en sommes vraiment reconnaissants, merci ! Qu'est-ce qu'un e-mail temporaire jetable Qui êtes-vous, et comment pouvons-nous vous aider ? Pourquoi ai-je besoin d'un courrier temporaire ? Recommanderiez-vous notre service ? Vous n'avez pas de jetons pour le moment. Vous ne souhaitez pas partager votre adresse e-mail personnelle ? Es-tu fatigué à cause de
 Spam ? Vous ne souhaitez pas partager votre adresse e-mail personnelle ? Vous êtes fatigué du spam ? Vous recevez cet e-mail parce que vous avez demandé une réinitialisation du mot de passe pour votre compte d'utilisateur à
 %(site_name)s. Votre Votre e-mail temporaire gratuit Vos amis et collègues peuvent obtenir une vue instantanée des e-mails entrants. Votre boîte de réception est vide Votre nom d'utilisateur, au cas où vous auriez oublié : depuis des milliards de boîtes de réception code conçu pour résoudre tous ces problèmes. ont calculé que 87%% de tous les courriels envoyés au deuxième trimestre 2020 (mois d'avril, mai et juin) contenaient des liens pour télécharger des fichiers malveillants, mais pas des pièces jointes avec les fichiers eux-mêmes. surveillance gratuite du site est une adresse qui est émise pour une courte période : de quelques minutes à un mois entier, puis elle sera supprimée avec toutes les lettres et pièces jointes entrantes, ou tout simplement indisponible pour une utilisation ultérieure. est spécialement conçue pour vous aider à oublier le spam, les envois publicitaires, le piratage et les robots attaquant. Gardez votre véritable boîte aux lettres propre et sécurisée. fournit une adresse e-mail temporaire, sécurisée, anonyme, gratuite et jetable. s'inscrire 