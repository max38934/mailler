��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  B  R;  �   �<  �   =  �   �=  �   @>  0  �>  �  @  �   �A  �   �B  t   �C  |   �C  �   sD  l   *E  �   �E  �   cF  �   G  �   �G    �H  u  �I  �  'N  Z   �R  �   S     �S     �S     �S     �S     �S     �S  5   �S     T  _   /T  *   �T  *   �T     �T  
   �T     U  	   U     )U     AU     ]U     pU     yU  	   �U  +   �U     �U     �U     �U     �U     �U     �U     V     'V     <V     SV     ZV     iV     pV     �V     �V     �V     �V     �V     �V     �V     �V     W     *W     0W     5W  *   >W  (   iW     �W     �W     �W  :   �W  +   X  $   .X  .   SX  C   �X     �X  8   �X     Y     1Y     =Y     SY     gY  %   uY     �Y     �Y     �Y     �Y     �Y     �Y  Q   Z     eZ  �   sZ    l[     r\  !   z\  I   �\     �\     �\  (   ]     :]  '   I]  U   q]     �]     �]  !   �]  <   ^  M   R^  #   �^     �^     �^     �^  l   �^     h_     w_     �_     �_  ,   �_     �_     �_     �_  o   
`     z`     �`     �`      �`  $   �`     �`  !   a  )   'a     Qa  +   ha  1   �a  ,   �a  N   �a  K   Bb     �b  F  �b  <   �c     &d     5d  !   <d  P   ^d  "   �d  "   �d  $   �d  +   e     Fe     fe  F   e  N   �e  _   f     uf  #   |f  E   �f  &   �f  %   g     3g     <g     Yg  -   ^g  �   �g     }h  �   �h  �   ii  R   j     rj     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                            100%% Privát postafiókok teljes tulajdonjoggal!
                                         
                                            Hamis e-maillétrehozása egyetlen kattintással
                                         
                                            Elrejti magát a spam elől a fokozott adatvédelemmel és biztonság
                                         
                                        Kattintson a „Törlés” gombra a kezdőlapon, és új ideiglenes e-mailt kap.
                                     
                                        Az e-mail cím addig érvényes, amíg nem törli, vagy amíg a szolgáltatás nem törli.
 Az élettartam számos különböző tényezőtől függ.
 Lehet ideiglenes e-mailek élettartama akár 1 ÉV regisztrált
 -fiókban.
                                     
                                        Számos alkalmazás és webhely arra kötelezi Önt, hogy személyes e-mail címet adjon meg minden ok nélkül
 kivéve, hogy személyes adatokat gyűjtsön Önről.
 Az ideiglenes e-mail cím lehetővé teszi a személyes e-mail cím elkülönítését
 banki, hitelkártya-vásárlásra és egyéb személyes azonosításra használt
 tranzakciók az összes többi helyszínen odakinn.
                                     
                                        E-mail küldése teljesen le van tiltva, és
 ez soha nem kerül végrehajtásra miatt csalás és spam kérdések.
                                     
                                        Ezek az eldobható e-mail alatt jelennek meg.
 Ha túl sokáig nem kapta meg a bejövő e-maileket - kattintson a „Frissítés” gombra.
                                     
                                        Megoszthatja a QR-kódot a főoldalon.
                                     
                                    Maximum %(max_tokens_allowed)s tokeneket kaphatsz.
                                     
                                Nem tudjuk kézbesíteni az e-mailt erre a címre. (Általában azért, mert a vállalati tűzfalak
 vagy szűrés.)
                                 
                                Miért van szükségem %(BRAND_NAME)s fiókra?
                             
                                Véletlenül megadtál nekünk egy másik e-mail címet. (Általában egy munka vagy személyes egy
 ahelyett, hogy azt gondoltad volna.)
                                 
                            Ha néhány percen belül nem jelenik meg tőlünk e-mailben, néhány
 történhettek volna dolgok:
                             
                            E-mailben elküldtük Önnek a jelszó beállítására vonatkozó utasításokat, ha
 létezik a megadott e-mailben. Ön
 meg kell kapnia őket hamarosan.
                         
                        Ha nem kap e-mailt, kérjük, győződjön meg arról, hogy megadta a címet
 bejegyzett
 segítségével, és ellenőrizze a
 spam mappába.
                         
 UptimeMetrics platform célja, hogy figyelemmel kíséri az állapotát a weboldalak. Ez magában foglalja az ilyen
 funkciók: Weboldal elérhetősége,
 A kiszolgáló elérhetősége,
 A weboldal SSL tanúsítványának és HTML-ellenőrzésének érvényességi ellenőrzése.
  
                    %(BRAND_NAME)s egy eldobható e-mail cím szolgáltatás. Amikor meglátogatod a %(BRAND_NAME)s egy új e-mailt
 cím generálódik csak az Ön számára. A generált e-mail cím azonnal megkaphatja az e-maileket.
 A beérkezett e-mailek azonnal megjelennek a főoldalon. Senki más, mint te.
 az e-mailek
 amelyek beérkeztek. Fontos megjegyezni, hogy egy e-mail cím 24 óra elteltével lejár. Amikor egy
 e-mail
 a cím lejárt, az e-mail cím és a kapott e-mailek eltűnnek. Új e-mail cím
 akarat
 létre kell hoznia a weboldal meglátogatásakor. Az eldobható e-mail használatának számos oka van.
 Érdemes megadnia valakinek e-mail címet anélkül, hogy felfedné személyazonosságát. Vagy érdemes
 iratkozzon fel egy webhelyre vagy webszolgáltatásra, de aggódik amiatt, hogy a webhely spamet küld Önnek
 a jövőt. <br/>
 A szolgáltatás használatához engedélyeznie kell a cookie-kat és a javascriptet, a cookie csak a munkamenet rögzítésére szolgál
 id
 és nyelvi preferencia, az Ön adatvédelmét az Adatvédelmi és Sütikre vonatkozó irányelveink szabályozzák
                 
                    %(BRAND_NAME)s egy eldobható e-mail cím szolgáltatás. Amikor meglátogatsz egy új e-mailt
 címet csak az Ön számára generálja. A generált e-mail cím azonnal megkaphatja az e-maileket.
 A kapott e-mailek azonnal megjelennek a főoldalon. Senki más nem fogja látni az e-maileket
 hogy megkapták. Fontos megjegyezni, hogy az e-mail cím 24 óra elteltével lejár. Amikor egy e-mail
 cím lejárt, az e-mail cím és a kapott e-mailek eltűnnek. Egy új e-mail cím
 generálható, ha meglátogatja ezt a weboldalt. Számos oka van az eldobható e-mail használatának.
 Előfordulhat, hogy az Ön személyazonosságának felfedése nélkül szeretne e-mail címet adni valakinek. Vagy lehet, hogy
 regisztráljon egy weboldalra vagy webszolgáltatásra, de aggódik amiatt, hogy a weboldal a jövőben spamet küld Önnek. <br />
 A szolgáltatás használatához engedélyeznie kell a cookie-t és a JavaScriptet, a cookie csak a munkamenet azonosítójának rögzítésére használható
 és a nyelvi beállításokat, az Adatvédelem az Adatvédelmi és Cookie-irányelveink hatálya alá tartozik.
                 
                Kérjük, kattintson erre a linkre a fiók aktiválásához:
             
            Köszönjük, hogy feliratkozott!
 Fiókja létrejött, bejelentkezhet, miután aktiválta fiókját az url
 alább.
             API-kulcsok Rólunk Hozzáférési token Cím Összes e-mail Minden jog fenntartva. Mindössze annyit kell tudni az ideiglenes levélről Már van fiókod? Hiba történt a közösségi hálózati fiókján keresztül történő bejelentkezés során. Biztosan törölni szeretné az e-maileket Biztosan törölni szeretné ezt a tokent. Vissza a listához Változás Ellenőrizze az e-mailjeit. Bezárás Jelszó megerősítése Lépjen kapcsolatba velünk Cookie szabályzat Cookie-k Lemásoltam! Másolás E-mail cím másolása a felső bemenetről létrehozás E-mail létrehozása Dátum Törlés E-mail törlése Token törlése Demo Beérkező levelek Nem kaptál e-mailt? Eldobható e-mail cím Domain Nincs fiókod? E-mail Az e-mailek aktiválása E-mail már létezik. Kapott e-mailek E-mail cím megadása Bejegyzések oldal Lejár GYIK Visszacsatolás Elfelejtette a jelszót? Gyakran Ismételt Kérdések -től MEGY Generál Kap az ingyenes ideiglenes e-mail még ma! Kérje meg ideiglenes e-mailjét még ma Szerezd meg az e-mail címemet Menj Szia, szeretnék Hogyan hosszabbíthatom meg az e-mail cím élettartamát? Hogyan oszthatok meg ideiglenes e-maileket? Milyen könnyű volt ezt használni? Hogyan lehet ellenőrizni a kapott e-maileket? Hogyan lehet törölni az eldobható e-mailt és megkapni az újat? Hogyan küldhetek e-mailt? Hogyan kell használni az eldobható ideiglenes e-mailt? Hogyan kell használni Elfogadom a Beérkezett üzenetek Beérkezett fiókok Beszéljünk. Az ideiglenes e-mail cím betöltése Bejelentkezés bejelentkezés Jelentkezzen be fiókjába Kijelentkezés Még több régi bejegyzés Újabb bejegyzések Most már bejelentkezhet fiókjába, és elkezdheti használni a szolgáltatást. Kapcsolataink Robusztus és biztonságos szolgáltatásunk lehetővé teszi, hogy teljesen névtelen és privát
 e-mail cím, amelyet Ön
 azonnal felhasználhatja.
 Valójában létrehozhat saját ideiglenes e-mail címet, és bármikor felhasználhatja
 akar. Robusztus és biztonságos szolgáltatásunk lehetővé teszi, hogy teljesen névtelen és privát e-mail címet kapjon, amelyet Ön
 azonnal felhasználható.
 Tulajdonképpen létrehozhat saját ideiglenes e-mail címet, és használhatja, amikor csak akarja. Jelszó Jelszó alaphelyzetbe állítása Kérjük, lépjen a következő oldalra, és válasszon egy új jelszót: Népszerű cikkek Adatvédelmi irányelvek Hamis postaládákkal tesztelt projektek Nyers tartalom Aktiválási e-mail ismételt küldése Bejövő e-mailek olvasása ezen az oldalon belül a Beérkezett üzenetek szakaszban Frissítés A regisztráció befejeződött Jelszó alaphelyzetbe állítása A jelszó visszaállítása linket elküldjük az e-mailben. Takarítson meg időt azzal, hogy soha ne kelljen e-mail címet létrehoznia. QR-kód beolvasása mobiltelefonnal Visszajelzés küldése Feladó Megosztott beérkezett Mutasson nekünk némi szeretetet azáltal, hogy ajánl minket kedvenc hálózatához vagy közösségéhez! Iratkozzon fel Iratkozzon fel Iratkozzon fel ingyen! Regisztráció Közösségi hálózati bejelentkezési hiba Spam emailek észlelt Spam Ingyenes Kezdés most Hagyja abba a szemetet e-mailek fogadását, és felejtse el a „szűrők” és a „leiratkozás” gombokat Tárgy Mesélj rólunk Ideiglenes e-mail Ideiglenes e-mail szolgáltatás Általános Szerződési Feltételek Felhasználási feltételek Köszönjük, hogy feliratkozott! Köszönjük, hogy oldalunkat használta! A %(site_name)s csapat Az aktiválni kívánt fiók érvénytelen. Az aktiválni kívánt fiók már aktiválva van. A megadott aktiválási kulcs érvénytelen. A megadott e-mail cím hibás vagy elírás volt. (A legjobbakkal történik.) Az e-mail a spam mappában található. (Néha a dolgok elvesznek odabent.) Ez a fiók lejárt. Ez a tendencia bizonyítja, hogy a linkek használatával a rosszindulatú kampányok üzemeltetői több kattintást és fertőzést kapnak, mint a fájlok e-mailekhez való csatolásának klasszikus technikája. Nyilvánvaló, hogy a felhasználók bizalmatlanok lettek az e-mailben található mellékletekkel kapcsolatban. Ezzel feliratkozhat webhelyekre, közösségi médiára stb. Hasznos linkek Nézet Várakozás a bejövő e-mailekre Célunk, hogy arra összpontosítsunk, hogy biztonságban tartsuk az interneten. Nagyon értékeljük, köszönöm! Mi az eldobható ideiglenes e-mail Ki vagy te, és miben segíthetünk? Miért van szükségem ideiglenes levélre? Ajánlaná szolgáltatásunkat? Jelenleg nincs zsetonja. Nem akarja megosztani személyes e-mail címét? Fáradt vagy a
 spam? Nem szeretné megosztani a személyes e-mail címét? Fáradt vagy a spamtől? Azért kapja ezt az e-mailt, mert a felhasználói fiókjához jelszót kért a
 %(site_name)s. Az Ön Az Ön ingyenes ideiglenes e-mailje Barátai és kollégái azonnal megtekinthetik a bejövő e-maileket. A beérkezett üzenetek mappája üres Felhasználóneved, ha elfelejtetted: ezelőtt több milliárd postaládák kód célja, hogy megoldja ezeket a problémákat. szakértők kiszámították, hogy 2020 második negyedévében (áprilisi, májusi és júniusi hónap) küldött e-mailek 87%% -a tartalmazott rosszindulatú fájlok letöltésére szolgáló hivatkozásokat, de magukkal a fájlokkal nem. ingyenes weboldal megfigyelés olyan cím, amelyet rövid időre adnak ki: néhány percről egy egész hónapra, majd minden bejövő betűvel és mellékletekkel törlődik, vagy egyszerűen nem érhető el további felhasználásra. platform kifejezetten arra tervezték, hogy segítsen elfelejteni a spam, reklám levelezés, hacker és támadó robotok. Tartsa tisztán és biztonságban valódi postaládáját. ideiglenes, biztonságos, névtelen, ingyenes, eldobható e-mail címet biztosít. feliratkozás 