��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  ;  R;  �   �<  {   =  �   �=  �   8>  :  �>  �  @  �   �A  �   �B  �   jC  �   �C  �   D  j   ,E  �   �E  �   @F  �   �F  �   �G  �   XH  ]  ,I  W  �M  S   �Q  �   6R  
   �R     �R     �R     S     	S     S  /   +S     [S  G   pS  '   �S  ,   �S     T     T     $T     8T     >T     TT     aT     rT     �T     �T  $   �T     �T  
   �T     �T     �T     �T     �T     �T     �T     U     *U     1U     CU     IU     eU     vU     �U     �U     �U     �U     �U     �U     �U     �U     �U     V  )   V  &   5V     \V     wV     }V  5   �V  '   �V  #   �V  -   W  F   AW     �W  8   �W     �W     �W     X     X     X     +X     IX     OX     UX     hX     oX     �X  D   �X     �X    �X  �   �Y  
   �Z     �Z  ;   �Z     :[     J[  %   \[     �[     �[  =   �[     �[     �[     \  5   \  9   K\     �\     �\     �\     �\  h   �\     8]  	   ?]     I]  	   X]     b]     �]  
   �]     �]  j   �]     ^     $^     ;^     K^     c^     x^     �^  *   �^     �^  )   �^  .   _  -   A_  o   o_  H   �_     (`    D`  ;   Ia     �a     �a     �a  E   �a  (   �a  $   b  ,   Cb  (   pb  *   �b  #   �b  Q   �b  R   :c  s   �c     d     d  C   "d     fd  #   ~d     �d     �d     �d  -   �d  �   �d     �e  �   �e  �   �f  G   Ng     �g     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 
                                            100%% Kotak pesan pribadi dengan kepemilikan penuh!
                                         
                                            Hasilkan email palsu dengan satu klik
                                         
                                            Sembunyikan diri Anda dari spam dengan privasi dan keamanan yang ditingkatkan
                                         
                                        Klik “Hapus” di beranda dan Anda akan mendapatkan email sementara baru.
                                     
                                        Alamat email berlaku sampai Anda menghapusnya atau sampai layanan akan menghapusnya.
 Masa hidup tergantung pada banyak faktor yang berbeda.
 Anda dapat memiliki email sementara dengan seumur hidup hingga 1 TAHUN dengan terdaftar
 akun.
                                     
                                        Banyak aplikasi dan situs web mewajibkan Anda untuk memberikan alamat email pribadi tanpa alasan
 selain untuk mengumpulkan informasi pribadi tentang Anda.
 Alamat email sementara memungkinkan Anda untuk memisahkan alamat email pribadi Anda
 Anda gunakan untuk perbankan, pembelian kartu kredit dan identifikasi pribadi lainnya
 transaksi dari semua situs lain di luar sana.
                                     
                                        Mengirim email benar-benar dinonaktifkan dan
 itu tidak akan pernah dilaksanakan karena penipuan dan masalah spam.
                                     
                                        Mereka ditampilkan di bawah email sekali pakai Anda.
 Jika Anda tidak mendapatkan email masuk terlalu lama - klik tombol “Segarkan”.
                                     
                                        Anda dapat berbagi dengan menggunakan kode QR pada halaman utama.
                                     
                                    Anda dapat memiliki maksimum %(max_tokens_allowed)s token.
                                     
                                Kami tidak dapat mengirimkan email ke alamat ini. (Biasanya karena firewall perusahaan
 atau penyaringan.)
                                 
                                Mengapa saya memerlukan akun %(BRAND_NAME)s?
                             
                                Kau sengaja memberi kami alamat email lain. (Biasanya pekerjaan atau pribadi
 bukan yang Anda maksud.)
                                 
                            Jika Anda tidak melihat email dari kami dalam beberapa menit, beberapa
 hal-hal bisa saja terjadi:
                             
                            Kami telah mengirimkan instruksi untuk menyetel kata sandi Anda, jika akun
 ada dengan email yang Anda masukkan. Anda
 harus menerima mereka segera.
                         
                        Jika Anda tidak menerima email, pastikan Anda telah memasukkan alamat yang Anda
 terdaftar
 dengan, dan periksa
 folder spam.
                         
 Platform UptimeMeTrics dirancang untuk memantau status situs web. Ini termasuk seperti
 fitur: Ketersediaan situs web,
 Ketersediaan server,
 Pemeriksaan validitas sertifikat SSL situs web dan validasi HTML.
  
                    %(BRAND_NAME)s adalah layanan alamat email sekali pakai. Ketika Anda mengunjungi %(BRAND_NAME)s email baru
 alamat yang dihasilkan hanya untuk Anda. Alamat email yang dihasilkan dapat segera menerima email apa pun.
 Setiap email yang diterima akan muncul di halaman utama segera. Tidak ada yang lain dari Anda akan melihat
 email
 yang diterima. Penting untuk dicatat bahwa alamat email berakhir setelah 24 jam. Ketika
 surel
 alamat telah kedaluwarsa, alamat email dan email yang diterima akan hilang. Alamat email baru
 kehendak
 dihasilkan saat mengunjungi situs web ini. Ada banyak alasan untuk menggunakan email sekali pakai.
 Anda mungkin ingin memberikan alamat email kepada seseorang tanpa mengungkapkan identitas Anda. Atau Anda mungkin ingin
 mendaftar untuk situs web atau layanan web tetapi Anda khawatir bahwa situs web akan mengirimkan spam
 masa depan. <br/>
 Untuk menggunakan layanan ini Anda HARUS mengaktifkan cookie dan javascript, cookie hanya untuk merekam sesi Anda
 id
 dan preferensi bahasa, Privasi Anda tercakup dalam Kebijakan Privasi dan Cookie kami.
                 
                    %(BRAND_NAME)s adalah layanan alamat email sekali pakai. Saat Anda mengunjungi %(BRAND_NAME)s email baru
 alamat yang dihasilkan hanya untuk Anda. Alamat email yang dihasilkan dapat segera menerima email apa pun.
 Setiap email yang diterima akan segera muncul di halaman utama. Tidak ada orang lain selain Anda akan melihat email
 yang diterima. Penting untuk dicatat bahwa alamat email berakhir setelah 24 jam. Saat email
 alamat telah kedaluwarsa, alamat email dan email yang diterima akan hilang. Alamat email baru akan
 dihasilkan saat mengunjungi situs web ini. Ada banyak alasan untuk menggunakan email sekali pakai.
 Anda mungkin ingin memberikan alamat email kepada seseorang tanpa mengungkapkan identitas Anda. Atau Anda mungkin ingin
 mendaftar untuk situs web atau layanan web tetapi Anda khawatir bahwa situs web akan mengirimkan spam di masa depan. <br />
 Untuk menggunakan layanan ini Anda HARUS mengaktifkan cookie dan javascript, cookie hanya untuk merekam id sesi Anda
 dan preferensi bahasa, Privasi Anda tercakup dalam kebijakan Privasi dan Cookie kami.
                 
                Silakan klik tautan ini untuk mengaktifkan akun Anda:
             
            Terima kasih sudah mendaftar!
 Akun Anda telah dibuat, Anda dapat login setelah Anda mengaktifkan akun Anda dengan menekan url
 di bawah ini.
             Tombol API Tentang Kami Token Akses Alamat Semua Email Semua Hak Dilindungi. Yang perlu Anda ketahui tentang surat sementara Sudah memiliki akun? Terjadi kesalahan saat mencoba masuk melalui akun jejaring sosial Anda. Apakah Anda yakin ingin menghapus email Apakah Anda yakin ingin menghapus token ini. Kembali ke daftar Ubah Periksa email Anda. Tutup Konfirmasi kata sandi Hubungi Kami Kebijakan Cookie Kebijakan Cookie Disalin! Salin Salin alamat email dari masukan atas Buat Buat Email Tanggal Hapus Hapus Email Hapus Token Kotak Masuk Demo Tidak mendapatkan email? Email sekali pakai Domain Tidak punya akun? Email Aktivasi Email diselesaikan Email Sudah ada. Email yang diterima Masukkan Alamat Email Halaman entri Kedaluwarsa FAQ Masukan Lupa kata sandi? Pertanyaan yang Sering Diajukan Dari PERGI Hasilkan Dapatkan Email Sementara Gratis Hari Ini! Dapatkan Email Sementara Anda Hari Ini Dapatkan alamat email saya Pergi Hi sana, saya ingin Bagaimana cara memperpanjang masa pakai alamat email? Bagaimana cara berbagi email sementara? Seberapa mudah ini untuk digunakan? Bagaimana cara memeriksa email yang diterima? Bagaimana cara menghapus email sekali pakai dan mendapatkan yang baru? Bagaimana cara mengirim email? Bagaimana cara menggunakan email sementara sekali pakai? Cara menggunakannya Saya menerima Kotak Masuk Kotak Masuk Mari kita bicara Memuat alamat email sementara Masuk Login Login ke akun Anda Logout Entri lama lainnya Entri terbaru Sekarang Anda bisa login ke akun Anda dan mulai menggunakan layanan. Kontak Kami Layanan kami yang kuat dan aman memungkinkan Anda untuk mendapatkan benar-benar anonim dan pribadi
 alamat email yang Anda
 dapat menggunakan segera.
 Sebenarnya Anda dapat membuat Anda memiliki alamat email sementara dan menggunakannya setiap kali Anda
 mau. Layanan kami yang kuat dan aman memungkinkan Anda mendapatkan alamat email yang benar-benar anonim dan pribadi yang Anda
 dapat digunakan segera.
 Sebenarnya Anda dapat membuat Anda memiliki alamat email sementara dan menggunakannya kapan pun Anda mau. Kata Sandi Reset Kata Sandi Silakan pergi ke halaman berikut dan pilih kata sandi baru: Artikel Populer Kebijakan Privasi Proyek diuji dengan kotak masuk palsu Konten Baku Kirim ulang email aktivasi Baca email masuk pada halaman ini di dalam bagian kotak masuk Segarkan Pendaftaran Selesai Reset Kata Sandi Tautan ulang kata sandi akan dikirim pada email Anda. Hemat waktu Anda dengan tidak perlu membuat alamat email. Memindai kode QR dengan ponsel Kirim Masukan Pengirim Kotak Masuk Bersama Tunjukkan kasih sayang kepada kami dengan merekomendasikan kami ke jaringan atau komunitas favorit Anda! Daftar Mendaftar Daftar gratis! Mendaftar Kegagalan Login Jaringan Sosial Email Spam Terdeteksi Bebas Spam Mulai Sekarang Berhenti menerima email sampah dan lupakan tombol “filter” kotak masuk dan “berhenti berlangganan” Subjek Ceritakan tentang kami Email Sementara Layanan Email Sementara Syarat dan Ketentuan Ketentuan Penggunaan Terima kasih sudah mendaftar! Terima kasih telah menggunakan situs kami! Tim %(site_name)s Akun yang Anda coba aktifkan tidak valid. Akun yang Anda coba aktifkan telah diaktifkan. Kunci aktivasi yang Anda berikan tidak valid. Alamat email yang Anda masukkan memiliki kesalahan atau kesalahan ketik. (Terjadi pada yang terbaik dari kita.) Email ada di folder spam Anda. (Kadang-kadang hal-hal tersesat di sana.) Akun ini telah kedaluwarsa. Tren ini membuktikan bahwa dengan menggunakan tautan, operator kampanye berbahaya menerima lebih banyak klik dan infeksi dibandingkan dengan teknik klasik melampirkan file ke email. Jelas, pengguna telah menjadi tidak percaya pada lampiran apa pun dalam surat. Gunakan ini untuk mendaftar di situs web, media sosial, dll Tautan Berguna Lihat Menunggu email masuk Kami bertujuan untuk fokus untuk menjaga Anda tetap aman di internet. Kami sangat menghargainya, terima kasih! Apa itu email sementara sekali pakai Siapa kau, dan bagaimana kami bisa membantu? Mengapa saya memerlukan surat sementara? Apakah Anda merekomendasikan layanan kami? Anda tidak memiliki token saat ini. Anda tidak ingin berbagi alamat email pribadi Anda? Apakah Anda lelah dari
 Spam? Anda tidak ingin berbagi alamat email pribadi Anda? Apakah Anda lelah dengan spam? Anda menerima email ini karena Anda meminta pengaturan ulang kata sandi untuk akun pengguna Anda di
 %(site_name)s. Anda Email Sementara Gratis Anda Teman dan kolega Anda bisa mendapatkan tampilan instan email masuk. Kotak masuk Anda kosong Nama pengguna Anda, jika Anda lupa: lalu miliaran kotak masuk kode direka untuk menyelesaikan semua masalah ini. para ahli menghitung bahwa 87%% dari semua spam email yang dikirim pada kuartal kedua 2020 (bulan April, Mei dan Juni) berisi tautan untuk mengunduh file berbahaya, namun bukan lampiran dengan file itu sendiri. pemantauan situs web gratis adalah alamat yang dikeluarkan untuk waktu yang singkat: dari beberapa menit sampai satu bulan penuh, dan kemudian akan dihapus dengan semua huruf dan lampiran yang masuk, atau tidak tersedia untuk penggunaan lebih lanjut. dirancang khusus untuk membantu Anda melupakan spam, surat iklan, hacking dan menyerang robot. Jaga agar kotak surat Anda tetap bersih dan aman. menyediakan alamat email sementara, aman, anonim, gratis, sekali pakai. pendaftaran 