��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  B  R;  �   �<  t   #=  �   �=  �   %>  4  �>  �  �?  �   �A  �   �B  �   gC  �   �C  �   �D  n   1E  �   �E  �   [F  �   �F  �   �G    hH  �  oI  �  �M  R   �R  �   �R  
   {S     �S     �S  	   �S     �S     �S  3   �S     T  i   +T  &   �T  +   �T     �T     �T      U     U     U  
   1U     <U     PU     dU     mU  /   sU     �U     �U     �U     �U     �U     �U     �U     �U     V     !V     )V     =V     DV     bV     uV     �V     �V     �V     �V     �V     �V     �V     �V     �V     �V  -    W  &   .W     UW     tW     xW  6   �W  ,   �W      �W  $   
X  ;   /X     kX  .   �X     �X  
   �X     �X     �X     �X  .   �X     Y     &Y     -Y     CY     RY     eY  E   wY     �Y    �Y  �   �Z     �[     �[  5   �[     \     ,\  +   F\     r\  (   �\  V   �\     ]     ]     %]  I   8]  :   �]  '   �]     �]     �]     �]  K   ^  
   c^  
   n^     y^  
   �^  #   �^     �^  
   �^  
   �^  i   �^     R_     [_     m_     ~_     �_     �_     �_  *   �_     `  4   `  =   N`  /   �`  d   �`  I   !a     ka    �a  5   �b  
   �b  
   �b     �b  F   �b     :c  &   Zc  !   �c  '   �c  "   �c     �c  L   d  M   Ud  �   �d     0e      7e  ^   Xe      �e  2   �e     f     f     +f  /   2f  �   bf     4g  �   Sg  �   4h  P   �h  
   )i     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                            Cassette postali private al 100%% con piena proprietà!
                                         
                                            Genera email false con un clic
                                         
                                            Nasconditi dallo spam con maggiore privacy e sicurezza
                                         
                                        Fai clic su «Elimina» nella home page e riceverai una nuova e-mail temporanea.
                                     
                                        L'indirizzo e-mail è valido fino a quando non lo elimini o fino a quando il servizio non lo eliminerà.
 La durata dipende da molti fattori diversi.
 Puoi avere e-mail temporanee con durata fino a 1 ANNO con registrazione
 conto.
                                     
                                        Molte app e siti web obbligano l'utente a fornire un indirizzo e-mail personale senza motivo
 diverso da raccogliere informazioni personali su di te.
 L'indirizzo e-mail temporaneo consente di separare il tuo indirizzo e-mail personale
 utilizzi per operazioni bancarie, acquisti con carta di credito e altri identificativi personali
 transazioni da tutti gli altri siti là fuori.
                                     
                                        L'invio di e-mail è completamente disabilitato e
 non verrà mai implementato a causa di problemi di frode e spam.
                                     
                                        Vengono visualizzati sotto la tua e-mail usa e getta.
 Se non hai ricevuto e-mail in arrivo per troppo tempo, fai clic sul pulsante «Aggiorna».
                                     
                                        Puoi condividerlo utilizzando il codice QR sulla pagina principale.
                                     
                                    Puoi avere un numero massimo di token %(max_tokens_allowed)s.
                                     
                                Non possiamo consegnare l'e-mail a questo indirizzo. (Di solito a causa di firewall aziendali
 o filtraggio.)
                                 
                                Perché ho bisogno di un account %(BRAND_NAME)s?
                             
                                Ci hai dato accidentalmente un altro indirizzo email. (Di solito un lavoro o personale
 invece di quello che intendevi.)
                                 
                            Se non vedi un'e-mail da noi entro pochi minuti, alcuni
 sarebbero potute succedere cose:
                             
                            Ti abbiamo inviato via email le istruzioni per impostare la password, se un account
 esiste con l'e-mail che hai inserito. Tu
 dovrebbe riceverli a breve.
                         
                        Se non ricevi un'e-mail, assicurati di aver inserito l'indirizzo
 registrato
 con, e controlla il tuo
 cartella spam.
                         
 La piattaforma UpTimeMetrics è progettata per monitorare lo stato dei siti web. Esso comprende tali
 caratteristiche: disponibilità del sito web,
 Disponibilità del server,
 Verifica della validità del certificato SSL del sito web e della convalida HTML.
  
                    %(BRAND_NAME)s è un servizio di indirizzi e-mail usa e getta. Quando visiti %(BRAND_NAME)s una nuova email
 l'indirizzo è generato solo per te. L'indirizzo e-mail generato può ricevere immediatamente qualsiasi e-mail.
 Qualsiasi e-mail ricevuta verrà visualizzata immediatamente sulla pagina principale. Nessuno oltre a te vedrà
 le email
 che vengono ricevuti. È importante notare che un indirizzo e-mail scade dopo 24 ore. Quando un
 e-mail
 l'indirizzo è scaduto, l'indirizzo e-mail e tutte le e-mail ricevute andranno perduti. Un nuovo indirizzo email
 volontà
 essere generato quando si visita questo sito Web. Ci sono molte ragioni per utilizzare un'e-mail usa e getta.
 Potresti voler fornire a qualcuno un indirizzo email senza rivelare la tua identità. Oppure potresti volerlo
 iscriversi a un sito Web o a un servizio Web ma si teme che il sito Web invii spam
 il futuro. <br/>
 Per utilizzare questo servizio DEVI abilitare cookie e javascript, il cookie è solo per registrare la tua sessione
 id
 e la lingua preferita, la tua privacy è coperta dalla nostra politica sulla privacy e sui cookie.
                 
                    %(BRAND_NAME)s è un servizio di indirizzi e-mail usa e getta. Quando visiti %(BRAND_NAME)s una nuova email
 l'indirizzo è generato solo per te. L'indirizzo e-mail generato può ricevere immediatamente qualsiasi e-mail.
 Qualsiasi e-mail ricevuta verrà visualizzata immediatamente sulla pagina principale. Nessun altro che tu vedrà le email
 che vengono ricevuti. È importante notare che un indirizzo e-mail scade dopo 24 ore. Quando un'e-mail
 l'indirizzo è scaduto, l'indirizzo e-mail e le e-mail ricevute non saranno più. Un nuovo indirizzo email
 essere generato al momento della visita di questo sito web. Ci sono molte ragioni per usare un'e-mail usa e getta.
 Potresti voler fornire a qualcuno un indirizzo e-mail senza rivelare la tua identità. Oppure potresti volerlo
 iscriviti a un sito Web o a un servizio web ma sei preoccupato che il sito web ti invierà spam in futuro. <br />
 Per utilizzare questo servizio DEVI abilitare cookie e javascript, il cookie è solo per registrare il tuo ID di sessione
 e preferenze linguistiche, la tua Privacy è coperta dalla nostra Informativa sulla privacy e sui cookie.
                 
                Fai clic su questo link per attivare il tuo account:
             
            Grazie per esserti isso!
 Il tuo account è stato creato, puoi effettuare il login dopo aver attivato il tuo account premendo l'URL
 sotto.
             Chiavi API Informazioni su di noi Token di accesso Indirizzo Tutte le email Tutti i diritti riservati. Tutto quello che devi sapere sulla posta temporanea Hai già un account? Si è verificato un errore durante il tentativo di accedere tramite il proprio account di social network. Sei sicuro di voler eliminare l'email? Sei sicuro di voler eliminare questo token. Torna alla lista Cambia Controlla la tua email. Chiudi Conferma password Contattaci Politica sui cookie Politica sui cookie Copiato! Copia Copiare l'indirizzo e-mail dall'input superiore Crea Crea e-mail Data Elimina Elimina e-mail Elimina token Posta in arrivo demo Non hai ricevuto un'email? E-mail usa e getta Dominio Non hai un account? E-mail Attivazione e-mail completata Email già esiste. Email ricevute Inserisci l'indirizzo e-mail Pagina voci Scade FAQ Feedback Hai dimenticato la password? Domande frequenti Da ANDARE Genera Ricevi oggi la tua email temporanea gratuita! Ricevi subito la tua e-mail temporanea Ottieni il mio indirizzo email Vai Ciao, vorrei Come posso prolungare la durata dell'indirizzo e-mail? Come faccio a condividere e-mail temporanee? Quanto è stato facile da usare? Come controllare le e-mail ricevute? Come eliminare le e-mail usa e getta e ottenerne una nuova? Come inviare e-mail? Come utilizzare e-mail temporanee usa e getta? Come usarlo Accetto il Posta in arrivo Caselle di posta Parliamo Caricamento del tuo indirizzo email temporaneo Accedi Accedi Accedi al tuo account Disconnettersi Altre voci vecchie Voci più recenti Ora puoi accedere al tuo account e iniziare a utilizzare il servizio. I nostri contatti Il nostro servizio robusto e sicuro ti consente di ottenere un servizio completamente anonimo e privato
 indirizzo email che tu
 può essere utilizzato immediatamente.
 In realtà puoi creare il tuo indirizzo email temporaneo e usarlo ogni volta che vuoi
 voglio. Il nostro servizio robusto e sicuro ti consente di ottenere un indirizzo email completamente anonimo e privato
 può essere utilizzato immediatamente.
 In realtà puoi creare il tuo indirizzo email temporaneo e utilizzarlo quando vuoi. Password Reimpostazione password Vai alla pagina seguente e scegli una nuova password: Articoli popolari Informativa sulla privacy Progetti testati con caselle di posta false Contenuto grezzo Invia nuovamente l'e-mail di attivazione Leggere le e-mail in arrivo in questa pagina all'interno della sezione Posta in arrivo Aggiorna Registrazione completata Reimposta password Il link di reimpostazione della password verrà inviato sulla tua e-mail. Risparmia tempo senza dover mai creare un indirizzo email. Scansiona il codice QR con il cellulare Invia feedback Mittente Posta in arrivo condivisa Mostraci un po' d'amore consigliandoci alla tua rete o comunità preferita! Registrati Registrati Registrati gratis! Registrati Errore di accesso ai social network Rilevate email di spam Senza Spam Inizia ora Interrompi la ricezione di email del cestino e dimentica i pulsanti «filtri» e «annulla l'iscrizione» Soggetto Raccontaci di noi Email temporanea Servizio e-mail temporaneo Termini e condizioni Condizioni d'uso Grazie per esserti isso! Grazie per aver utilizzato il nostro sito! Il team %(site_name)s L'account che hai tentato di attivare non è valido. L'account che hai tentato di attivare è già stato attivato. La chiave di attivazione fornita non è valida. L'indirizzo e-mail inserito ha avuto un errore o un errore di battitura. (Succede al meglio di noi.) L'e-mail è nella cartella spam. (A volte le cose si perdono lì dentro.) Questo account è scaduto. Questa tendenza dimostra che utilizzando i link, gli operatori di campagne dannose ricevono più clic e infezioni rispetto alla classica tecnica di allegare file alle e-mail. Ovviamente, gli utenti sono diventati diffidenti di qualsiasi allegato nella posta. Usalo per registrarsi su siti web, social media, ecc. Link utili Visualizza In attesa di e-mail in arrivo Il nostro obiettivo è concentrarci su Internet per tenerti al sicuro. Lo apprezziamo davvero, grazie! Cos'è l'e-mail temporanea usa e getta Chi sei e come possiamo aiutarti? Perché ho bisogno di posta temporanea? Consiglieresti il nostro servizio? Al momento non hai token. Non vuoi condividere il tuo indirizzo email personale? Sei stanco del
 spam? Non vuoi condividere il tuo indirizzo email personale? Sei stanco dello spam? Stai ricevendo questa e-mail perché hai richiesto una reimpostazione della password per il tuo account utente all'indirizzo
 %(site_name)s. Il tuo La tua email temporanea gratuita I tuoi amici e colleghi possono ottenere una visualizzazione immediata delle e-mail in arrivo. La tua casella di posta è vuota Il tuo nome utente, nel caso tu abbia dimenticato: fa miliardi di caselle di posta codice progettato per risolvere tutti questi problemi. hanno calcolato che l'87%% di tutte le e-mail spam inviate nel secondo trimestre del 2020 (mesi di aprile, maggio e giugno) conteneva collegamenti per scaricare file dannosi, ma non allegati con i file stessi. monitoraggio gratuito del sito è un indirizzo che viene rilasciato per un breve periodo: da pochi minuti a un mese intero, e quindi verrà eliminato con tutte le lettere e gli allegati in arrivo, o semplicemente non disponibile per un ulteriore utilizzo. è appositamente progettata per aiutarti a dimenticare spam, mailing pubblicitari, hacking e attacchi robot. Mantieni la tua vera cassetta postale pulita e sicura. fornisce un indirizzo email temporaneo, sicuro, anonimo, gratuito e usa e getta. iscrizione 