Þ    ª      l  ã   ¼
      H     I  x   Ì     E     Ö    j  :    Ù   =  ï     }     }     Æ     c   Ê  Ð   .  ±   ÿ  û   ±  ù   ­  L  §  .  ô  Ë  #"  N   ï&  ¶   >'     õ'     þ'     (     (  
   (     '(  )   <(     f(  L   (  %   Ì(  +   ò(     )     +)     2)     D)     J)  
   [)     f)     t)     )     )  %   )     ¶)     ½)     Ê)     Ï)     Ö)     ã)  
   ð)     û)     *     #*     **     A*     G*     b*     x*     *     *     ©*     ±*     µ*     ¾*     Ï*     ê*     ï*     ò*  $   û*      +     ?+     T+     W+  2   q+     ¤+     Ä+     Þ+  3   ü+     0,  &   C,     j,     x,     ,     ,  
   ,  $   ,     Ã,     Ê,     Ð,     æ,     í,     þ,  <   -     O-  #  \-  ó   .     t/     }/  :   /     Ç/     Ø/  !   ç/     	0     0  6   .0     e0     m0     0  /   0  :   Â0     ý0     1     *1     11  K   >1     1     1     1     ¬1     ³1     Ð1  	   å1  	   ï1  V   ù1     P2     X2     f2     v2     2     £2     °2     Ç2     â2  1   ù2  =   +3  +   i3  Q   3  G   ç3     /4  ó   I4  2   =5     p5     }5     5  A   5      à5  "   6  !   $6     F6      d6  $   6  g   ª6  S   7  r   f7     Ù7     Þ7  C   ø7     <8  *   P8     {8     8     8  %   8  Ê   ¾8     9  Á   ¡9      c:  F   ;     K;  ;  R;  ¡   <     0=  §   ´=  ´   \>  ]  ?  ?  o@  Ñ   ¯B  ì   C     nD     E  ô    E  ~   F  Ï   G  ²   äG     H  Å   I  >  ^J  2  K  -  ÐQ  ~   þW  Þ   }X  
   \Y     gY     zY     Y     Y     °Y  B   ÏY  0   Z     CZ  0   ÅZ  <   öZ     3[     F[  -   S[  	   [     [     ¤[     ·[     Ð[     é[     ü[  ?   \     H\     O\     b\     i\     p\     \     \  !   ¯\     Ñ\     ç\  '   ô\  
   ]  B   ']  %   j]     ]  *   ¯]     Ú]     ð]     ý]     ^  '   &^     N^     a^     h^     o^  <   v^  -   ³^     á^      _  !   _  Z   )_  E   _  6   Ê_  *   `  N   ,`     {`  *   `  	   Å`     Ï`     å`     õ`  	   a  *   a     =a     Ja  $   Wa     |a     a     ¨a  Z   Áa     b    2b  -  Rc     d     d  W   ¬d     e     e  <   *e     ge  6   }e  N   ´e     f     f     #f  K   Bf  ]   f  )   ìf     g  	   5g     ?g     Ug     ×g     êg     ýg     h  6   )h  *   `h     h     h  ~   «h     *i     1i     Pi     `i     |i     i  3   i  E   Êi     j  E   (j  f   nj  ?   Õj  z   k  r   k  -   l  }  1l  l   ¯m     n     5n  $   >n     cn  3   ån  0   o  K   Jo  0   o  3   Ço  0   ûo  q   ,p  ]   p     üp     q     q  E   °q     öq  H   r     [r     br  	   r  N   r  :  Úr  !   t  ô   7t  ö   ,u  l   #v     v     9   /   ,   \             h      l             ¤          ^       =         u                 $      ¥   Z   ¨   ¡   w      U   k      7   T   3          6           F         f   &      O   ¢               E   !   c             ~   ]           j   J       8       o   £               0      D   ¦   §           5      B      
   |          -   4   <      a             g   {      R   p   r           A   >      I      Y   _   (      t   v           m   e   n          +   2   :   M           X         Q          W             `       "   s   [                                z           .          %       L              q   @       S   d       }   V   '                      P             H               y                               ?   G          ©       x   i          N   #      *   1   K   C      	   b       ª       )          ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We canât deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you donât see an email from us within a few minutes, a few
                            things could have happened:
                             
                            Weâve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you donât receive an email, please make sure youâve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Websiteâs availability,
                        Server availability,
                        Validity check of websiteâs SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didnât get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case youâve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 
                                            100%% å®å¨ãªæææ¨©ãæã¤ãã©ã¤ãã¼ãã¡ã¼ã«ããã¯ã¹ï¼
                                         
                                            ã¯ã³ã¯ãªãã¯ã§å½ã®ã¡ã¼ã«ãçæ
                                         
                                            ãã©ã¤ãã·ã¼ã¨ã»ã­ã¥ãªãã£ãå¼·åãã¦ã¹ãã ããèº«ãé ã
                                         
                                        ãã¼ã ãã¼ã¸ã®ãåé¤ããã¯ãªãã¯ããã¨ãæ°ããä¸æã¡ã¼ã«ãå±ãã¾ãã
                                     
                                        ã¡ã¼ã«ã¢ãã¬ã¹ã¯ãåé¤ãããããµã¼ãã¹ãåé¤ããã¾ã§æå¹ã§ãã
 å¯¿å½ã¯å¤ãã®ç°ãªãè¦å ã«ä¾å­ãã¾ãã
 ç»é²ãããç¶æã§æå¤§1å¹´éã®çæ¶¯ã®ä¸æã¡ã¼ã«ãåãåããã¨ãã§ãã¾ã
 ã¢ã«ã¦ã³ãã
                                     
                                        å¤ãã®ã¢ããªãã¦ã§ããµã¤ãã¯ãçç±ããªãåäººç¨ã®ã¡ã¼ã«ã¢ãã¬ã¹ãæä¾ãããã¨ãç¾©åä»ãã¦ãã¾ãã
 ãå®¢æ§ã«é¢ããåäººæå ±ãåéããä»¥å¤ã
 ä¸æçãªé»å­ã¡ã¼ã«ã¢ãã¬ã¹ãä½¿ç¨ããã¨ãåäººã®ã¡ã¼ã«ã¢ãã¬ã¹ãåºåããã¨ãã§ãã¾ãã
 éè¡ãã¯ã¬ã¸ããã«ã¼ãã®è³¼å¥ããã®ä»ã®åäººè­å¥ã«ä½¿ç¨ãã
 ããã«ããä»ã®ãã¹ã¦ã®ãµã¤ãããã®ãã©ã³ã¶ã¯ã·ã§ã³ã
                                     
                                        ã¡ã¼ã«ã®éä¿¡ã¯å®å¨ã«ç¡å¹ã«ãªãã
 è©æ¬ºãã¹ãã ã®åé¡ã§å®è£ããããã¨ã¯æ±ºãã¦ããã¾ããã
                                     
                                        ãããã¯ä½¿ãæ¨ã¦ã¡ã¼ã«ã®ä¸ã«è¡¨ç¤ºããã¾ãã
 åä¿¡ã¡ã¼ã«ãé·ãå±ããªãå ´åã¯ã[æ´æ°] ãã¿ã³ãã¯ãªãã¯ãã¾ãã
                                     
                                        ã¡ã¤ã³ãã¼ã¸ã®QRã³ã¼ããä½¿ç¨ãã¦å±æã§ãã¾ãã
                                     
                                    æå¤§ã§ %(max_tokens_allowed)s åã®ãã¼ã¯ã³ãæã¤ãã¨ãã§ãã¾ãã
                                     
                                ãã®ã¢ãã¬ã¹ã«ã¡ã¼ã«ãéä¿¡ãããã¨ã¯ã§ãã¾ããã(éå¸¸ãä¼æ¥­ã®ãã¡ã¤ã¢ã¦ã©ã¼ã«ãåå ã§ãã
 ã¾ãã¯ãã£ã«ã¿ãªã³ã°ãã¾ãã)
                                 
                                ãªã %(BRAND_NAME)s ã¢ã«ã¦ã³ããå¿è¦ãªã®ã§ããï¼
                             
                                èª¤ã£ã¦å¥ã®ã¡ã¼ã«ã¢ãã¬ã¹ãæ¸¡ãã¾ããã(éå¸¸ãä»äºãåäººã®ãã®
 ãããæå³ãããã®ã®ä»£ããã«ãï¼
                                 
                            æ°åä»¥åã«å½ç¤¾ããã®ã¡ã¼ã«ãè¡¨ç¤ºãããªãå ´åã¯ãå°æ°
 äºæãèµ·ãã£ããããããªã:
                             
                            ã¢ã«ã¦ã³ãã®å ´åããã¹ã¯ã¼ãã®è¨­å®æé ãã¡ã¼ã«ã§ãéããã¾ãã
 å¥åããé»å­ã¡ã¼ã«ã¨ã¨ãã«å­å¨ãã¾ãããåã
 ããã«åãåããã¯ãã§ãã
                         
                        ã¡ã¼ã«ãå±ããªãå ´åã¯ãä½æãå¥åããã¦ãããã¨ãç¢ºèªãã¦ãã ãã
 ç»é²æ¸ã¿
 ã§ã
 ã¹ãã ãã©ã«ãã
                         
 uptimeMetricsãã©ãããã©ã¼ã ã¯ãã¦ã§ããµã¤ãã®ç¶æãç£è¦ããããã«è¨­è¨ããã¦ãã¾ããããã¯ãã®ãããªãã®ãå«ãã§ãã
 æ©è½ï¼ã¦ã§ããµã¤ãã®å¯ç¨æ§ã
 ãµã¼ãã®å¯ç¨æ§ã
 ã¦ã§ããµã¤ãã®SSLè¨¼ææ¸ã®æå¹æ§ãã§ãã¯ã¨HTMLæ¤è¨¼ã
  
                    %(BRAND_NAME)s ã¯ä½¿ãæ¨ã¦ã®Eã¡ã¼ã«ã¢ãã¬ã¹ãµã¼ãã¹ã§ãã%(BRAND_NAME)s ã«ã¢ã¯ã»ã¹ããã¨ãæ°ããã¡ã¼ã«ãå±ãã¾ã
 ä½æã¯ããªãã®ããã ãã«çæããã¾ããçæãããé»å­ã¡ã¼ã«ã¢ãã¬ã¹ã¯ããã¹ã¦ã®é»å­ã¡ã¼ã«ãå³åº§ã«åä¿¡ã§ãã¾ãã
 åä¿¡ããã¡ã¼ã«ã¯ãããã«ã¡ã¤ã³ãã¼ã¸ã«è¡¨ç¤ºããã¾ãããåä»¥å¤ã«èª°ãè¦ããªã
 ãã®ã¡ã¼ã«ã¯
 åãåã£ããã¡ã¼ã«ã¢ãã¬ã¹ã¯ 24 æéå¾ã«æéåãã«ãªããã¨ã«æ³¨æãã¦ãã ããããã¤
 Eã¡ã¼ã«
 ã¢ãã¬ã¹ã®æå¹æéãåããå ´åãã¡ã¼ã«ã¢ãã¬ã¹ã¨åä¿¡ããã¡ã¼ã«ã¯ãã¹ã¦å¤±ããã¾ããæ°ããã¡ã¼ã«ã¢ãã¬ã¹
 ã ãã
 ãã®ã¦ã§ããµã¤ããè¨ªåããã¨çæããã¾ããä½¿ãæ¨ã¦ã¡ã¼ã«ãä½¿ç¨ããçç±ã¯ããããããã¾ãã
 èº«åãæãããã«èª°ãã«ã¡ã¼ã«ã¢ãã¬ã¹ãä¼ãããã¨æãããããã¾ãããã¾ãã¯ãããªãããããããããã¾ãã
 ã¦ã§ããµã¤ãã¾ãã¯ã¦ã§ããµã¼ãã¹ã«ç»é²ãããããã®ã¦ã§ããµã¤ãããã¹ãã ãéä¿¡ãããã®ã§ã¯ãªããã¨å¿éãã¦ãã
 æªæ¥ã<br/>
 ãã®ãµã¼ãã¹ãä½¿ç¨ããã«ã¯ãCookieã¨JavaScriptãæå¹ã«ããå¿è¦ãããã¾ããCookieã¯ã»ãã·ã§ã³ãè¨é²ããããã®ãã®ã§ã
 id
 ããã³è¨èªè¨­å®ã®å ´åããå®¢æ§ã®ãã©ã¤ãã·ã¼ã¯å½ç¤¾ã®ãã©ã¤ãã·ã¼ããã³Cookieããªã·ã¼ã®å¯¾è±¡ã¨ãªãã¾ãã
                 
                    %(BRAND_NAME)s ã¯ä½¿ãæ¨ã¦ã®é»å­ã¡ã¼ã«ã¢ãã¬ã¹ãµã¼ãã¹ã§ãã%(BRAND_NAME)s ã«ã¢ã¯ã»ã¹ããã¨ãæ°ããã¡ã¼ã«ãå±ãã¾ã
 ã¢ãã¬ã¹ã¯ããªãã®ããã ãã«çæããã¾ããçæãããé»å­ã¡ã¼ã«ã¢ãã¬ã¹ã¯ãé»å­ã¡ã¼ã«ãããã«åä¿¡ã§ãã¾ãã
 åä¿¡ããã¡ã¼ã«ã¯ãããã«ã¡ã¤ã³ãã¼ã¸ã«è¡¨ç¤ºããã¾ããããªãä»¥å¤ã®èª°ãã¡ã¼ã«ã¯è¦ãã¾ãã
 ãåä¿¡ããã¾ããé»å­ã¡ã¼ã«ã¢ãã¬ã¹ã¯ 24 æéå¾ã«æéåãã«ãªãç¹ã«æ³¨æãã¦ãã ãããã¡ã¼ã«ãéä¿¡ãããã¨ã
 ã¢ãã¬ã¹ã®æå¹æéãåãã¦ãã¾ããé»å­ã¡ã¼ã«ã¢ãã¬ã¹ã¨åä¿¡ããã¡ã¼ã«ã¯ãã¹ã¦æ¶ãã¾ããæ°ããã¡ã¼ã«ã¢ãã¬ã¹ã¯
 ãã®ã¦ã§ããµã¤ããè¨ªåããã¨ãã«çæããã¾ããä½¿ãæ¨ã¦ã¡ã¼ã«ãä½¿ç¨ããçç±ã¯ããããããã¾ãã
 èº«åãæãããã¨ãªããèª°ãã«ã¡ã¼ã«ã¢ãã¬ã¹ãä¸ãããã¨æãããããã¾ãããããã¨ãã
 ã¦ã§ããµã¤ãã¾ãã¯ã¦ã§ããµã¼ãã¹ã«ãµã¤ã³ã¢ãããã¾ãããä»å¾ã¦ã§ããµã¤ããã¹ãã ãéä¿¡ãããã¨ãæ¸å¿µãã¦ãã¾ãã<br />
 ãã®ãµã¼ãã¹ãå©ç¨ããã«ã¯ãã¯ãã­ã¼ã¨JavaScriptãæå¹ã«ããå¿è¦ãããã¾ããã¯ãã­ã¼ã¯ã»ãã·ã§ã³IDãè¨é²ããããã ãã§ãã
 è¨èªãåªåãããã¨ããå®¢æ§ã®ãã©ã¤ãã·ã¼ã¯å½ç¤¾ã®ãã©ã¤ãã·ã¼ã¨ã¯ãã­ã¼ããªã·ã¼ã®å¯¾è±¡ã¨ãªãã¾ãã
                 
                ãã®ãªã³ã¯ãã¯ãªãã¯ãã¦ãã¢ã«ã¦ã³ããã¢ã¯ãã£ãåãã¦ãã ããã
             
            ãµã¤ã³ã¢ãããã¦ããã¦ãããã¨ãï¼
 ã¢ã«ã¦ã³ããä½æããã¾ãããURLãæ¼ãã¦ã¢ã«ã¦ã³ããã¢ã¯ãã£ãã¼ãããå¾ã«ã­ã°ã¤ã³ã§ãã¾ã
 ä»¥ä¸ã
             API ã­ã¼ å½ç¤¾ã«ã¤ãã¦ ã¢ã¯ã»ã¹ãã¼ã¯ã³ ä½æ ãã¹ã¦ã®ã¡ã¼ã« ãã¹ã¦ã®æ¨©å©ãä¿æã ä¸æã¡ã¼ã«ã«ã¤ãã¦ç¥ã£ã¦ããã¹ããã¨ã¯ãã¹ã¦ ãã§ã«ã¢ã«ã¦ã³ãããæã¡ã§ããï¼ ã½ã¼ã·ã£ã«ãããã¯ã¼ã¯ã¢ã«ã¦ã³ãçµç±ã§ã­ã°ã¤ã³ãããã¨ããã¨ãã«ã¨ã©ã¼ãçºçãã¾ããã ã¡ã¼ã«ãåé¤ãã¦ãããããã§ãã ãã®ãã¼ã¯ã³ãåé¤ãã¦ãããããã§ããã ãªã¹ãã«æ»ã å¤æ´ãã ã¡ã¼ã«ããã§ãã¯ãã¦ãã ããã éãã ãã¹ã¯ã¼ãã®ç¢ºèª ãåãåãã ã¯ãã­ã¼ããªã·ã¼ ã¯ãã­ã¼ããªã·ã¼ ã³ãã¼ããï¼ [ã³ãã¼] ä¸çªä¸ã®å¥åããã¡ã¼ã«ã¢ãã¬ã¹ãã³ãã¼ãã ä½æ ã¡ã¼ã«ã®ä½æ æ¥ä» åé¤ ã¡ã¼ã«ãåé¤ ãã¼ã¯ã³ã®åé¤ ãã¢åä¿¡ãã¬ã¤ ã¡ã¼ã«ãå±ããªãã£ãï¼ ä½¿ãæ¨ã¦ã¡ã¼ã« ãã¡ã¤ã³ ã¢ã«ã¦ã³ããæã£ã¦ããªãï¼ Eã¡ã¼ã« é»å­ã¡ã¼ã«ã®ã¢ã¯ãã£ãã¼ã·ã§ã³ãå®äºãã¾ãã Eã¡ã¼ã«ã¯æ¢ã«å­å¨ãã¾ãã ã¡ã¼ã«ãåä¿¡ãã¾ãã é»å­ã¡ã¼ã«ã¢ãã¬ã¹ãå¥åãã ã¨ã³ããªãã¼ã¸ æéåã ããããè³ªå ãã£ã¼ãããã¯ ãã¹ã¯ã¼ãããå¿ãã§ããï¼ ããããè³ªå ãã è¡ã çæ ä»ããç¡æã®ä¸æã¡ã¼ã«ãå¥æãã¾ãããï¼ ä»ããä¸æçãªã¡ã¼ã«ãåãåã ã¡ã¼ã«ã¢ãã¬ã¹ãåå¾ è¡ã ããã«ã¡ã¯ãç§ã¯ããã ã¡ã¼ã«ã¢ãã¬ã¹ã®æå¹æéãå»¶é·ããã«ã¯ã©ããããããã§ããï¼ ä¸æã¡ã¼ã«ãå±æããã«ã¯ã©ããããããã§ããï¼ ããã¯ã©ããããç°¡åã«ä½¿ãã¾ãããï¼ åä¿¡ããã¡ã¼ã«ã®ç¢ºèªæ¹æ³ã¯ï¼ ä½¿ãæ¨ã¦ã¡ã¼ã«ãåé¤ãã¦æ°ããã¡ã¼ã«ãå¥æããã«ã¯ï¼ ã¡ã¼ã«ã®éä¿¡æ¹æ³ã¯ï¼ ä½¿ãæ¨ã¦ã®ä¸æã¡ã¼ã«ã®ä½¿ãæ¹ ä½¿ãæ¹ ç§ã¯åãå¥ãã åä¿¡ãã¬ã¤ åä¿¡ããã¯ã¹ è©±ãã ä¸æã¡ã¼ã«ã¢ãã¬ã¹ã®èª­ã¿è¾¼ã¿ ã­ã°ã¤ã³ ã­ã°ã¤ã³ ã¢ã«ã¦ã³ãã«ã­ã°ã¤ã³ãã ã­ã°ã¢ã¦ã ãã£ã¨å¤ãã¨ã³ããª æè¿ã®ã¨ã³ããªã¼ ããã§ã¢ã«ã¦ã³ãã«ã­ã°ã¤ã³ãã¦ãµã¼ãã¹ã®ä½¿ç¨ãéå§ã§ãã¾ãã ç§ãã¡ã®é£çµ¡å å½ç¤¾ã®å ç¢ã§å®å¨ãªãµã¼ãã¹ã«ãããå®å¨ã«å¿åã§ãã©ã¤ãã¼ããª
 ãåãç»é²ããã¡ã¼ã«ã¢ãã¬ã¹
 ããã«ä½¿ç¨ã§ãã¾ãã
 å®éã«ã¯ãèªåã®ä¸æçãªã¡ã¼ã«ã¢ãã¬ã¹ãä½æãã¦ããã¤ã§ãä½¿ç¨ã§ãã¾ã
 æ¬²ããã å½ç¤¾ã®å ç¢ã§å®å¨ãªãµã¼ãã¹ã«ãããå®å¨ã«å¿åã§ãã©ã¤ãã¼ãã®é»å­ã¡ã¼ã«ã¢ãã¬ã¹ãåå¾ã§ãã¾ã
 ããã«ä½¿ç¨ã§ãã¾ãã
 å®éã«ã¯ãèªåã®ä¸æçãªã¡ã¼ã«ã¢ãã¬ã¹ãä½æãããã¤ã§ããããä½¿ç¨ãããã¨ãã§ãã¾ãã ãã¹ã¯ã¼ã ãã¹ã¯ã¼ããªã»ãã æ¬¡ã®ãã¼ã¸ã«ç§»åãã¦ãæ°ãããã¹ã¯ã¼ããé¸æãã¦ãã ããã äººæ°è¨äº åäººæå ±ä¿è­·æ¹é å½ã®åä¿¡ãã¬ã¤ã§ãã¹ãããããã­ã¸ã§ã¯ã çã®ã³ã³ãã³ã ã¢ã¯ãã£ãã¼ã·ã§ã³ã¡ã¼ã«ãåéä¿¡ãã ãã®ãã¼ã¸ã®åä¿¡ãã¬ã¤ã»ã¯ã·ã§ã³åã®åä¿¡ã¡ã¼ã«ãèª­ã ãªãã¬ãã·ã¥ ç»é²å®äº ãã¹ã¯ã¼ãã®ãªã»ãã ãã¹ã¯ã¼ãã®ãªã»ãããªã³ã¯ãã¡ã¼ã«ã«éä¿¡ããã¾ãã ã¡ã¼ã«ã¢ãã¬ã¹ãä½æããå¿è¦ããªããã¨ã§ãæéãç¯ç´ã§ãã¾ãã æºå¸¯é»è©±ã§QRã³ã¼ããã¹ã­ã£ã³ ãã£ã¼ãããã¯ãéä¿¡ éä¿¡è å±æåä¿¡ãã¬ã¤ ãæ°ã«å¥ãã®ãããã¯ã¼ã¯ãã³ãã¥ããã£ã«ç§ãã¡ãæ¨è¦ãã¦ãç§ãã¡ã«æãç¤ºãã¦ãã ããï¼ ãµã¤ã³ã¢ãã ãµã¤ã³ã¢ãã ç¡æã§ç³ãè¾¼ãï¼ ãµã¤ã³ã¢ãã ã½ã¼ã·ã£ã«ãããã¯ã¼ã¯ã®ã­ã°ã¤ã³å¤±æ ã¹ãã ã¡ã¼ã«ãæ¤åºããã¾ãã ã¹ãã ç¡æ ä»ããéå§ ã´ãã¡ã¼ã«ã®åä¿¡ãåæ­¢ããåä¿¡ãã¬ã¤ã®ããã£ã«ã¿ããã¿ã³ã¨ãè³¼èª­è§£é¤ããã¿ã³ãå¿ãã ä»¶å ç§ãã¡ã«ã¤ãã¦æãã¦ ä¸æã¡ã¼ã« ä¸æã¡ã¼ã«ãµã¼ãã¹ å©ç¨è¦ç´ å©ç¨è¦ç´ ãµã¤ã³ã¢ãããã¦ããã¦ãããã¨ãï¼ å½ãµã¤ãããå©ç¨ããã ããããã¨ããããã¾ãï¼ %(site_name)s ãã¼ã  ã¢ã¯ãã£ãåãããã¨ããã¢ã«ã¦ã³ãã¯ç¡å¹ã§ãã ã¢ã¯ãã£ãåãããã¨ããã¢ã«ã¦ã³ãã¯ãã§ã«ã¢ã¯ãã£ãåããã¦ãã¾ãã æå®ããã¢ã¯ãã£ãã¼ã·ã§ã³ã­ã¼ãç¡å¹ã§ãã å¥åããã¡ã¼ã«ã¢ãã¬ã¹ã«ééããå¥åãã¹ãããã¾ããã(ç§ãã¡ã®æé«ã®äººã«èµ·ããã) ã¡ã¼ã«ãè¿·æã¡ã¼ã«ãã©ã«ãã«ããã¾ããï¼ãã¾ã«ç©äºãè¿·ã£ã¦ãã¾ããã¨ãããï¼ ãã®ã¢ã«ã¦ã³ãã¯æéåãã§ãã ãã®å¾åã¯ããªã³ã¯ãä½¿ç¨ãããã¨ã«ãããæªæã®ããã­ã£ã³ãã¼ã³ã®ãªãã¬ã¼ã¿ã¯ãé»å­ã¡ã¼ã«ã«ãã¡ã¤ã«ãæ·»ä»ããå¤å¸çãªæè¡ã¨æ¯è¼ãã¦ãããå¤ãã®ã¯ãªãã¯ã¨ææãåä¿¡ãããã¨ãè¨¼æãã¦ãã¾ããæããã«ãã¦ã¼ã¶ã¼ã¯ã¡ã¼ã«åã®æ·»ä»ãã¡ã¤ã«ã«ä¸ä¿¡æãæ±ãã¦ãã¾ãã ãããä½¿ã£ã¦ã¦ã§ããµã¤ããã½ã¼ã·ã£ã«ã¡ãã£ã¢ãªã©ã«ãµã¤ã³ã¢ãããã¾ããã å½¹ã«ç«ã¤ãªã³ã¯é [è¡¨ç¤º] åä¿¡ã¡ã¼ã«ãå¾ã£ã¦ãã¾ã ç§ãã¡ã¯ãã¤ã³ã¿ã¼ãããä¸ã®å®å¨ãç¶­æãããã¨ã«éç¹ãç½®ãã¦ãããã¨ãç®æãã¦ãã¾ãã æ¬å½ã«æè¬ãã¦ãã¾ãããããã¨ãï¼ ä½¿ãæ¨ã¦ã®ä¸æã¡ã¼ã«ã£ã¦ä½ã§ãã ããã¯èª°ã§ãããããã¦ç§ãã¡ã¯ã©ãå½¹ç«ã¤ã®ã§ããï¼ ãªãä¸æã¡ã¼ã«ãå¿è¦ãªã®ã§ããï¼ ç§ãã¡ã®ãµã¼ãã¹ããå§ããã¾ããï¼ ç¾æç¹ã§ã¯ãã¼ã¯ã³ãããã¾ããã åäººã®ã¡ã¼ã«ã¢ãã¬ã¹ãå±æããããªãã§ããï¼ããªãã¯ç²ãã¦ãã¾ãã
 ã¹ãã ï¼ åäººã®ã¡ã¼ã«ã¢ãã¬ã¹ãå±æããããªãã§ããï¼ã¹ãã ã«ç²ãã¦ãï¼ ã¦ã¼ã¶ã¼ã¢ã«ã¦ã³ãã®ãã¹ã¯ã¼ããªã»ããããªã¯ã¨ã¹ãããããããã®ã¡ã¼ã«ãå±ãã¾ãã
 %(site_name)sã ããªãã® ç¡æã®ä¸æã¡ã¼ã« åéãååã¯ãåä¿¡ã¡ã¼ã«ãç¬æã«è¡¨ç¤ºã§ãã¾ãã åä¿¡ãã¬ã¤ãç©ºã§ã ããªãã®ã¦ã¼ã¶ã¼åãããªããå¿ãã¦ãã¾ã£ãå ´åï¼ åã« ä½ååãã®åä¿¡ãã¬ã¤ ã³ã¼ã ããããã¹ã¦ã®åé¡ãè§£æ±ºããããã«è¨­è¨ããã¦ãã¾ãã å°éå®¶ã¯ã2020å¹´ã®ç¬¬2ååæï¼4æã5æã6æï¼ã«éä¿¡ããããã¹ã¦ã®ã¡ã¼ã«ã¹ãã ã® 87%% ã«ãæªæã®ãããã¡ã¤ã«ããã¦ã³ã­ã¼ãããããã®ãªã³ã¯ãå«ã¾ãã¦ãã¾ããããã¡ã¤ã«èªä½ã®æ·»ä»ãã¡ã¤ã«ã¯å«ã¾ãã¦ããªãã¨è¨ç®ãã¾ããã ç¡æã®ã¦ã§ããµã¤ãç£è¦ ç­ãæéã®ããã«çºè¡ãããã¢ãã¬ã¹ã§ãï¼æ°åãã1ã¶æéããã®å¾ãããã¯ãã¹ã¦ã®çä¿¡æç´ã¨æ·»ä»ãã¡ã¤ã«ã§åé¤ãããããã¾ãã¯åã«ããä»¥ä¸ã«ä½¿ç¨ãããã¨ã¯ã§ãã¾ããã ãã©ãããã©ã¼ã ã¯ãã¹ãã ãåºåã¡ã¼ã«ãããã­ã³ã°ãæ»æã­ããããå¿ããã®ãå©ããããã«ç¹å¥ã«è¨­è¨ããã¦ãã¾ããããªãã®æ¬å½ã®ã¡ã¼ã«ããã¯ã¹ãæ¸æ½ã§å®å¨ã«ä¿ã¡ã¾ãã ã¯ãä¸æçãå®å¨ãå¿åãç¡æãä½¿ãæ¨ã¦ã®é»å­ã¡ã¼ã«ã¢ãã¬ã¹ãæä¾ãã¾ãã ãµã¤ã³ã¢ãã 