��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  B  R;  �   �<  z   =  �   �=  �   +>  >  �>  �  @  �   �A  �   �B  �   _C  �   �C  �   pD  m   E  �   �E  �   OF  �   �F  �   �G    �H  `  �I  \  N  K   ^R  �   �R     WS     dS     mS     {S     �S     �S  ,   �S     �S  [   �S  ,   IT  0   vT     �T     �T     �T     �T     �T     �T     �T     U     U  	    U  /   *U     ZU     _U     lU     rU     ~U     �U  
   �U     �U     �U     �U     �U     �U     �U     V     *V     <V     QV     gV     pV     tV     }V     �V     �V     �V     �V  0   �V  (   �V     W     +W     .W  1   NW     �W  #   �W  )   �W  :   �W     (X  7   DX     |X     �X  
   �X  
   �X     �X     �X     �X     �X     �X  	   Y     Y     ,Y  A   BY     �Y  �   �Y  �   Z     j[     s[  8   �[     �[     �[  "   �[     \  !   \  @   0\  
   q\     |\     �\  P   �\  5   �\  $   3]     X]     k]     t]  W   �]     �]     �]     �]  
   ^     ^     3^     M^  
   Y^  i   d^  	   �^     �^     �^     �^     _     (_     <_  '   X_     �_  6   �_  =   �_  8   `  a   E`  E   �`     �`  =  a  @   Db     �b     �b     �b  L   �b  &   c  .   +c  (   Zc  $   �c     �c  "   �c  G   �c  F   3d  �   zd     �d     e  A   e     _e  6   pe     �e     �e     �e  ,   �e  �   �e      �f  �   �f  �   �g  >   Zh     �h     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                            100%% Privé-postvakken met volledige eigendom!
                                         
                                            Genereer valse e-mail met één klik
                                         
                                            Verberg jezelf voor spam met verbeterde privacy en beveiliging
                                         
                                        Klik op „Verwijderen” op de startpagina en je krijgt nieuwe tijdelijke e-mail.
                                     
                                        Het e-mailadres is geldig totdat je het verwijdert of totdat de service het verwijdert.
 De levensduur hangt van veel verschillende factoren af.
 U kunt tijdelijke e-mails ontvangen met een levensduur tot 1 JAAR met geregistreerde
 account.
                                     
                                        Veel apps en websites verplichten je om zonder reden een persoonlijk e-mailadres op te geven
 anders dan om persoonlijke informatie over jou te verzamelen.
 Tijdelijk e-mailadres kunt u uw persoonlijke e-mailadres scheiden
 je gebruikt voor bankieren, creditcardaankopen en andere persoonlijke identificatie
 transacties van alle andere sites die er zijn.
                                     
                                        Het verzenden van e-mail is volledig uitgeschakeld en
 het zal nooit worden geïmplementeerd vanwege fraude- en spamproblemen.
                                     
                                        Ze worden weergegeven onder je wegwerpmail.
 Als u niet te lang inkomende e-mails hebt ontvangen, klikt u op de knop „Vernieuwen”.
                                     
                                        Je kunt het delen door de QR-code op de hoofdpagina te gebruiken.
                                     
                                    Je kunt maximaal %(max_tokens_allowed)s tokens hebben.
                                     
                                We kunnen de e-mail niet naar dit adres sturen. (Meestal vanwege bedrijfsfirewalls
 of filteren.)
                                 
                                Waarom heb ik een %(BRAND_NAME)s account nodig?
                             
                                Je hebt ons per ongeluk een ander e-mailadres gegeven. (Meestal een werk- of persoonlijk exemplaar
 in plaats van degene die je bedoelde.)
                                 
                            Als je binnen een paar minuten geen e-mail van ons ziet, een paar
 er hadden dingen kunnen gebeuren:
                             
                            We hebben je per e-mail instructies gestuurd voor het instellen van je wachtwoord, als je een account hebt
 bestaat met de e-mail die je hebt ingevoerd. Jij
 zou ze binnenkort moeten ontvangen.
                         
                        Als je geen e-mail ontvangt, zorg er dan voor dat je het adres dat je hebt ingevoerd
 registreerde
 met, en controleer je
 spam-map.
                         
 UpTimeMetrics platform is ontworpen om de status van de websites te controleren. Het omvat dergelijke
 functies: beschikbaarheid van de website,
 Beschikbaarheid van de server
 Geldigheidscontrole van het SSL-certificaat van de website en HTML-validatie.
  
                    %(BRAND_NAME)s is een wegwerp-e-mailadresservice. Wanneer je %(BRAND_NAME)s een nieuwe e-mail bezoekt
 adres wordt speciaal voor jou gegenereerd. Het gegenereerde e-mailadres kan onmiddellijk e-mails ontvangen.
 Elke e-mail die wordt ontvangen, wordt onmiddellijk op de hoofdpagina weergegeven. Niemand anders dan jij zal het zien
 de e-mails
 die worden ontvangen. Het is belangrijk op te merken dat een e-mailadres na 24 uur vervalt. Wanneer een
 e-mail
 adres is verlopen, het e-mailadres en eventuele ontvangen e-mails zijn verdwenen. Een nieuw e-mailadres
 wil
 worden gegenereerd bij het bezoeken van deze website. Er zijn veel redenen om een wegwerpmail te gebruiken.
 Misschien wil je iemand een e-mailadres geven zonder je identiteit te onthullen. Of misschien wil je
 meld u aan voor een website of webservice, maar u bent bang dat de website u spam zal sturen
 de toekomst. <br/>
 Voor het gebruik van deze service MOET u cookie en javascript inschakelen, cookie is alleen om uw sessie op te nemen
 id
 en taalvoorkeur, uw privacy valt onder ons privacy- en cookiebeleid.
                 
                    %(BRAND_NAME)s is een wegwerpservice voor e-mailadressen. Wanneer je %(BRAND_NAME)s een nieuwe e-mail bezoekt
 adres wordt speciaal voor jou gegenereerd. Het gegenereerde e-mailadres kan onmiddellijk e-mails ontvangen.
 Elke e-mail die wordt ontvangen, wordt onmiddellijk op de hoofdpagina weergegeven. Niemand anders dan jij ziet de e-mails
 die worden ontvangen. Het is belangrijk om op te merken dat een e-mailadres na 24 uur verloopt. Wanneer een e-mail
 adres is verlopen, het e-mailadres en de ontvangen e-mails zijn verdwenen. Een nieuw e-mailadres
 worden gegenereerd bij het bezoeken van deze website. Er zijn veel redenen om een wegwerpmail te gebruiken.
 Misschien wil je iemand een e-mailadres geven zonder je identiteit te onthullen. Of misschien wil je
 meld je aan voor een website of webservice, maar je bent bang dat de website je in de toekomst spam zal sturen. <br />
 Voor het gebruik van deze service MOET je cookie en javascript inschakelen, cookie is alleen om je sessie-id op te nemen
 en taalvoorkeur, uw Privacy valt onder ons privacy- en cookiebeleid.
                 
                Klik op deze link om je account te activeren:
             
            Bedankt dat je je aanmeldt!
 Je account is aangemaakt, je kunt inloggen nadat je je account hebt geactiveerd door op de url te drukken
 hieronder.
             API-sleutels Over Ons Toegangstoken Adres Alle e-mails Alle rechten voorbehouden. Alles wat je moet weten over tijdelijke post Heb je al een account? Er is een fout opgetreden tijdens een poging om in te loggen via uw sociale netwerkaccount. Weet je zeker dat je e-mail wilt verwijderen Weet je zeker dat je dit token wilt verwijderen? Terug naar lijst Wijzigen Check je email. Sluit Bevestig wachtwoord Contacteer ons Cookiebeleid Cookiebeleid Gekopieerd! Kopiëren E-mailadres kopiëren vanuit de bovenste invoer Maak E-mail maken Datum Verwijderen E-mail verwijderen Token verwijderen Demo Inbox Heb je geen e-mail gekregen? Wegwerp e-mail Domein Heb je geen account? E-mail E-mailactivering voltooid E-mail bestaat al. Ontvangen e-mails E-mailadres invoeren Pagina Inschrijvingen Verloopt FAQ feedback Wachtwoord vergeten? Veelgestelde vragen Van GAAN Genereer Ontvang vandaag nog je gratis tijdelijke e-mail! Ontvang vandaag nog je tijdelijke e-mail Mijn e-mailadres ophalen Ga Hallo daar, ik zou graag willen Hoe verleng ik de levensduur van het e-mailadres? Hoe deel ik tijdelijke e-mail? Hoe makkelijk was dit te gebruiken? Hoe kan ik ontvangen e-mails controleren? Hoe verwijder ik wegwerpbare e-mail en krijg je de nieuwe? Hoe verstuur ik een e-mail? Hoe gebruik je tijdelijke e-mail voor eenmalig gebruik? Hoe gebruik je het? Ik accepteer de Postvak IN Postvakken Laten we praten Je tijdelijke e-mailadres laden Inloggen Login Inloggen op je account Uitloggen Meer oude inzendingen Recentere inzendingen Nu kun je inloggen op je account en gebruik maken van de service. Onze Contacten Onze robuuste en veilige service stelt u in staat om volledig anoniem en privé te worden
 e-mailadres dat je
 kan onmiddellijk worden gebruikt.
 Eigenlijk kunt u uw eigen tijdelijke e-mailadres aanmaken en gebruiken wanneer u
 willen. Onze robuuste en veilige service stelt u in staat om volledig anoniem en privé een e-mailadres te krijgen dat u
 kan direct gebruiken.
 Eigenlijk kun je je eigen tijdelijke e-mailadres aanmaken en deze gebruiken wanneer je maar wilt. Paswoord Reset wachtwoord Ga naar de volgende pagina en kies een nieuw wachtwoord: Populaire artikelen Privacybeleid Projecten getest met valse inboxen Ruwe inhoud Activeringsmail opnieuw verzenden Binnenkomende e-mails op deze pagina lezen in het inbox gedeelte Vernieuwen Registratie voltooid Paswoord opnieuw instellen De link voor het opnieuw instellen van wachtwoord wordt verzonden via je e-mail. Bespaar tijd door nooit een e-mailadres aan te maken. QR-code scannen met mobiele telefoon Feedback verzenden Afzender Gedeeld Postvak IN Laat ons wat liefde zien door ons aan te bevelen bij je favoriete netwerk of community! Meld je aan Meld je aan Meld je gratis aan! Registreer Social Network Login mislukt Spam e-mails gedetecteerd Spam gratis Nu starten Stop met het ontvangen van prullenbak e-mails en vergeet de inbox „filters” en „afmelden” knoppen Onderwerp Vertel over ons Tijdelijke e-mail Tijdelijke e-mailservice Algemene voorwaarden Gebruiksvoorwaarden Bedankt dat je je aanmeldt! Bedankt voor het gebruik van onze site! Het %(site_name)s team Het account dat je probeerde te activeren is ongeldig. Het account dat je probeerde te activeren, is al geactiveerd. De activeringssleutel die je hebt opgegeven is ongeldig. Het e-mailadres dat je hebt ingevoerd had een fout of typefout. (Toevallig met de beste van ons.) De e-mail bevindt zich in je spammap. (Soms gaan er dingen verloren.) Dit account is verlopen. Deze trend bewijst dat exploitanten van kwaadaardige campagnes door het gebruik van links meer klikken en infecties krijgen in vergelijking met de klassieke techniek van het koppelen van bestanden aan e-mails. Het is duidelijk dat gebruikers wantrouwend zijn geworden ten opzichte van eventuele bijlagen in de e-mail. Gebruik dit om je aan te melden op websites, sociale media, enz. Handige links Bekijk Wachten op inkomende e-mails We streven ernaar je gericht te zijn om je veilig te houden op het internet. We stellen het echt op prijs, bedankt! Wat is tijdelijke e-mail voor eenmalig gebruik Wie ben jij, en hoe kunnen we je helpen? Waarom heb ik tijdelijke post nodig? Zou je onze service aanbevelen? Je hebt op dit moment geen tokens. Wil je je persoonlijke e-mailadres niet delen? Ben je moe van de
 spam? Wil je je persoonlijke e-mailadres niet delen? Ben je moe van de spam? U ontvangt deze e-mail omdat u hebt gevraagd om een wachtwoord opnieuw in te stellen voor uw gebruikersaccount op
 %(site_name)s. Jouw Je gratis tijdelijke e-mail Je vrienden en collega's kunnen direct inkomende e-mail bekijken. Uw inbox is leeg Uw gebruikersnaam, voor het geval u het vergeten bent: geleden miljarden inboxen code ontworpen om al deze problemen op te lossen. experts berekenden dat 87%% van alle e-mailspam verzonden in het tweede kwartaal van 2020 (april, mei en juni maanden) links bevatte om schadelijke bestanden te downloaden, maar geen bijlagen met de bestanden zelf. gratis monitoring van de website is een adres dat voor een korte tijd wordt uitgegeven: van enkele minuten tot een hele maand, en dan wordt het verwijderd met alle binnenkomende brieven en bijlagen, of gewoon niet beschikbaar voor verder gebruik. -platform is speciaal ontworpen om u te helpen spam, reclamemailings, hacking en aanvallende robots te vergeten. Houd uw echte mailbox schoon en veilig. biedt tijdelijk, veilig, anoniem, gratis, wegwerp e-mailadres. inschrijven 