��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;    R;  �   j<  x   �<  �   f=  �   �=  &  �>  �  �?  �   ]A  �   B  �   �B  w   cC  �   �C  g   �D  �   �D  �   �E  �   HF  �   G  �   �G  T  �H  n  M  Q   tQ  �   �Q     {R     �R     �R     �R     �R     �R  *   �R      S  R   S  (   mS  0   �S     �S     �S     �S     �S     �S     	T     T  &   %T     LT     UT  ,   \T     �T     �T     �T     �T     �T     �T     �T     �T     �T     �T      U     U     U     <U     TU     lU     �U     �U     �U     �U     �U     �U     �U     �U     �U  (   �U     V     .V     DV     HV  3   `V  %   �V     �V  !   �V  -   �V     (W  )   >W     hW  
   {W     �W  	   �W     �W  )   �W     �W     �W     �W     �W     X     X  C   6X     zX  �   �X  �   \Y     4Z     <Z  /   QZ     �Z     �Z  &   �Z     �Z  #   �Z  7   [     C[     L[     c[  7   x[  7   �[     �[     \     \     $\  X   1\     �\     �\     �\  
   �\     �\     �\      ]  	   ]  Y   ]     p]     u]     �]     �]     �]     �]     �]  '   �]     "^  -   :^  5   h^  *   �^  Y   �^  N   #_     r_    �_  D   �`     �`     �`     �`  K   a     Ta  !   ra  %   �a  %   �a     �a  #   �a  T   #b  D   xb  l   �b     *c     .c  R   Lc     �c  +   �c     �c     �c     �c  +   d  �   /d      �d  �   e  �   �e  B   xf  
   �f     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
                                            100%% Private postkasser med fullt eierskap!
                                         
                                            Generer falsk e-post med ett klikk
                                         
                                            Skjul deg for søppelpost med forbedret personvern og sikkerhet
                                         
                                        Klikk på «Slett» på hjemmesiden, og du vil få ny midlertidig e-post.
                                     
                                        E-postadressen er gyldig til du sletter den eller til tjenesten sletter den.
 Levetiden avhenger av mange forskjellige faktorer.
 Du kan ha midlertidige e-poster med levetid på opptil 1 ÅR med registrert
 -kontoen.
                                     
                                        Mange apper og nettsteder forplikter deg til å oppgi personlig e-postadresse uten grunn
 annet enn å samle inn personlig informasjon om deg.
 Midlertidig e-postadresse lar deg skille din personlige e-postadresse
 du bruker til bank, kredittkortkjøp og annen personlig identifisering
 transaksjoner fra alle de andre nettstedene der ute.
                                     
                                        Sende e-post er fullstendig deaktivert og
 det vil aldri bli implementert på grunn av svindel og spam problemer.
                                     
                                        De vises under disponibel e-post.
 Hvis du ikke fikk innkommende e-post for lenge - klikk på «Oppdater» -knappen.
                                     
                                        Du kan dele den ved å bruke QR-kode på hovedsiden.
                                     
                                    Du kan ha maks %(max_tokens_allowed)s tokens.
                                     
                                Vi kan ikke levere e-posten til denne adressen. (Vanligvis på grunn av bedriftens brannmurer
 eller filtrering.)
                                 
                                Hvorfor trenger jeg %(BRAND_NAME)s konto?
                             
                                Du ga oss en annen e-postadresse ved et uhell. (Vanligvis et arbeid eller personlig en
 i stedet for den du mente.)
                                 
                            Hvis du ikke ser en e-post fra oss i løpet av få minutter, kan noen få
 ting kunne ha skjedd:
                             
                            Vi har sendt deg instruksjoner om hvordan du angir passordet ditt, hvis en konto
 finnes med e-posten du skrev inn. Du
 skal motta dem om kort tid.
                         
                        Hvis du ikke mottar en e-post, må du sørge for at du har skrevet inn adressen du
 registrert
 med, og sjekk
 spam mappe.
                         
 UptiMETRICS-plattformen er designet for å overvåke statusen til nettstedene. Det inkluderer slike
 funksjoner: Nettstedets tilgjengelighet,
 Server-tilgjengelighet,
 Gyldighetskontroll av nettstedets SSL-sertifikat og HTML-validering.
  
                    %(BRAND_NAME)s er en engangs e-postadressetjeneste. Når du besøker %(BRAND_NAME)s en ny e-post
 adressen genereres bare for deg. Den genererte e-postadressen kan umiddelbart motta e-postmeldinger.
 Eventuelle e-postmeldinger som mottas, vises umiddelbart på hovedsiden. Ingen andre enn du vil se
 e-postene
 som er mottatt. Det er viktig å merke seg at en e-postadresse utløper etter 24 timer. Når en
 e-post
 adressen er utløpt, e-postadressen og eventuelle mottatte e-poster vil være borte. En ny e-postadresse
 vil
 genereres ved å besøke dette nettstedet. Det er mange grunner til å bruke en engangs e-post.
 Det kan være lurt å gi noen en e-postadresse uten å avsløre identiteten din. Eller du kan være lurt å
 registrer deg for et nettsted eller en webtjeneste, men du er bekymret for at nettstedet vil sende deg spam i
 fremtiden. <br/>
 For å bruke denne tjenesten må du aktivere informasjonskapsel og javascript, cookie er bare for å registrere økten
 id
 og språkpreferanser, ditt personvern dekkes under vår Personvern og Cookie policy.
                 
                    %(BRAND_NAME)s er en disponibel e-postadressetjeneste. Når du besøker %(BRAND_NAME)s en ny e-post
 -adressen genereres bare for deg. Den genererte e-postadressen kan umiddelbart motta e-poster.
 Alle e-postmeldinger som mottas, vises umiddelbart på hovedsiden. Ingen andre enn du vil se e-postene
 som er mottatt. Det er viktig å merke seg at en e-postadresse utløper etter 24 timer. Når en e-post
 -adressen er utløpt, vil e-postadressen og eventuelle mottatte e-poster være borte. En ny e-postadress kommer
 genereres ved å besøke dette nettstedet. Det er mange grunner til å bruke en engangs e-post.
 Det kan være lurt å gi noen en e-postadresse uten å avsløre identiteten din. Eller du vil kanskje
 registrere deg for et nettsted eller en nettjeneste, men du er bekymret for at nettstedet vil sende deg spam i fremtiden. <br />
 For å bruke denne tjenesten må du aktivere informasjonskapsel og javascript, informasjonskapsel er bare for å registrere økt-ID
 og språkpreferanser, dekkes personvernet ditt i henhold til retningslinjene for personvern og informasjonskapsler.
                 
                Klikk på denne lenken for å aktivere kontoen din:
             
            Takk for at du registrerte deg!
 Kontoen din er opprettet, du kan logge inn etter at du har aktivert kontoen din ved å trykke på nettadressen
 nedenfor.
             API-nøkler Om oss Tilgangstoken Adresse Alle e-postmeldinger Alle rettigheter reservert. Alt du trenger å vite om midlertidig post Har du allerede en konto? Det oppstod en feil under forsøk på å logge inn via din sosiale nettverkskonto. Er du sikker på at du vil slette e-post Er du sikker på at du vil slette dette tokenet. Tilbake til listen Endre Sjekk e-posten din. Lukk Bekreft passord Kontakt oss Cookie politikk Retningslinjer for informasjonskapsler Kopiert! Kopier Kopier e-mail-adresse fra den øverste input Opprette Opprett e-post Dato Slett Slett e-post Slett token Demo Innboks Fikk du ikke en e-post? Engangs e-post Domene Har du ikke en konto? E-post Aktivering av e-post fullført E-post finnes allerede. E-postmeldinger mottatt Skriv inn e-postadresse Oppføring-siden Utløper FAQ tilbakemelding Glemt passord? Ofte stilte spørsmål Fra GÅ Generere Få din gratis midlertidig e-post i dag! Få midlertidig e-post i dag Få min e-postadresse Gå Hei der, jeg ønsker å Hvordan forlenger jeg levetiden til e-postadressen? Hvordan deler jeg midlertidig e-post? Hvor lett var dette å bruke? Hvordan sjekke mottatte e-poster? Hvordan slette engangs e-post og få den nye? Hvordan sende e-post? Hvordan bruke engangs midlertidig e-post? Slik bruker du det Jeg godtar Innboks Innbokser La oss snakke Laster inn din midlertidige e-postadresse Logg inn Logg inn Logg inn på kontoen din Logg ut Flere gamle oppføringer Flere nylige oppføringer Nå kan du logge inn på kontoen din og begynne å bruke tjenesten. Våre Kontakter Vår robuste og sikre service gjør at du kan få helt anonym og privat
 e-postadresse som du
 kan bruke umiddelbart.
 Egentlig kan du opprette din egen midlertidig e-postadresse og bruke den når du
 ønsker. Vår robuste og sikre tjeneste gir deg mulighet til å få helt anonym og privat en e-postadresse som du
 kan bruke umiddelbart.
 Faktisk kan du opprette din egen midlertidige e-postadresse og bruke den når du vil. Passord Tilbakestill passord Gå til følgende side og velg et nytt passord: Populære artikler Retningslinjer for personvern Prosjekter testet med falske innbokser Rått innhold Send e-post til aktivering på nytt Les innkommende e-poster på denne siden i innboksdelen Oppdater Registrering fullført Tilbakestill passord Tilbakestill passord link vil bli sendt på din e-post. Spar tid ved å aldri måtte opprette en e-postadresse. Skann QR-kode med mobiltelefon Send tilbakemelding Avsender Delt innboks Vis oss litt kjærlighet ved å anbefale oss til ditt favorittnettverk eller fellesskap! Registrer deg Registrer deg Registrer deg gratis! Påmelding Feil ved pålogging på sosiale Nettsøppelpost oppdaget Spam Gratis Start nå Stopp å motta søppelpost og glem innboksen «filtre» og «avslutt abonnement» knapper Emne Fortell om oss Midlertidig e-post Midlertidig e-posttjeneste Vilkår og betingelser Vilkår for bruk Takk for at du registrerte deg! Takk for at du bruker nettstedet vårt! Det %(site_name)s laget Kontoen du forsøkte å aktivere, er ugyldig. Kontoen du prøvde å aktivere, er allerede aktivert. Aktiveringsnøkkelen du oppga, er ugyldig. E-postadressen du skrev inn, hadde en feil eller skrivefeil. (Skjer med de beste av oss.) E-posten er i søppelpostmappen din. Noen ganger går ting seg vill der inne.) Denne kontoen er utløpt. Denne trenden viser at ved å bruke lenker, mottar operatører av ondsinnede kampanjer flere klikk og infeksjoner i forhold til den klassiske teknikken for å legge ved filer til e-post. Åpenbart har brukerne blitt misfornøyde med eventuelle vedlegg i posten. Bruk denne til å registrere deg på nettsteder, sosiale medier osv. Nyttige Lenker Vis Venter på innkommende e-post Vi satser høyt på å være fokusert på å holde deg trygg på internett. Vi setter pris på det, takk! Hva er engangs midlertidig e-post Hvem er du, og hvordan kan vi hjelpe? Hvorfor trenger jeg midlertidig post? Vil du anbefale vår tjeneste? Du har ikke tokens for øyeblikket. Du ønsker ikke å dele din personlige e-postadresse? Er du trøtt fra
 søppelpost? Vil du ikke dele din personlige e-postadresse? Er du sliten av spam? Du mottar denne e-posten fordi du ba om tilbakestilling av passord for brukerkontoen din på
 %(site_name)s. Din Din gratis midlertidig e-post Vennene og kollegaene dine kan få øyeblikkelig oversikt over innkommende e-post. Innboksen din er tom Brukernavnet ditt, i tilfelle du har glemt: siden milliarder av innbokser kode designet til at løse alle disse problemer. eksperter beregnet at 87%% av all søppelpost som ble sendt i andre kvartal 2020 (april, mai og juni måneder) inneholdt koblinger for å laste ned ondsinnede filer, men ikke vedlegg med selve filene. gratis overvåking av nettstedet er en adresse som er utstedt i kort tid: fra noen få minutter til en hel måned, og da vil den bli slettet med alle innkommende brev og vedlegg, eller bare utilgjengelig for videre bruk. -plattformen er spesielt utviklet for å hjelpe deg å glemme spam, reklameutsendelser, hacking og angripende roboter. Hold din virkelige postkasse ren og sikker. gir midlertidig, sikker, anonym, gratis, disponibel e-postadresse. påmelding 