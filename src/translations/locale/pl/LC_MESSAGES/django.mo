��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  �  R;  �   '=  �   �=  �   L>  �   �>  8  �?  �  �@  �   lB  �   /C  �   D  �   �D  �   E  h   �E  �   6F  �   �F  �   �G  �   SH  �   I  �  �I  �  �N  K   S  �   _S  
   
T     T     T     *T     0T     MT  2   jT     �T  e   �T  /   U  '   DU     lU     }U     �U     �U     �U     �U     �U     �U     �U     �U  '   V     -V     5V     LV     QV     WV     pV     |V     �V     �V     �V     �V     �V     �V     W     W     9W     PW     _W     fW     jW     qW     �W     �W     �W  
   �W  3   �W  $   �W     X     'X     ,X  1   @X  0   rX     �X  !   �X  0   �X     Y  4   )Y     ^Y  
   uY     �Y     �Y     �Y  &   �Y     �Y  	   �Y     �Y     Z     Z     .Z  M   >Z     �Z  �   �Z  �   �[     r\     y\  8   �\     �\     �\  2   �\     "]  #   5]  W   Y]  	   �]     �]     �]  =   �]  :    ^  0   [^     �^     �^     �^  W   �^     _     &_     7_     R_  ,   c_     �_  
   �_     �_  a   �_     `     !`     0`     B`  	   \`     f`     {`  +   �`     �`  9   �`  A   a  -   Sa  Q   �a  G   �a     b  !  .b  a   Pc     �c     �c  /   �c  P   �c      Kd  (   ld  !   �d  (   �d     �d     �d  Z   e  T   ye  s   �e     Bf  "   Hf  h   kf  #   �f  9   �f     2g     7g     Ug  6   Yg  �   �g     {h  �   �h  �   ki  Q   .j     �j     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 
                                            100%% prywatnych skrzynek pocztowych z pełną własnością!
                                         
                                            Wygeneruj fałszywe wiadomości e-mail jednym kliknięciem
                                         
                                            Ukryj się przed spamem z większą prywatnością i bezpieczeństwem
                                         
                                        Kliknij „Usuń” na stronie głównej, a otrzymasz nowy tymczasowy e-mail.
                                     
                                        Adres e-mail jest ważny do czasu jego usunięcia lub do czasu, gdy usługa go usunie.
 Żywotność zależy od wielu różnych czynników.
 Możesz mieć tymczasowe wiadomości e-mail z dożywotnią do 1 ROK z zarejestrowanym
 konto.
                                     
                                        Wiele aplikacji i witryn internetowych zobowiązuje Cię do podania osobistego adresu e-mail bez powodu
 inne niż zbieranie danych osobowych o Tobie.
 Tymczasowy adres e-mail umożliwia oddzielenie osobistego adresu e-mail
 używasz do bankowości, zakupów kart kredytowych i innych identyfikacji osobistej
 transakcji ze wszystkich innych stron tam.
                                     
                                        Wysyłanie wiadomości e-mail jest całkowicie wyłączone i
 nigdy nie zostanie wdrożony z powodu oszustw i spamu.
                                     
                                        Są one wyświetlane pod jednorazowym e-mailem.
 Jeśli nie dostałeś przychodzących e-maili zbyt długo - kliknij przycisk „Odśwież”.
                                     
                                        Można go udostępnić za pomocą kodu QR na stronie głównej.
                                     
                                    Możesz mieć maksimum żetonów %(max_tokens_allowed)s.
                                     
                                Nie możemy dostarczyć wiadomości e-mail na ten adres. (Zwykle z powodu zapór firmowych
 lub filtrowanie.)
                                 
                                Dlaczego potrzebuję konta %(BRAND_NAME)s?
                             
                                Przypadkowo podałeś nam inny adres e-mail. (Zwykle praca lub osobista
 zamiast tego, który miałeś na myśli.)
                                 
                            Jeśli nie zobaczysz e-maila od nas w ciągu kilku minut, kilka
 rzeczy mogły się zdarzyć:
                             
                            Wydaliśmy Ci e-mail instrukcje dotyczące ustawienia hasła, jeśli konto
 istnieje z wpisaną wiadomością e-mail. Ty
 powinien je otrzymać wkrótce.
                         
                        Jeśli nie otrzymasz e-maila, upewnij się, że podałeś adres, który
 zarezewowano
 za pomocą i sprawdź
 folder spamu.
                         
 Platforma UpTimeMetrics jest przeznaczona do monitorowania stanu stron internetowych. Obejmuje to takie
 funkcje: dostępność strony internetowej,
 dostępność serwera,
 Sprawdzenie ważności certyfikatu SSL witryny i walidacji HTML.
  
                    %(BRAND_NAME)s to jednorazowa usługa adresów e-mail. Kiedy odwiedzasz %(BRAND_NAME)s nową wiadomość e-mail
 jest generowany tylko dla Ciebie. Wygenerowany adres e-mail może natychmiast otrzymywać wszelkie wiadomości e-mail.
 Każda otrzymana wiadomość e-mail zostanie natychmiast wyświetlona na stronie głównej. Nikt inny niż ty nie zobaczy
 wiadomości e-mail
 , które są odbierane. Ważne jest, aby pamiętać, że adres e-mail wygasa po 24 godzinach. Gdy
 e-mail
 adres wygasł, adres e-mail i wszelkie otrzymane e-maile znikną. Nowy adres e-mail
 chęć
 być generowane po odwiedzeniu tej strony internetowej. Istnieje wiele powodów używania jednorazowego e-maila.
 Możesz podać komuś adres e-mail bez ujawniania swojej tożsamości. Lub może chcesz
 zarejestruj się w witrynie internetowej lub serwisie internetowym, ale obawiasz się, że strona wyśle Ci spam
 Przyszłość. <br/>
 Do korzystania z tej usługi należy włączyć cookie i javascript, ciasteczko jest po prostu nagrać swoją sesję
 id
 i preferencji językowych, Twoja Prywatność jest objęta naszą polityką prywatności i plików cookie.
                 
                    %(BRAND_NAME)s jest jednorazową usługą adresów e-mail. Gdy odwiedzasz %(BRAND_NAME)s nową wiadomość e-mail
 adres jest generowany właśnie dla Ciebie. Wygenerowany adres e-mail może natychmiast otrzymywać wszelkie e-maile.
 Każdy otrzymany e-mail natychmiast pojawi się na stronie głównej. Nikt poza tobą nie zobaczy e-maili
 które zostały odebrane. Ważne jest, aby pamiętać, że adres e-mail wygasa po 24 godzinach. Gdy e-mail
 adres wygasł, adres e-mail i wszelkie otrzymane wiadomości e-mail znikną. Nowy adres e-mail
 być generowane po odwiedzeniu tej strony internetowej. Istnieje wiele powodów korzystania z jednorazowego e-maila.
 Możesz podać komuś adres e-mail bez ujawniania Twojej tożsamości. Lub może chcesz
 zapisz się na stronę internetową lub usługę internetową, ale obawiasz się, że strona wyśle Ci spam w przyszłości. <br />
 Do korzystania z tej usługi MUSI włączyć pliki cookie i javascript, cookie jest po prostu nagrać identyfikator sesji
 i preferencji językowych, Twoja Prywatność jest objęta naszą Polityką prywatności i plików cookie.
                 
                Kliknij ten link, aby aktywować swoje konto:
             
            Dzięki za rejestrację!
 Twoje konto zostało utworzone, możesz się zalogować po aktywowaniu konta przez naciśnięcie adresu URL
 poniżej.
             Klucze API O nas Token dostępu Adres Wszystkie wiadomości e-mail Wszelkie prawa zastrzeżone. Wszystko, co musisz wiedzieć o tymczasowej poczty Masz już konto? Wystąpił błąd podczas próby zalogowania się za pośrednictwem konta w sieci społecznościowej. Czy na pewno chcesz usunąć wiadomość e-mail Czy na pewno chcesz usunąć ten token. Powrót do listy Zmień Sprawdź swój adres e-mail. Zamknij Potwierdź hasło Skontaktuj się z nami Polityka Cookie Polityka Cookies Skopiowany! Kopia Kopiuj adres e-mail z górnego wejścia Stwórz Utwórz pocztę e-mail Data Usuń Usuń wiadomość e-mail Usuń token Skrzynka odbiorcza demo Nie dostałeś e-maila? Jednorazowe wiadomości e-mail Domena Nie masz konta? E-mail Aktywacja e-mail zakończona E-mail już istnieje. Otrzymane wiadomości e-mail Wprowadź adres e-mail Strona wpisów Wygasa FAQ Opinie Zapomniałeś hasła? Często zadawane pytania Od IŚĆ Generować Pobierz swój darmowy tymczasowy e-mail już dziś! Pobierz tymczasowy e-mail już dziś Pobierz mój adres e-mail Idź Cześć, chciałbym Jak przedłużyć okres ważności adresu e-mail? Jak udostępnić tymczasową wiadomość e-mail? Jak łatwo było tego użyć? Jak sprawdzić otrzymane e-maile? Jak usunąć jednorazowy e-mail i uzyskać nowy? Jak wysłać e-mail? Jak korzystać z jednorazowego tymczasowego e-maila? Jak z niego korzystać Akceptuję Skrzynka odbiorcza Skrzynki odbiorcze Porozmawiajmy Wczytywanie tymczasowego adresu e-mail Zaloguj się Logowanie Zaloguj się na swoje konto Wyloguj Więcej starych wpisów Najnowsze wpisy Teraz możesz zalogować się na swoje konto i zacząć korzystać z usługi. Nasze kontakty Nasze solidne i bezpieczne usługi pozwalają uzyskać całkowicie anonimowe i prywatne
 adres e-mail, który
 można użyć natychmiast.
 Właściwie można utworzyć własny tymczasowy adres e-mail i używać go za każdym razem
 chcesz. Nasza solidna i bezpieczna usługa pozwala uzyskać całkowicie anonimowy i prywatny adres e-mail, który
 można użyć natychmiast.
 Właściwie można utworzyć własny tymczasowy adres e-mail i używać go, kiedy tylko chcesz. Hasło Resetowanie hasła Przejdź do następującej strony i wybierz nowe hasło: Popularne artykuły Polityka prywatności Projekty testowane z fałszywymi skrzynkami odbior Zawartość surowa Wyślij ponownie e-mail aktywacyjny Przeczytaj przychodzące wiadomości e-mail na tej stronie w sekcji skrzynki odbiorczej Odśwież Rejestracja zakończona Resetuj hasło Link do resetowania hasła zostanie wysłany na Twój e-mail. Oszczędzaj czas, nigdy nie musisz tworzyć adresu e-mail. Zeskanuj kod QR za pomocą telefonu komórkowego Wyślij opinię Nadawca Współdzielona skrzynka Pokaż nam trochę miłości, polecając nas swojej ulubionej sieci lub społeczności! Zarejestruj się Zarejestruj się Zarejestruj się za darmo! Zarejestruj się Błąd logowania do sieci społecznościowej Wykryto spam Bezspamowy Rozpocznij teraz Przestań otrzymywać śmieci i zapomnij o przyciskach „filtrów” i „anuluj subskrypcję” Temat Opowiedz o nas Tymczasowe e-mail Tymczasowe usługi e-mail Regulamin Warunki Użytkowania Dzięki za rejestrację! Dziękujemy za korzystanie z naszej strony! Zespół %(site_name)s Konto, które próbowano aktywować, jest nieprawidłowe. Konto, które próbowałeś aktywować, zostało już aktywowane. Podany klucz aktywacyjny jest nieprawidłowy. Podany adres e-mail miał błąd lub literówkę. (Zdarza się najlepszym z nas.) E-mail znajduje się w folderze spam. (Czasami rzeczy się tam gubią.) To konto wygasło. Tendencja ta dowodzi, że za pomocą linków operatorzy złośliwych kampanii otrzymują więcej kliknięć i infekcji w porównaniu z klasyczną techniką dołączania plików do wiadomości e-mail. Oczywiście użytkownicy stali się nieufni wobec jakichkolwiek załączników w poczcie. Użyj tego, aby zarejestrować się na stronach internetowych, w mediach społecznościowych itp. Przydatne łącza Widok Oczekiwanie na przychodzące wiadomości e-mail Naszym celem jest skupienie się na zapewnieniu Ci bezpieczeństwa w Internecie. Naprawdę to doceniamy, dzięki! Co to jest jednorazowy tymczasowy e-mail Kim jesteś i jak możemy pomóc? Dlaczego potrzebuję tymczasowej poczty? Czy poleciłbyś nasz serwis? W tej chwili nie masz tokenów. Nie chcesz udostępniać swojego osobistego adresu e-mail? Czy jesteś zmęczony z
 Spamu? Nie chcesz udostępniać swojego osobistego adresu e-mail? Jesteś zmęczony spamem? Otrzymasz ten e-mail, ponieważ zażądałeś zresetowania hasła dla konta użytkownika na stronie
 %(site_name)s. Twój Twój bezpłatny tymczasowy e-mail Twoi znajomi i współpracownicy mogą uzyskać natychmiastowy widok przychodzących wiadomości e-mail. Twoja skrzynka odbiorcza jest pusta Twoja nazwa użytkownika, na wypadek, gdybyś zapomniał: temu miliardy skrzynek odbiorczych kod zaprojektowany, aby rozwiązać wszystkie te problemy. eksperci obliczyli, że 87%% wszystkich wiadomości e-mail spamu wysłanych w drugim kwartale 2020 roku (kwiecień, maj i czerwiec miesiące) zawierało linki do pobierania złośliwych plików, ale nie załączniki do samych plików. darmowe monitorowanie strony to adres wydawany przez krótki czas: od kilku minut do całego miesiąca, a następnie zostanie usunięty ze wszystkimi przychodzącymi literami i załącznikami lub po prostu niedostępny do dalszego użytku. została specjalnie zaprojektowana, aby pomóc zapomnieć o spamie, wysyłkach reklamowych, hakerskich i atakujących robotów. Utrzymuj prawdziwą skrzynkę pocztową w czystości i bezpieczne. zapewnia tymczasowy, bezpieczny, anonimowy, bezpłatny, jednorazowy adres e-mail. Zapisz się 