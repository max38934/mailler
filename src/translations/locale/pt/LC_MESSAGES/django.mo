��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  B  R;  �   �<  w   &=  �   �=  �   0>  0  �>  �  @  �   �A  �   �B  �   eC  �   �C  �   {D  m   /E  �   �E  �   \F  �   �F  �   �G  �   qH  y  eI  g  �M  F   GR  �   �R     (S  
   6S     AS  	   QS     [S     lS  8   �S     �S  M   �S  *   $T  -   OT     }T     �T     �T     �T     �T     �T     �T     �T     	U     U  0   U     JU     PU     ]U     bU     jU     yU     �U     �U     �U     �U     �U     �U      �U     V     #V     5V     RV     fV     mV     qV     zV     �V     �V     �V     �V  ,   �V  (   �V     W     +W     .W  =   CW  &   �W     �W  $   �W  2   �W     X  ,   3X     `X     mX     yX     �X     �X  .   �X     �X     �X     �X      Y     Y     Y  G   2Y     zY  �   �Y  �   �Z     �[     �[  @   �[     �[     �[  .   \     :\     J\  G   h\  	   �\     �\     �\  =   �\  >   ]  ,   []     �]  	   �]     �]  R   �]     ^      ^     ,^     G^     S^     q^     �^     �^  t   �^     _     "_     3_     F_     e_     z_     �_     �_     �_  -   �_  0   `  6   9`  m   p`  F   �`     %a    9a  9   Ab     {b     �b     �b  ?   �b  %   �b  (   c  $   8c  '   ]c  "   �c  !   �c  [   �c  Z   &d  �   �d     e     	e  K   )e      ue  1   �e     �e     �e     �e  .   �e  �   $f     g  �   !g  �   �g  S   �h     i     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
                                            100%% de caixas de correio privadas com propriedade total!
                                         
                                            Gere e-mails falsos com um clique
                                         
                                            Esconda-se do spam com privacidade e segurança aprimoradas
                                         
                                        Clique em “Excluir” na página inicial e você receberá um novo e-mail temporário.
                                     
                                        O endereço de e-mail é válido até você excluí-lo ou até que o serviço o exclua.
 A vida útil depende de muitos fatores diferentes.
 Você pode ter e-mails temporários com vida útil de até 1 ANO com registro
 conta.
                                     
                                        Muitos aplicativos e sites obrigam você a fornecer endereço de e-mail pessoal sem motivo
 além de coletar informações pessoais sobre você.
 O endereço de e-mail temporário permite separar seu endereço de e-mail pessoal
 você usa para compras bancárias, com cartão de crédito e outras identificações pessoais
 transações de todos os outros sites lá fora.
                                     
                                        O envio de e-mail está completamente desativado e
 nunca será implementado devido a problemas de fraude e spam.
                                     
                                        Eles são exibidos sob seu e-mail descartável.
 Se você não receber e-mails recebidos por muito tempo - clique no botão “Atualizar”.
                                     
                                        Você pode compartilhá-lo usando o código QR na página principal.
                                     
                                    Você pode ter no máximo %(max_tokens_allowed)s tokens.
                                     
                                Não podemos enviar o e-mail para este endereço. (Geralmente por causa de firewalls corporativos
 ou filtragem.)
                                 
                                Por que eu preciso de uma conta %(BRAND_NAME)s?
                             
                                Você acidentalmente nos deu outro endereço de e-mail. (Geralmente um trabalho ou pessoal
 em vez do que você quis dizer.)
                                 
                            Se você não vir um e-mail nosso em poucos minutos, alguns
 coisas poderiam ter acontecido:
                             
                            Enviamos instruções por e-mail para definir sua senha, se uma conta
 existe com o e-mail que você inseriu. Você
 deve recebê-los em breve.
                         
                        Se você não receber um e-mail, certifique-se de ter inserido o endereço que você
 registrado
 com e verifique seu
 pasta de spam.
                         
 A plataforma UptimeMetrics foi projetada para monitorar o status dos sites. Ele inclui tais
 características: Disponibilidade do site,
 Disponibilidade do servidor,
 Verificação de validade do certificado SSL do site e validação HTML.
  
                    %(BRAND_NAME)s é um serviço de endereço de e-mail descartável. Quando você visita %(BRAND_NAME)s um novo e-mail
 endereço é gerado apenas para você. O endereço de e-mail gerado pode receber imediatamente qualquer e-mail.
 Qualquer e-mail recebido aparecerá na página principal imediatamente. Ninguém além de você verá
 os e-mails
 que são recebidos. É importante observar que um endereço de e-mail expira após 24 horas. Quando um
 e-mail
 o endereço expirou, o endereço de e-mail e todos os e-mails recebidos desaparecerão. Um novo endereço de e-mail
 vai
 ser gerado ao visitar este site. Existem muitos motivos para usar um e-mail descartável.
 Você pode dar a alguém um endereço de e-mail sem revelar sua identidade. Ou você pode querer
 inscreva-se em um site ou serviço da web, mas você está preocupado com o fato de o site enviar spam
 o futuro. <br/>
 Para usar este serviço, você DEVE habilitar o cookie e o javascript, o cookie é apenas para gravar sua sessão
 id
 e preferência de idioma, sua privacidade é coberta por nossa política de privacidade e cookies.
                 
                    %(BRAND_NAME)s é um serviço de endereço de e-mail descartável. Quando você visita %(BRAND_NAME)s um novo e-mail
 o endereço é gerado apenas para você. O endereço de e-mail gerado pode receber imediatamente qualquer e-mail.
 Qualquer e-mail recebido aparecerá na página principal imediatamente. Ninguém além de você verá os e-mails
 que são recebidos. É importante notar que um endereço de e-mail expira após 24 horas. Quando um e-mail
 o endereço expirou, o endereço de e-mail e todos os e-mails recebidos desaparecerão. Um novo endereço de e-mail
 ser gerado ao visitar este site. Há muitos motivos para usar um e-mail descartável.
 Você pode querer dar a alguém um endereço de e-mail sem revelar sua identidade. Ou você pode querer
 inscreva-se em um site ou serviço da Web, mas você está preocupado que o site enviará spam no futuro. <br />
 Para usar este serviço, você DEVE ativar cookie e javascript, cookie é apenas para gravar sua ID de sessão
 e preferência de idioma, sua Privacidade é coberta pela nossa Política de Privacidade e Cookies.
                 
                Clique neste link para ativar sua conta:
             
            Obrigado por se inscrever!
 Sua conta foi criada, você pode fazer login depois de ativar sua conta pressionando o url
 abaixo.
             Chaves de API Sobre nós Token de acesso Endereço Todos os e-mails Todos os direitos reservados. Tudo o que você precisa saber sobre correio temporário Já tem uma conta? Ocorreu um erro ao tentar fazer o login através da sua conta de rede social. Tem certeza de que deseja excluir o e-mail Tem certeza de que deseja excluir esse token. Voltar para a lista Alterar Verifique seu e-mail. Fechar Confirmar senha Entre em contato conosco Política de cookies Política de Cookies Copiado! Copiar Copiar o endereço de e-mail da entrada superior Criar Criar e-mail Data Excluir Excluir e-mail Excluir token Caixa de entrada demo Não recebeu um e-mail? E-mail descartável Domínio Não tem uma conta? E-mail Ativação por e-mail concluída O e-mail já existe. E-mails recebidos Digite o endereço de e-mail Página de entradas Expira FAQ Feedback Esqueceu a senha? Perguntas frequentes A partir de VAI Gerar Receba seu e-mail temporário gratuito hoje! Receba seu e-mail temporário hoje mesmo Obter meu endereço de e-mail Ir Olá, eu gostaria de Como faço para estender a vida útil do endereço de e-mail? Como compartilho e-mails temporários? Quão fácil foi isso de usar? Como verificar os e-mails recebidos? Como excluir e-mails descartáveis e obter o novo? Como enviar e-mail? Como usar o e-mail temporário descartável? Como usá-lo Eu aceito o Caixa de entrada Caixas de entrada Vamos conversar Carregando seu endereço de e-mail temporário Login Login Faça login na sua conta Sair Entradas mais antigas Entradas mais recentes Agora você pode fazer login em sua conta e começar a usar o serviço. Nossos Contatos Nosso serviço robusto e seguro permite que você fique completamente anônimo e privado
 endereço de e-mail que você
 pode usar imediatamente.
 Na verdade, você pode criar seu próprio endereço de e-mail temporário e usá-lo sempre que
 quero. Nosso serviço robusto e seguro permite que você obtenha um endereço de e-mail completamente anônimo e privado que você
 pode usar imediatamente.
 Na verdade, você pode criar seu próprio endereço de e-mail temporário e usá-lo sempre que quiser. Senha Redefinição de senha Por favor, vá para a página seguinte e escolha uma nova senha: Artigos populares Política de Privacidade Projetos testados com caixas de entrada falsas Conteúdo Bruto Reenviar e-mail de ativação Leia e-mails recebidos nesta página dentro da seção Caixa de entrada Atualizar Registro concluído Redefinir senha O link de redefinição de senha será enviado em seu e-mail. Economize seu tempo sem precisar criar um endereço de e-mail. Digitalize o código QR com telefone celular Enviar feedback Remetente Caixa de entrada compartilhada Mostre-nos um pouco de amor recomendando-nos para sua rede ou comunidade favorita! Cadastre-se Cadastre-se Inscreva-se gratuitamente! Cadastre-se Falha de login de rede social Emails de spam detectados Spam grátis Comece agora Pare de receber e-mails de lixo e esquecer os botões “filtros” da caixa de entrada e “cancelar inscrição” Assunto Conte sobre nós E-mail temporário Serviço de e-mail temporário Termos e condições Termos de Uso Obrigado por se inscrever! Obrigado por usar o nosso site! A equipe %(site_name)s A conta que você tentou ativar é inválida. A conta que você tentou ativar já foi ativada. A chave de ativação que você forneceu é inválida. O endereço de e-mail que você inseriu teve um erro ou erro de digitação. (Acontece com o melhor de nós.) O e-mail está na pasta de spam. (Às vezes, as coisas se perdem lá.) Essa conta expirou. Essa tendência prova que, usando links, operadores de campanhas maliciosas recebem mais cliques e infecções em comparação com a técnica clássica de anexar arquivos a e-mails. Obviamente, os usuários se tornaram desconfiados de quaisquer anexos no correio. Use isso para se inscrever em sites, mídias sociais, etc Links úteis Ver Aguardando e-mails recebidos Nosso objetivo é estar focado em mantê-lo seguro na internet. Nós realmente agradecemos, obrigado! O que é e-mail temporário descartável Quem é você e como podemos ajudar? Por que preciso de correio temporário? Você recomendaria nosso serviço? Você não tem tokens no momento. Você não quer compartilhar seu endereço de e-mail pessoal? Você está cansado do
 spam? Você não quer compartilhar seu endereço de e-mail pessoal? Você está cansado do spam? Você está recebendo este e-mail porque você solicitou uma redefinição de senha para sua conta de usuário em
 %(site_name)s. O seu Seu e-mail temporário gratuito Seus amigos e colegas podem ter uma visão instantânea do e-mail recebido. Sua caixa de entrada está vazia Seu nome de usuário, caso você tenha esquecido: atrás bilhões de caixas de entrada código projetado para resolver todos esses problemas. especialistas calcularam que 87%% de todos os e-mails de spam enviados no segundo trimestre de 2020 (meses de abril, maio e junho) continha links para baixar arquivos maliciosos, mas não anexos com os próprios arquivos. monitoramento de site gratuito é um endereço que é emitido por um curto período de tempo: de alguns minutos a um mês inteiro, e então ele será excluído com todas as letras e anexos recebidos, ou simplesmente indisponível para uso posterior. plataforma é projetado especificamente para ajudá-lo a esquecer spam, correspondências publicitárias, pirataria e atacar robôs. Mantenha sua caixa de correio real limpa e segura. fornece endereço de e-mail temporário, seguro, anônimo, gratuito e descartável. inscrever-se 