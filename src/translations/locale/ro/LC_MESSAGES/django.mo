��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  k  R;  �   �<  ~   F=  �   �=  �   d>  I  ?  �  ]@  �   =B  �   C  �   D  ~   �D  �   E  e   �E  �   -F  �   �F  �   �G  �   wH    5I  �  HJ  �  0O  c   �S  �   _T  	   U  
   U     !U     .U     6U     HU  3   dU     �U  b   �U  2   V  6   ?V     vV  	   �V     �V     �V     �V     �V     �V     �V     W     "W  0   *W     [W     dW     rW     wW     W     �W  
   �W     �W     �W     �W     �W     �W     �W     X     -X     ?X     ]X     mX     uX     yX     �X     �X     �X     �X     �X  ,   �X  &   �X     Y     0Y     6Y  8   PY  -   �Y  -   �Y  (   �Y  N   Z     ]Z  ;   vZ     �Z     �Z     �Z     �Z     �Z  (   �Z     [     ![     ([     F[     R[     k[  U   �[     �[  �   �[  �   �\     �]     �]  L   �]     J^     \^  "   {^     �^     �^  D   �^     _     _     4_  4   E_  E   z_  $   �_     �_  	   �_      `  W   `     g`     w`     �`     �`  (   �`     �`     �`     �`  k   a     ta     |a     �a     �a     �a     �a      �a  1   b     7b  <   Lb  B   �b  -   �b  �   �b  J   �c     �c  ,  �c  U   e     fe     te  %   �e  _   �e  !   f  /   (f  "   Xf  &   {f      �f     �f  T   �f  S   8g  v   �g     h     h  W   0h     �h  9   �h  	   �h     �h     �h  1   �h  �   -i  "   $j  �   Gj  �   >k  [   �k     Fl     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 
                                            100%% Cutii poștale private cu drepturi depline!
                                         
                                            Generează e-mail fals cu un singur clic
                                         
                                            Ascunde-te de spam cu confidențialitate și securitate îmbunătățite
                                         
                                        Faceți clic pe „Ștergeți” de pe pagina de pornire și veți primi un nou e-mail temporar.
                                     
                                        Adresa de e-mail este validă până când o ștergeți sau până când serviciul o va șterge.
 Durata de viață depinde de mulți factori diferiți.
 Puteți avea e-mailuri temporare cu o durată de viață de până la 1 an cu înregistrat
 cont.
                                     
                                        Multe aplicații și site-uri web vă obligă să dați adresa personală de e-mail fără niciun motiv
 altele decât pentru a colecta informații personale despre dvs.
 Adresa de e-mail temporară vă permite să vă separați adresa de e-mail personală
 utilizaţi pentru bancar, achiziţii de carduri de credit şi alte personale de identificare
 tranzacţii de la toate celelalte site-uri acolo.
                                     
                                        Trimiterea e-mailului este complet dezactivată și
 nu va fi niciodată pusă în aplicare din cauza problemelor de fraudă și spam.
                                     
                                        Acestea sunt afișate sub adresa dvs. de e-mail de unică folosință.
 Dacă nu ați primit e-mailuri prea mult timp - faceți clic pe butonul „Reîmprospătare”.
                                     
                                        Puteți să-l partajați utilizând codul QR pe pagina principală.
                                     
                                    Puteți avea maxim %(max_tokens_allowed)s token-uri.
                                     
                                Nu putem livra e-mailul la această adresă. (De obicei, din cauza firewall-uri corporative
 sau filtrare.)
                                 
                                De ce am nevoie de %(BRAND_NAME)s cont?
                             
                                Ne-ai dat din greşeală o altă adresă de e-mail. (De obicei, o muncă sau una personală
 în loc de cel pe care l-ai vrut.)
                                 
                            Dacă nu vedeți un e-mail de la noi în câteva minute, câteva
 lucruri s-ar fi putut întâmpla:
                             
                            V-am trimis prin e-mail instrucțiuni pentru setarea parolei, în cazul în care un cont
 există cu e-mailul pe care l-ați introdus. Tu
 ar trebui să le primească în curând.
                         
                        Dacă nu primiți un e-mail, vă rugăm să vă asigurați că ați introdus adresa pe care
 înregistrat
 cu, și verificați
 dosar spam.
                         
 Platforma UpTimeMetrics este concepută pentru a monitoriza starea site-urilor web. Acesta include astfel de
 caracteristici: disponibilitatea site-ului web;
 Disponibilitatea serverului;
 Verificarea validității certificatului SSL al site-ului web și validarea HTML.
  
                    %(BRAND_NAME)s este un serviciu de adrese de e-mail de unică folosință. Când vizitați %(BRAND_NAME)s un e-mail nou
 adresa este generată doar pentru tine. Adresa de e-mail generată poate primi imediat orice e-mailuri.
 Orice e-mail primit va apărea imediat pe pagina principală. Nimeni altul decât tine va vedea
 e-mailurile
 care sunt primite. Este important să rețineți că o adresă de e-mail expiră după 24 de ore. Când un
 emailul
 adresa a expirat, adresa de e-mail și orice e-mailuri primite vor dispărea. O nouă adresă de e-mail
 va
 să fie generate la vizitarea acestui site. Există multe motive pentru utilizarea unui e-mail de unică folosință.
 Se recomandă să oferiți unei persoane o adresă de e-mail fără a vă dezvălui identitatea. Sau poate doriți să
 înregistrați-vă pentru un site web sau un serviciu web, dar sunteți îngrijorat de faptul că site-ul vă va trimite spam în
 viitorul. <br/>
 Pentru utilizarea acestui serviciu TREBUIE să activați cookie-ul și JavaScript, cookie-ul este doar pentru a înregistra sesiunea dvs.
 id
 și preferința lingvistică, Confidențialitatea dvs. este acoperită de politica noastră de confidențialitate și cookie.
                 
                    %(BRAND_NAME)s este un serviciu de adresă de e-mail de unică folosință. Când vizitați %(BRAND_NAME)s un e-mail nou
 este generată doar pentru dvs. Adresa de e-mail generată poate primi imediat orice e-mailuri.
 Orice e-mail primit va apărea imediat pe pagina principală. Nimeni altul decât tine nu va vedea e-mailurile
 care sunt primite. Este important să rețineți că o adresă de e-mail expiră după 24 de ore. Atunci când un e-mail
 a expirat, adresa de e-mail și e-mailurile primite vor dispărea. O nouă adresă de e-mail va
 să fie generate la vizitarea acestui site. Există multe motive pentru utilizarea unui e-mail de unică folosință.
 Poate doriți să oferiți unei persoane o adresă de e-mail fără a vă dezvălui identitatea. Sau poate doriți să
 înscrieți-vă pentru un site web sau un serviciu web, dar sunteți îngrijorat de faptul că site-ul vă va trimite spam în viitor. <br />
 Pentru utilizarea acestui serviciu trebuie să activați cookie și javascript, cookie este doar pentru a înregistra ID-ul sesiunii
 și preferința de limbă, Confidențialitatea dvs. este acoperită de politica noastră de confidențialitate și cookie.
                 
                Vă rugăm să faceți clic pe acest link pentru a vă activa contul:
             
            Vă mulțumim pentru înscriere!
 Contul dvs. a fost creat, vă puteți autentifica după ce ați activat contul apăsând adresa URL
 de mai jos.
             Taste API Despre noi Token Access Adresă Toate e-mailurile Toate drepturile rezervate. Tot ce trebuie să știți despre poșta temporară Ai deja un cont? A apărut o eroare în timp ce încercați să vă conectați prin contul dvs. de rețea socială. Sunteți sigur că doriți să ștergeți e-mailul Sunteți sigur că doriți să ștergeți acest token. Înapoi la listă Schimbare Verifică-ţi adresa de e-mail. Închide Confirmați parola Contactează-ne Politica privind cookie-urile Politica privind cookie-urile Copiat! Copiere Copierea adresei de e-mail de la intrarea de sus Creează Creare e-mail Data Șterge Ștergere e-mail Șterge token Inbox demo Nu ai primit un e-mail? e-mail de unică folosință Domeniu Nu ai un cont? E-mail Activare e-mail finalizată E-mail există deja. E-mailuri primite Introduceți adresa de e-mail Pagina Intrări Expiră FAQ feedback Ai uitat parola? Întrebări frecvente De la MERGE Genera Primaţi-vă gratuit Email Temporar astăzi! Primați-vă e-mailul temporar astăzi Obțineți adresa mea de e-mail Du-te Bună acolo, aș dori să Cum prelungească durata de viață a adresei de e-mail? Cum pot partaja mesajele de e-mail temporare? Cât de ușor a fost acest lucru de utilizat? Cum să verificați e-mailurile primite? Cum să ștergeți e-mailul de unică folosință și să obțineți unul nou? Cum de a trimite e-mail? Cum se utilizează e-mailul temporar de unică folosință? Cum se utilizează Accept Inbox Inboxuri Hai să vorbim Încărcarea adresei de e-mail temporare Autentificare Logare Conectați-vă la contul dvs. Deconectare Mai multe intrări vechi Mai multe intrări recente Acum vă puteți conecta la contul dvs. și puteți începe să utilizați serviciul. Contactele noastre Serviciul nostru robust și sigur vă permite să obțineți complet anonim și privat
 adresa de e-mail pe care o
 se poate utiliza imediat.
 De fapt, vă puteți crea propria adresă de e-mail temporară și să o utilizați ori de câte ori
 Vreau. Serviciul nostru robust și sigur vă permite să obțineți o adresă de e-mail complet anonimă și privată pe care
 pot folosi imediat.
 De fapt, puteți să vă creați propria adresă de e-mail temporară și să o utilizați oricând doriți. Parole Resetare parolă Vă rugăm să accesați pagina următoare și să alegeți o parolă nouă: Articole populare Politica de confidențialitate Proiecte testate cu inboxuri false Conținut brut Re-trimite e-mail de activare Citiți e-mailurile primite pe această pagină în secțiunea Inbox Actualizare Înregistrare completă Resetați parola Linkul de resetare a parolei va fi trimis pe e-mail. Economisiți timp fără a fi nevoie să creați o adresă de e-mail. Scanați codul QR cu telefonul mobil Trimite feedback Expeditor Inbox partajat Arată-ne puţină dragoste recomandându-ne reţelei sau comunităţii tale preferate! Înscrieți-vă Înscrieți-vă Înscrieți-vă gratuit! Înscrieți-vă Eroare de conectare la rețeaua socială E-mailuri de spam Spam gratuit Începeți acum Opriți primirea e-mailurilor de gunoi și uitați de butoanele „filtre” și „dezabonare” din inbox Subiect Spune despre noi E-mail temporar Serviciu de e-mail temporar Termeni și condiții Termeni de utilizare Vă mulțumim pentru înscriere! Vă mulțumim pentru utilizarea site-ului nostru! Echipa %(site_name)s Contul pe care ați încercat să-l activați nu este valid. Contul pe care ați încercat să-l activați a fost deja activat. Cheia de activare furnizată nu este validă. Adresa de e-mail pe care ați introdus-o a avut o greșeală sau o greșeală de scriere. (Se întâmplă celor mai buni dintre noi.) E-mailul se află în folderul de spam. (Uneori lucrurile se pierd acolo.) Contul ăsta a expirat. Această tendință dovedește că, prin utilizarea linkurilor, operatorii de campanii rău intenționate primesc mai multe clicuri și infecții comparativ cu tehnica clasică de atașare a fișierelor la e-mailuri. Evident, utilizatorii au devenit neîncrezători în orice atașamente din poștă. Utilizați acest lucru pentru a vă înregistra pe site-uri web, rețele sociale etc. Linkuri utile Vizualizare Așteptare pentru e-mailurile primite Scopul nostru este de a fi concentrat pe păstrarea dumneavoastră în siguranță pe internet. Apreciem foarte mult, mulțumesc! Ce este e-mailul temporar de unică folosință Cine eşti şi cum te putem ajuta? De ce am nevoie de poștă temporară? Aţi recomanda serviciul nostru? Nu ai jetoane în acest moment. Nu doriți să partajați adresa dvs. de e-mail personală? Esti obosit de la
 spam? Nu doriți să partajați adresa dvs. de e-mail personală? Te-ai săturat de spam? Primiți acest e-mail deoarece ați solicitat o resetare a parolei pentru contul dvs. de utilizator la
 %(site_name)s. Dumneavoastră Emailul dvs. gratuit temporar Prietenii și colegii dvs. pot obține vizualizarea instantanee a e-mailurilor primite. Inboxul dvs. este gol Numele dvs. de utilizator, în cazul în care ați uitat: în urmă miliarde de inboxuri cod concepute pentru a rezolva toate aceste probleme. Experții au calculat că 87%% din totalul e-mailurilor trimise în al doilea trimestru al anului 2020 (lunile aprilie, mai și iunie) conțineau linkuri pentru a descărca fișiere rău intenționate, dar nu și atașări cu fișierele în sine. monitorizare gratuită a site-ului este o adresă care este emisă pentru o perioadă scurtă de timp: de la câteva minute până la o lună întreagă, iar apoi va fi șters cu toate literele și atașamentele primite sau pur și simplu indisponibil pentru utilizare ulterioară. este special concepută pentru a vă ajuta să uitați de spam, mesaje publicitare, hacking și atacarea roboților. Păstrați cutia poștală reală curată și sigură. oferă o adresă de e-mail temporară, sigură, anonimă, gratuită, de unică folosință. Înscrie-te 