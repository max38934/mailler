��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  �  R;  �   =  �   �=  �   �>    n?    q@  �  �B    �E  P  �F  �   �G  �   �H    II  �   \J    �J  �   �K  r  �L    ON  �  jO  �  �P  "	  �Y  ~   �b    ~c     �d  	   �d     �d  
   �d  *   �d  #   �d  L   !e  6   ne  �   �e  ]   /f  P   �f  "   �f     g  >   g     Qg  #   `g     �g  ?   �g  ?   �g     #h     ;h  b   Ph     �h  ;   �h      i     	i  ;   i     Ti  "   ni  :   �i  !   �i  
   �i  -   �i  !   'j  G   Ij  >   �j  8   �j  ;   	k     Ek  *   ek  .   �k     �k     �k  .   �k     %l     *l     3l  q   Ll  T   �l  D   m     Xm      gm  ]   �m  _   �m  G   Fn  <   �n  n   �n  <   :o  l   wo  $   �o     	p     p     .p  !   Fp  T   hp  
   �p  
   �p  )   �p  
   �p  $   q  $   -q  �   Rq     �q    �q  �  �s     �u     �u  ~   v  !   �v  5   �v  W   �v  1   5w  X   gw  p   �w     1x  )   Bx     lx  �   �x  z   y  X   �y     �y     �y  *   z  �   @z  $   �z  $   �z  6   {     T{  8   k{  0   �{     �{     �{  �   |     �|     �|     �|  A   }  $   D}  )   i}  <   �}  G   �}     ~  {   5~  |   �~  L   .  �   {  }    �  J   ��  �  �  �   т     h�     ��  ,   ��  �   ȃ  2   e�  ]   ��  3   ��  ;   *�  =   f�  ?   ��  �   �  �   x�  �   �     �  [   �  �   E�  )   �  E   �     S�  0   g�     ��  V   ��  �  ��  4   w�  v  ��  )  #�  �   M�     �     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 
                                            100%% частных почтовых ящиков с полным правом собственности!
                                         
                                            Создавайте поддельные письма одним щелчком мыши
                                         
                                            Защитите себя от спама с улучшенной конфиденциальностью и безопасностью
                                         
                                        Нажмите «Удалить» на главной странице, и вы получите новое временное сообщение электронной почты.
                                     
                                        Адрес электронной почты действителен до тех пор, пока вы не удалите его или пока служба не удалит его.
 Срок службы зависит от множества различных факторов.
 Вы можете иметь временную электронную почту сроком службы до 1 ГОДА при регистрации
 аккаунт.
                                     
                                        Многие приложения и веб-сайты обязывают вас без причины предоставлять личный адрес электронной почты
 кроме сбора личной информации о вас.
 Временный адрес электронной почты позволяет разделить личный адрес электронной почты
 вы используете для банковских операций, покупок по кредитным картам и других личных идентификационных данных
 транзакции со всех других сайтов.
                                     
                                        Отправка электронной почты полностью отключена и
 он никогда не будет реализован из-за мошенничества и спама.
                                     
                                        Они отображаются под вашим одноразовым электронным письмом.
 Если вы не получали входящие письма слишком долго — нажмите кнопку «Обновить».
                                     
                                        Вы можете поделиться им с помощью QR-кода на главной странице.
                                     
                                    У вас может быть максимум %(max_tokens_allowed)s токенов.
                                     
                                Мы не можем доставить электронное письмо на этот адрес. (Обычно из-за корпоративных брандмауэров
 или фильтрация.)
                                 
                                Зачем мне нужна учетная запись %(BRAND_NAME)s?
                             
                                Вы случайно указали нам другой адрес электронной почты. (Обычно рабочий или личный
 вместо того, что вы имели в виду.)
                                 
                            Если вы не увидите письмо от нас в течение нескольких минут, несколько
 все могло произойти:
                             
                            Мы отправили вам инструкции по настройке пароля, если у вас есть учетная запись
 существует вместе с введенным электронным письмом. Вы
 должны получить их в ближайшее время.
                         
                        Если вы не получили электронное письмо, убедитесь, что вы ввели свой адрес
 зарегистрировал
 с помощью и проверьте
 папка спама.
                         
 Платформа UpTimeMetrics предназначена для мониторинга состояния веб-сайтов. Она включает в себя такие
 особенности: Доступность сайта,
 Доступность сервера,
 Проверка действительности SSL-сертификата веб-сайта и проверка HTML.
  
                    %(BRAND_NAME)s — это служба одноразовых адресов электронной почты. Когда вы посещаете %(BRAND_NAME)s новое письмо
 адрес генерируется специально для вас. Сгенерированный адрес электронной почты может немедленно получать любые электронные письма.
 Любое полученное электронное письмо сразу появится на главной странице. Никто, кроме вас, не увидит
 электронные письма
 которые получены. Важно отметить, что срок действия адреса электронной почты истекает через 24 часа. Когда появляется
 письмо
 срок действия адреса истек, адрес электронной почты и все полученные письма исчезнут. Новый адрес электронной почты
 будет
 генерируется при посещении этого веб-сайта. Существует множество причин для использования одноразовой электронной почты.
 Возможно, вы захотите предоставить кому-то адрес электронной почты, не раскрывая свою личность. Или, возможно, вы захотите
 зарегистрироваться на веб-сайте или веб-сервисе, но вы обеспокоены тем, что веб-сайт будет отправлять вам спам
 будущее. <br/>
 Для использования этой услуги вы ДОЛЖНЫ включить cookie и javascript, cookie предназначен только для записи вашего сеанса
 id
 и языковые предпочтения, ваша конфиденциальность регулируется нашей Политикой конфиденциальности и использования файлов cookie.
                 
                    %(BRAND_NAME)s — это одноразовая служба адресов электронной почты. Когда вы посещаете %(BRAND_NAME)s новое письмо
 адрес генерируется только для вас. Сгенерированный адрес электронной почты может немедленно получать любые электронные письма.
 Любое полученное электронное письмо будет немедленно отображаться на главной странице. Никто, кроме вас, не будет видеть электронные письма
 которые получены. Важно отметить, что срок действия адреса электронной почты истекает через 24 часа. Когда отправляется электронное письмо
 адрес истек, адрес электронной почты и все полученные письма исчезнут. Новый адрес электронной почты будет
 создаваться при посещении этого веб-сайта. Существует множество причин для использования одноразового электронного письма.
 Возможно, вы захотите предоставить кому-либо адрес электронной почты, не раскрывая свою личность. Или вы можете захотеть
 подпишитесь на веб-сайт или веб-сервис, но вы обеспокоены тем, что этот веб-сайт будет отправлять вам спам в будущем. <br />
 Для использования этого сервиса вы ДОЛЖНЫ включить файлы cookie и javascript, куки предназначены только для записи вашего идентификатора сеанса
 и языковых предпочтений, ваша Конфиденциальность регулируется нашей Политикой конфиденциальности и использования файлов cookie.
                 
                Нажмите эту ссылку, чтобы активировать свой аккаунт:
             
            Спасибо, что зарегистрировались!
 Ваша учетная запись создана, вы можете войти в систему после активации учетной записи, нажав на url
 ниже.
             API ключи О нас Токен доступа Адрес Все электронные письма Все права защищены. Все, что вам нужно знать о временной почте У вас уже есть учетная запись? Произошла ошибка при попытке входа через учетную запись в социальной сети. Вы действительно хотите удалить электронную почту Вы действительно хотите удалить этот токен. Вернуться к списку Изменить Проверьте свою электронную почту. Закрыть Подтвердите пароль Свяжитесь с нами Политика использования файлов cookie Политика использования файлов cookie Скопировано! Копировать Скопируйте адрес электронной почты из верхнего ввода Создайте Создать адрес электронной почты Дата Удалить Удалить адрес электронной почты Удалить токен Демо-почтовый ящик Не получили электронное письмо? Одноразовая почта Домен У вас нет учетной записи? Электронная почта Активация электронной почты завершена Электронная почта уже существует. Полученные электронные письма Введите адрес электронной почты Страница записей Срок действия истекает ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ Обратная связь Забыли пароль? Часто задаваемые вопросы От ИДТИ Генерировать Получите бесплатную временную электронную почту уже сегодня! Получите временную электронную почту сегодня Получить мой адрес электронной почты Перейти Привет, я бы хотел Как продлить срок службы адреса электронной почты? Как я могу поделиться временной электронной почтой? Насколько легко это было использовать? Как проверить полученные письма? Как удалить одноразовую электронную почту и получить новую? Как отправить электронную почту? Как использовать одноразовую временную электронную почту? Как им пользоваться Я принимаю Входящие Венные ящики Давайте поговорим Загрузка временного адреса электронной почты Войти Логин Войдите в свой аккаунт Выход Более старые записи Более свежие записи Теперь вы можете войти в свою учетную запись и начать пользоваться сервисом. Наши контакты Наш надежный и безопасный сервис позволяет вам получить полностью анонимный и конфиденциальный
 адрес электронной почты, на который вы
 можно использовать сразу.
 На самом деле вы можете создать свой временный адрес электронной почты и использовать его всякий раз, когда вы
 хочу. Наш надежный и безопасный сервис позволяет вам получить полностью анонимный и конфиденциальный адрес электронной почты, который вы
 можно использовать сразу.
 На самом деле вы можете создать свой временный адрес электронной почты и использовать его в любое время. пароль Сброс пароля Пожалуйста, перейдите на следующую страницу и выберите новый пароль: Популярные статьи Политика конфиденциальности Проекты, протестированные с помощью поддельных Необработанное содержимое Повторная отправка электронной почты активации Читать входящие письма на этой странице в разделе «Входящие» Обновить Регистрация завершена Сброс пароля Ссылка для сброса пароля будет отправлена на ваш адрес электронной почты. Экономьте свое время, никогда не создавая адрес электронной почты. Сканировать QR-код с помощью мобильного телефона Отправить отзыв Отправитель Общая папку «Входящие» Покажите нам свою любовь, порекомендовав нас своей любимой сети или сообществу! Зарегистрироваться Зарегистрироваться Зарегистрируйтесь бесплатно! Подписаться Ошибка входа в социальную сеть Обнаружены спам-сообщения Без спама Начать сейчас Прекратите получать мусорные письма и забудьте о кнопках «фильтров» папки «Входящие» и «отписаться» Тема Расскажите о нас Временная почта Служба временной электронной почты Положения и условия Условия использования Спасибо, что зарегистрировались! Спасибо за использование нашего сайта! Команда %(site_name)s Учетная запись, которую вы пытались активировать, недействительна. Учетная запись, которую вы пытались активировать, уже активирована. Указанный ключ активации недействителен. Введенный адрес электронной почты был ошибкой или опечаткой. (Случается с лучшими из нас.) Электронная почта находится в папке спама. (Иногда там все теряется.) Срок действия этой учетной записи истек. Эта тенденция доказывает, что, используя ссылки, операторы вредоносных кампаний получают больше кликов и инфекций по сравнению с классической техникой прикрепления файлов к электронной почте. Очевидно, что пользователи стали недоверчивы к любым вложениям в почте. Используйте это, чтобы зарегистрироваться на веб-сайтах, в социальных сетях и т. Д. Полезные ссылки Посмотреть Ожидание входящих писем Мы стремимся сосредоточить внимание на том, чтобы вы были в безопасности в Интернете. Мы очень ценим это, спасибо! Что такое одноразовая временная электронная почта Кто вы и чем мы можем помочь? Зачем мне нужна временная почта? Вы бы порекомендовали наш сервис? На данный момент у вас нет токенов. Вы не хотите делиться своим личным адресом электронной почты? Вы устали от
 спам? Вы не хотите делиться своим личным адресом электронной почты? Вы устали от спама? Вы получаете это письмо, потому что запросили сброс пароля для своей учетной записи пользователя по адресу
 %(site_name)s. Ваш Ваш бесплатный временный адрес электронной почты Ваши друзья и коллеги могут получить мгновенный просмотр входящей электронной почты. Ваш почтовый ящик пуст Ваше имя пользователя, если вы забыли: тому назад миллиарды почтовых ящиков код предназначенный для решения всех этих проблем. эксперты подсчитали, что 87%% всего спама электронной почты, отправленного во втором квартале 2020 года (апрель, май и июнь месяцы), содержали ссылки для загрузки вредоносных файлов, но не вложения с самими файлами. бесплатный мониторинг сайта - это адрес, который выдается на короткое время: от нескольких минут до целого месяца, а затем он будет удален со всеми входящими письмами и вложениями, или просто недоступен для дальнейшего использования. специально разработана, чтобы помочь вам забыть о спаме, рекламных рассылках, взломе и атаке роботов. Держите ваш реальный почтовый ящик в чистоте и безопасности. предоставляет временный, безопасный, анонимный, бесплатный, одноразовый адрес электронной почты. подписаться 