��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  �  R;  �   �<     r=  �   �=  �   �>  (  :?  �  c@  �   B  �   �B  �   �C  �   "D  �   �D  c   TE  �   �E  �   aF  �   �F  �   �G  �   ZH  o  /I  K  �M  S   �Q  �   ?R     �R     �R     �R     S     S     S  ,   0S     ]S  O   oS  3   �S  6   �S     *T  	   9T     CT     ]T     eT     wT     �T     �T     �T     �T  $   �T  	   �T     �T     U     U     U     !U  
   /U     :U     OU     dU     jU     xU     U     �U     �U     �U     �U     �U     �U     �U     V     &V     AV     DV  	   HV  -   RV  !   �V     �V     �V     �V  /   �V  "   W  !   /W  #   QW  2   uW     �W  ;   �W     �W  	   X     X     !X     *X     ?X     _X     lX     tX     �X     �X     �X  F   �X     �X  �   Y  �   �Y     �Z     �Z  ;   �Z     [     *[  $   ?[     d[  %   t[  G   �[     �[     �[     \  <   \  E   L\  '   �\     �\     �\     �\  a   ]     f]     s]     �]     �]  !   �]     �]  	   �]     �]  ^   �]     Q^     Y^     f^     z^     �^     �^     �^  #   �^     �^  5   _  :   D_  4   _  Y   �_  D   `     S`    j`  K   ra     �a     �a     �a  L   �a     @b  '   Yb  #   �b  (   �b  &   �b     �b  M   c  W   [c  o   �c     #d  #   (d  H   Ld     �d  4   �d     �d  !   �d     e  %   	e  �   /e     f  �   f  �   �f  Q   �g     �g     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 
                                            100%% Privatni poštanski sandučići sa punim vlasništvom!
                                         
                                            Generišite lažnu e-poštu jednim klikom
                                         
                                            Sakrij se od neželjene pošte sa poboljšanom privatnošću i bezbednošću
                                         
                                        Kliknite na „Izbriši“ na početnoj stranici i dobićete novu privremenu e-poštu.
                                     
                                        E-mail adresa važi dok je ne izbrišete ili dok je usluga ne izbriše.
 Životni vek zavisi od mnogo različitih faktora.
 Možete imati privremene e-mailove sa životnim vremenom do 1 GODINA sa registrovanim
 račun.
                                     
                                        Mnoge aplikacije i veb sajtovi obavezuju vas da bez razloga dajete ličnu e-mail adresu
 osim da prikupljaju lične podatke o vama.
 Privremena email adresa vam omogućava da odvojite svoju ličnu email adresu
 koristite za bankarstvo, kupovine kreditnih kartica i druge lične identifikovanje
 transakcije sa svih ostalih sajtova tamo.
                                     
                                        Slanje e-pošte je potpuno onemogućeno i
 nikada neće biti sprovedena zbog prevare i neželjene pošte pitanja.
                                     
                                        Oni su prikazani pod vašim e-mailom za jednokratnu upotrebu.
 Ako niste dobili dolazne e-mailove predugo - kliknite na dugme „Osveži“.
                                     
                                        Možete ga deliti pomoću QR koda na glavnoj stranici.
                                     
                                    Možete imati maksimalne %(max_tokens_allowed)s tokene.
                                     
                                Ne možemo dostaviti e-poštu na ovu adresu. (Obično zbog korporativnih zaštitnih zidova
 ili filtriranje.)
                                 
                                Zašto mi treba %(BRAND_NAME)s nalog?
                             
                                Slučajno ste nam dali drugu e-mail adresu. (Obično rad ili lično
 umesto onog na koji ste mislili.)
                                 
                            Ako ne vidite e-mail od nas u roku od nekoliko minuta, nekoliko
 stvari su mogle da se desi:
                             
                            Mi smo vam poslali uputstva za podešavanje lozinke, ako nalog
 postoji sa e-mailom koji ste uneli. Vi
 treba da ih primi uskoro.
                         
                        Ako ne primite e-mail, molimo vas da se uverite da li ste uneli adresu koju
 registrovan
 sa, i proverite
 spam folder.
                         
 UpTimeMetrics platforma je dizajnirana da prati status veb-sajtova. To uključuje takve
 karakteristike: Dostupnost sajta,
 Dostupnost servera,
 Provera validnosti SSL sertifikata veb-sajta i HTML validacije.
  
                    %(BRAND_NAME)s je usluga za jednokratnu upotrebu e-pošte. Kada posetite %(BRAND_NAME)s novu e-poštu
 adresa se generiše samo za vas. Generisana adresa e-pošte može odmah primiti bilo koju e-poštu.
 Svaka e-pošta koja je primljena odmah će se pojaviti na glavnoj stranici. Niko drugi nego što ćete videti
 e-mailove
 koji su primljeni. Važno je napomenuti da e-mail adresa ističe nakon 24 sata. Kada
 elektronska pošta
 adresa je istekao, e-mail adresa i sve primljene e-poruke će nestati. Nova adresa e-pošte
 volja
 biti generisani nakon posete ovoj veb stranici. Postoji mnogo razloga za korišćenje e-pošte za jednokratnu upotrebu.
 Možda ćete želeti da nekome date adresu e-pošte bez otkrivanja vašeg identiteta. Ili možda želite
 prijavite se za veb stranicu ili veb uslugu, ali ste zabrinuti da će vam veb lokacija poslati neželjenu poštu
 budućnost. <br/>
 Za korišćenje ove usluge morate omogućiti kolačić i javascript, kolačić je samo da snimite sesiju
 id
 i preferencije jezika, vaša privatnost je pokrivena našom politikom privatnosti i kolačića.
                 
                    %(BRAND_NAME)s je usluga e-adrese za jednokratnu upotrebu. Kada posetite %(BRAND_NAME)s novu e-poštu
 adresa se generira samo za vas. Generisana adresa e-pošte može odmah da primi bilo kakve e-mailove.
 Svaka e-pošta koja je primljena odmah će se pojaviti na glavnoj stranici. Niko osim vas neće videti e-mailove
 koji su primljeni. Važno je napomenuti da adresa e-pošte ističe nakon 24 sata. Kada e-mail
 adresa je istekla, e-mail adresa i svi primljeni e-mailovi će nestati. Nova imejl adresa će
 biti generisana nakon posete ovaj sajt. Postoji mnogo razloga za korišćenje e-maila za jednokratnu upotrebu.
 Možda ćete želeti da nekome date email adresu bez otkrivanja vašeg identiteta. Ili može da iskate da
 prijavite se za veb sajt ili veb servis, ali ste zabrinuti da će vam sajt poslati neželjenu poštu u budućnosti. <br />
 Za korišćenje ove usluge morate omogućiti kolačić i JavaScript, kolačić je samo da snimi ID sesije
 i jezičke preferencije, Vaša Privatnost je pokrivena pod našom politikom privatnosti i kolačića.
                 
                Kliknite na ovaj link da biste aktivirali svoj nalog:
             
            Hvala što ste se prijavili!
 Vaš nalog je kreiran, možete se prijaviti nakon što ste aktivirali svoj nalog pritiskom na URL
 ispod.
             API ključevi O nama Pristupni token Adresa Svi e-mailovi Sva prava zadržana. Sve što treba da znate o privremenoj pošti Već imate nalog? Došlo je do greške prilikom pokušaja prijave putem naloga društvene mreže. Da li ste sigurni da želite da izbrišete e-poštu Da li ste sigurni da želite da izbrišete ovaj token. Nazad na listu Promenite Proverite svoju e-poštu. Zatvori Potvrdite lozinku Kontaktirajte nas Politika kolačića Politika kolačića Kopiraj! Kopiraj Kopirajte email adresu sa vrha unosa Kreirajte Kreiraj e-poštu Datum Obriši Obriši e-poštu Obriši token Demo Inbok Niste dobili e-mail? Jednokratna e-pošta Domen Nemate nalog? E-mail Aktivacija e-pošte završena E-pošta već postoji. E-mailovi primljeni Unesite e-mail adresu Stranica unosa Ističe FAK Povratne informacije Zaboravili ste lozinku? Često postavljana pitanja Od IDI Generišu Dobiti svoj slobodan privremeni e-mail danas! Dobijte privremenu e-poštu danas Nabavite moju email adresu Idi Zdravo tamo, želeo bih da Kako da produžim životni vek adrese e-pošte? Kako da delim privremenu e-poštu? Koliko je to bilo lako koristiti? Kako proveriti primljene e-mailove? Kako izbrisati jednokratnu e-poštu i dobiti novu? Kako da pošaljete e-mail? Kako koristiti privremenu e-poštu za jednokratnu upotrebu? Kako ga koristiti Prihvatam Inbok Inbotije Hajde da razgovaramo Učitavanje privremene e-adrese Prijavite se Prijava Prijavite se na svoj nalog Odjava Više starih unosa Noviji unosi Sada se možete prijaviti na svoj nalog i početi da koristite uslugu. Naši kontakti Naša robustan i siguran servis omogućava vam da se potpuno anoniman i privatan
 email adresa koju
 može odmah koristiti.
 Zapravo možete kreirati svoju privremenu adresu e-pošte i koristiti je kad god
 žele. Naša robusna i bezbedna usluga omogućavaju vam da dobijete potpuno anonimnu i privatnu e-mail adresu koju
 mogu odmah koristiti.
 Zapravo možete kreirati svoju privremenu e-mail adresu i koristiti ga kad god želite. Lozinka Poništavanje lozinke Molimo idite na sledeću stranicu i izaberite novu lozinku: Popularne Članci Politika privatnosti Projekti testirani lažnim inboksima Sirovi sadržaj Ponovno pošaljite aktivacioni e-mail Pročitajte dolazne e-mailove na ovoj stranici unutar sekcije inbočeta Osveži Registracija kompletna Reset lozinke Veza za resetovanje lozinke će biti poslata na vaš e-mail. Sačuvajte vreme tako što nikada ne morate da kreirate email adresu. Skenirajte QR kod sa mobilnim telefonom Pošalji povratne informacije Pošiljalac Zajedničko poštansko sanduče Pokažite nam neku ljubav tako što ćete nas preporučiti vašoj omiljenoj mreži ili zajednici! Prijavite se Prijavite se Prijavite se besplatno! Signup Društvena mreža Prijava Neuspeh Otkrivena spam e-mailova Spam Free Počnite sada Prestanite da primate smeće e-pošte i zaboravite na dugmad „filtere“ i „odjavite se“ Predmet Kaži za nas Privremena e-pošta Privremena usluga e-pošte Uslovi i uslovi Uslovi korišćenja Hvala što ste se prijavili! Hvala što ste koristili naš sajt! Tim %(site_name)s - Nalog koji ste pokušali da aktivirate je nevažeći. Račun koji ste pokušali da aktivirate već je aktiviran. Ključ za aktiviranje koji ste naveli je nevažeći. E-mail adresa koju ste uneli imala je grešku ili grešku. (Događa se najboljem od nas.) E-pošta je u vašoj spam fascikli. (Ponekad se stvari izgube tamo.) Ovaj nalog je istekao. Ovaj trend dokazuje da korišćenjem linkova operateri zlonamernih kampanja dobijaju više klikova i infekcija u odnosu na klasičnu tehniku pričvršćivanja fajlova na e-mailove. Očigledno, korisnici su postali nepoverljivi prema bilo kojim prilozima u pošti. Koristite ovo da biste se prijavili na sajtovima, društvenim medijima itd. Korisni linkovi Pogledaj Čekanje dolaznih e-mailova Cilj nam je visoko da bude fokusiran na čuvanje vas bezbednim na internetu. Zaista to cenimo, hvala! Šta je jednokratna privremena e-pošta Ko si ti, i kak možem da pomognem? Zašto mi je potrebna privremena pošta? Da li biste preporučili našu uslugu? Trenutno nemate tokene. Ne želite da delite svoju ličnu adresu e-pošte? Da li ste umorni od
 spam? Ne želite da delite svoju ličnu e-mail adresu? Da li ste umorni od neželjene pošte? Primamo ovu e-poštu jer ste zatražili resetovanje lozinke za svoj korisnički nalog na adresi
 %(site_name)s. Vaš Vaša besplatna privremena e-pošta Vaši prijatelji i kolege mogu dobiti trenutni pregled dolazne e-pošte. Vaš inbok je prazan Vaše korisničko ime, u slučaju da ste zaboravili: pre milijarde poštanskih sandučića kod dizajniran da reši sve ove probleme. stručnjaci su izračunali da 87%% svih spam e-pošte poslat u drugom kvartalu 2020. godine (april, maj i jun meseci) sadržavao linkove za preuzimanje zlonamjernih datoteka, ali ne i priloge sa samim datotekama. free monitoring veb sajta je adresa koja se izdaje za kratko vreme: od nekoliko minuta do čitavog meseca, a onda će biti izbrisana sa svim dolaznim slovima i prilozima, ili jednostavno nedostupna za dalju upotrebu. platforma je specijalno dizajniran da vam pomogne da zaboravite na neželjenu poštu, reklamne pošte, hakovanje i napada robota. Čuvajte svoje pravo poštansko sanduče čisto i bezbedno. obezbeđuje privremeno, bezbedno, anonimno, besplatno, jednokratnu e-mail adresu. registracija 