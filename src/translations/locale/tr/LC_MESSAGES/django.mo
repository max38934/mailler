��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;  A  R;  �   �<  �   =  �   �=  �   5>  $  �>  �  �?  �   �A  �   oB  �   YC  �   �C  �   aD  i   E  �   yE  �   ,F  �   �F  �   �G    >H  z  AI  s  �M  e   0R  �   �R     #S     1S     >S     PS     VS     fS  1   �S     �S  Q   �S  /   #T  1   ST     �T  
   �T     �T     �T     �T     �T     �T     �T     U     U  +   U     DU     MU     ^U     dU     hU     tU     �U     �U     �U  	   �U     �U     �U  "   �U     V     V     1V     @V     SV     aV     eV     rV     �V     �V     �V     �V  -   �V  "   �V     	W     W     !W  0   8W  *   iW  !   �W  )   �W  I   �W      *X  6   KX     �X     �X     �X     �X     �X  #   �X  
   �X     Y     	Y     $Y     3Y     GY  N   [Y     �Y  �   �Y  �   �Z     W[     ^[  =   q[     �[     �[  .   �[     \  ,   \  :   A\     |\     �\     �\  @   �\  ?   �\  #   +]     O]  	   e]     o]  N   �]     �]     �]     �]     �]     ^     '^     E^     T^  z   b^     �^     �^     �^      _     _     ._     D_  3   d_     �_  5   �_  D   �_  5   (`  Y   ^`  E   �`     �`  "  a  A   <b     ~b     �b     �b  C   �b  '   �b  '   $c  +   Lc  %   xc     �c     �c  X   �c  N   (d  m   wd     �d     �d  _   	e     ie  $   |e     �e     �e     �e  4   �e    �e     �f  �   g  �   �g  O   �h     �h     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
                                            %%100 Tam sahipliğe sahip özel posta kutuları!
                                         
                                            Tek bir tıklama ile sahte e-posta oluştur
                                         
                                            Gelişmiş gizlilik ve güvenlik ile kendinizi spam'den gizleyin
                                         
                                        Ana sayfada “Sil” e tıklayın ve yeni geçici e-posta alacaksınız.
                                     
                                        E-posta adresi siz silene kadar veya hizmet silinene kadar geçerlidir.
 Ömür boyu birçok farklı faktöre bağlıdır.
 Kayıtlı olan 1 YIL'a kadar ömrü olan geçici e-postalara sahip olabilirsiniz
 hesabı.
                                     
                                        Birçok uygulama ve web sitesi sebepsiz yere kişisel e-posta adresi vermenizi zorunlu kılar
 hakkınızda kişisel bilgi toplamak dışında.
 Geçici e-posta adresi, kişisel e-posta adresinizi ayırmanızı sağlar
 Eğer bankacılık, kredi kartı alımları ve diğer kişisel tanımlama için kullanmak
 orada tüm diğer sitelerden işlemler.
                                     
                                        E-posta gönderme tamamen devre dışı bırakıldı ve
 dolandırıcılık ve spam sorunları nedeniyle asla uygulanmayacaktır.
                                     
                                        Bunlar tek kullanımlık e-postanızın altında görüntülenir.
 Gelen e-postaları çok uzun süre almadıysanız - “Yenile” düğmesine tıklayın.
                                     
                                        Ana sayfadaki QR kodunu kullanarak paylaşabilirsiniz.
                                     
                                    Maksimum %(max_tokens_allowed)s jetonlarınız olabilir.
                                     
                                E-postayı bu adrese teslim edemeyiz. (Genellikle çünkü kurumsal güvenlik duvarları
 veya filtreleme.)
                                 
                                Neden %(BRAND_NAME)s hesaba ihtiyacım var?
                             
                                Yanlışlıkla bize başka bir e-posta adresi verdiniz. (Genellikle bir iş veya kişisel bir
 yerine kast bir.)
                                 
                            Birkaç dakika içinde bizden bir e-posta görmüyorsanız
 Bazı şeyler olabilirdi:
                             
                            Bir hesap varsa, şifrenizi belirleme yönergelerini size e-postayla gönderdik
 girdiğiniz e-posta ile mevcut. Sen
 kısa sürede almalısınız.
                         
                        Eğer bir e-posta almazsanız, lütfen adresinizi girdiğinizden emin olun
 kayıtlı
 ile, ve kontrol senin
 spam klasörüne gidin.
                         
 UptimeMetrics platformu web sitelerinin durumunu izlemek için tasarlanmıştır. Bu tür içerir
 özellikler: Web sitesinin kullanılabilirliği,
 Sunucu kullanılabilirliği,
 Web sitesinin SSL sertifikası ve HTML doğrulaması geçerlilik kontrolü.
  
                    %(BRAND_NAME)s tek kullanımlık bir e-posta adresi hizmetidir. %(BRAND_NAME)s yeni bir e-postayı ziyaret ettiğinizde
 adresi sadece sizin için oluşturulur. Oluşturulan e-posta adresi hemen herhangi bir e-posta alabilir.
 Alınan herhangi bir e-posta hemen ana sayfada görünecektir. Senden başka kimse görmeyecek.
 e-postalar
 Bu alındı. Bir e-posta adresinin 24 saat sonra sona erdiğini unutmamak önemlidir. Ne zaman bir
 E-posta
 adresinizin süresi doldu, e-posta adresi ve alınan e-postalar gitmiş olacak. Yeni bir e-posta adresi
 ecek
 Bu web sitesini ziyaret ederek oluşturulacaktır. Tek kullanımlık bir e-posta kullanmanın birçok nedeni vardır.
 Kimliğinizi ifşa etmeden birine e-posta adresi vermek isteyebilirsiniz. Ya da isteyebilirsiniz
 bir web sitesi veya web hizmeti için kaydolun ancak web sitesinin size spam'i göndereceğinden endişe ediyorsunuz
 Geleceğe. <br/>
 Bu hizmeti kullanmak için çerez ve javascript'i etkinleştirmelisiniz, çerez sadece oturumunuzu kaydetmektir
 kimlik
 ve dil tercihi, Gizliliğiniz Gizlilik ve Çerez politikamız kapsamındadır.
                 
                    %(BRAND_NAME)s tek kullanımlık bir e-posta adresi hizmetidir. %(BRAND_NAME)s yeni bir e-postayı ziyaret ettiğinizde
 adresi sadece sizin için oluşturulur. Oluşturulan e-posta adresi hemen tüm e-postaları alabilir.
 Alınan herhangi bir e-posta hemen ana sayfada görünür. Senden başka kimse e-postaları görmeyecek
 Alınan. Bir e-posta adresinin 24 saat sonra sona erdiğini unutmamak önemlidir. Ne zaman bir e-posta
 adresi doldu, e-posta adresi ve alınan tüm e-postalar gitmiş olacak. Yeni bir e-posta adresi olacak
 Bu web sitesini ziyaret ederek oluşturulacaktır. Tek kullanımlık bir e-posta kullanmanın birçok nedeni vardır.
 Kimliğinizi açıklamadan birine bir e-posta adresi vermek isteyebilirsiniz. Ya da isteyebilirsiniz
 bir web sitesi veya web hizmeti için kaydolun, ancak web sitesinin gelecekte size spam göndereceğinden endişe duyuyorsunuz. <br />
 Bu hizmeti kullanmak için çerez ve javascript etkinleştirmeniz gerekir, çerez sadece oturum kimliğinizi kaydetmek içindir
 ve dil tercihi, Gizliliğiniz Gizlilik ve Çerez politikamız kapsamındadır.
                 
                Hesabınızı etkinleştirmek için lütfen bu bağlantıya tıklayın:
             
            Kaydınız için teşekkürler!
 Hesabınız oluşturuldu, hesabınızı etkinleştirdikten sonra url
 aşağıda.
             API tuşları Hakkımızda Erişim Belirteci Adres Tüm E-postalar Tüm Hakları Saklıdır. Geçici posta hakkında bilmeniz gereken her şey Zaten bir hesabınız var mı? Sosyal ağ hesabınız üzerinden oturum açmaya çalışırken bir hata oluştu. E-postayı silmek istediğinizden emin misiniz? Bu belirteci silmek istediğinizden emin misiniz? Listeye geri dön Değiştir E-mailini kontrol et. Kapat Şifreyi onayla Bize Ulaşın Çerez Politikası Çerezler Politikası Kopyalandı! Kopya E-posta adresini üst girişten kopyalayın Oluştur E-posta Oluştur Tarih Sil E-posta Sil Simgeyi Sil Demo Gelen Kutusu E-posta almadın mı? Tek kullanımlık e-posta Alan Adı Hesabınız yok mu? E-posta E-posta Etkinleştirme Tamamlandı E-posta Zaten var. Alınan e-postalar E-Posta Adresi Girişler sayfası Süresi Dolur SSS Geribildirim Şifremi mi unuttun Sıkça Sorulan Sorular itibaren GİTMEK Oluştur Ücretsiz Geçici E-postanızı Bugün Alın! Geçici E-postanızı Bugün Alın E-posta adresimi al Git Merhaba, ben istiyorum E-posta adresinin ömrünü nasıl uzatabilirim? Geçici e-postayı nasıl paylaşabilirim? Bunu kullanmak ne kadar kolaydı? Alınan e-postalar nasıl kontrol edilir? Tek kullanımlık e-posta nasıl silinir ve yenisini nasıl alırsınız? Nasıl e-posta göndermek için? Tek kullanımlık geçici e-posta nasıl kullanılır? Nasıl kullanılır Kabul ediyorum Gelen Kutusu Gelen Kutuları Hadi konuşalım Geçici e-posta adresinizi yükleme Giriş Yap Giriş Hesabınıza giriş yapın Oturum Kapatma Daha eski girişler Daha yeni girişler Artık hesabınıza giriş yapabilir ve hizmeti kullanmaya başlayabilirsiniz. Kişilerimiz Sağlam ve güvenli hizmetimiz tamamen anonim ve özel bir
 e-posta adresiniz
 hemen kullanabilirsiniz.
 Aslında kendi geçici e-posta adresinizi oluşturabilir ve her zaman kullanabilirsiniz
 istiyorum. Sağlam ve güvenli hizmetimiz tamamen anonim ve özel bir e-posta adresi almanızı sağlar
 hemen kullanabilirsiniz.
 Aslında kendi geçici e-posta adresi oluşturmak ve istediğiniz zaman kullanabilirsiniz. Şifre Şifre sıfırlama Lütfen aşağıdaki sayfaya gidin ve yeni bir şifre seçin: Popüler Makaleler Gizlilik Politikası Sahte gelen kutuları ile test edilen projeler Hham İçerik Etkinleştirme e-postasını yeniden gönder Gelen kutusunun içindeki bu sayfadaki e-postaları okuyun Yenile Kayıt Tamamlandı Parolayı sıfırla Şifre sıfırlama bağlantısı e-postanızda gönderilecektir. Bir e-posta adresi oluşturmak zorunda kalmadan zaman kazanın. Cep telefonu ile QR kodunu tarayın Geri Bildirim Gönder Gönderen Paylaşılan Gelen kutusu En sevdiğiniz ağa veya topluluğa bizi önererek bize biraz sevgi gösterin! Kaydolun Kaydolun Ücretsiz kaydolun! Kaydolun Sosyal Ağ Oturum Açma Hatası Spam E-postaları Algılandı Spam Ücretsiz Şimdi Başla Çöp kutusu e-postaları almayı durdurun ve gelen kutusu “filtreler” ve “abonelikten çık” düğmelerini unutun Konu Bizden bahset Geçici E-posta Geçici E-posta Hizmeti Şartlar ve Koşullar Kullanım Koşulları Kaydınız için teşekkürler! Sitemizi kullandığınız için teşekkür ederiz! %(site_name)s takım Etkinleştirmeye çalıştığınız hesap geçersiz. Etkinleştirmeye çalıştığınız hesap zaten etkinleştirilmiş. Sağladığınız etkinleştirme anahtarı geçersiz. Girdiğiniz e-posta adresinde bir hata veya yazım hatası vardı. (En iyilerimize olur.) E-posta, spam klasörünüzdedir. (Bazen orada bir şeyler kaybolur.) Bu hesabın süresi doldu. Bu eğilim, bağlantıları kullanarak, kötü amaçlı kampanyaların operatörlerinin e-postalara dosya ekleme klasik tekniğine kıyasla daha fazla tıklama ve enfeksiyon aldığını kanıtlamaktadır. Açıkçası, kullanıcılar postadaki herhangi bir ekten güvensiz hale gelmiştir. Web sitelerine, sosyal medyaya vb. kaydolmak için bunu kullanın Faydalı Linkler Görüntüle Gelen e-postaları bekliyor Sizi internette güvende tutmaya odaklanmış olmayı hedefliyoruz. Gerçekten minnettarız, teşekkürler! Tek kullanımlık geçici e-posta nedir Sen kimsin ve nasıl yardımcı olabiliriz? Neden geçici postaya ihtiyacım var? Hizmetimizi önerir misiniz? Şu anda jeton yok. Kişisel e-posta adresinizi paylaşmak istemiyor musunuz? Eğer yorgun musunuz
 Spam'mi? Kişisel e-posta adresinizi paylaşmak istemiyor musunuz? Spam yorgun musunuz? Bu e-postayı, kullanıcı hesabınız için parola sıfırlama talebinde bulunduğunuz için
 %(site_name)s. senin Ücretsiz Geçici E-postanız Arkadaşlarınız ve iş arkadaşlarınız gelen e-postanın anında görüntüsünü alabilir. Gelen kutunuz boş Unuttuysanız, kullanıcı adınız: önce milyarlarca gelen kutusu kod tüm bu sorunları çözmek için tasarlanmıştır. Uzmanlar, 2020'nin ikinci çeyreğinde (Nisan, Mayıs ve Haziran ayları) gönderilen tüm e-postaların%%87'sinin kötü amaçlı dosyaları indirmek için bağlantılar içerdiğini, ancak dosyaların kendisiyle ilgili ekleri içermediğini hesapladılar. ücretsiz web sitesi izleme kısa bir süre için verilen bir adrestir: birkaç dakikadan bir aya kadar ve daha sonra gelen tüm harfler ve eklerle silinir veya daha fazla kullanım için kullanılamaz. platformu, spam, reklam postaları, hack ve robotlara saldıran hakkında unutmanıza yardımcı olmak için özel olarak tasarlanmıştır. Gerçek posta kutunuzu temiz ve güvenli tutun. geçici, güvenli, anonim, ücretsiz, tek kullanımlık e-posta adresi sağlar. kaydolun 