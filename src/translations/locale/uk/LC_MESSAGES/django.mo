��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;     R;  �   s=  �   (>  �   �>  �   �?    �@    �B  .  �E  R  �F  �   0H  �   �H     �I  �   �J  !  DK    fL  Z  mM    �N  w  �O  `  SQ  �  �Y  �   gb    c     d      d     .d     Hd  &   Ud  #   |d  M   �d  (   �d  �   e  Y   �e  N   f  (   Pf     yf  <   �f     �f  #   �f     �f  -   g  -   Fg     tg     �g  d   �g     h  0   h     Dh     Mh  0   ^h     �h     �h  :   �h     �h  
   i  7   $i     \i  0   ci  2   �i     �i  =   �i     !j     Aj     Zj     ^j     mj     �j     �j     �j     �j  Y   �j  V   #k  F   zk     �k      �k  c   �k  l   Ul  K   �l  6   m  R   Em      �m  p   �m  &   *n     Qn     cn     pn     }n  \   �n     �n     �n  B   	o     Lo  (   io  ,   �o  �   �o     Ap  �  [p  �  <r     t     t  {   ;t     �t  1   �t  a   	u     ku  G   �u  �   �u     ^v  '   mv     �v  f   �v  |   w  Z   �w     �w     x  (   %x  �   Nx     �x     �x  .   y     Cy  <   Ty  $   �y  !   �y     �y  �   �y     �z      �z     �z  C   �z     3{  #   R{  )   v{  E   �{     �{  k   |  t   o|  J   �|  �   /}  �   �}  L   ^~  �  �~  �   ��  !   "�     D�  0   [�  �   ��  .   "�  U   Q�  <   ��  <   �  <   !�  A   ^�  �   ��  �   (�    ˄     څ  R   �  �   4�  7   І  D   �     M�  2   V�     ��  C   ��  �  ԇ  8   g�  n  ��    �  �   +�     ˌ     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 
                                            100%% приватних поштових скриньок з повною власністю!
                                         
                                            Створіть підроблену електронну пошту одним клацанням миші
                                         
                                            Сховайте себе від спаму з підвищеною конфіденційністю та безпекою
                                         
                                        Натисніть «Видалити» на головній сторінці, і ви отримаєте новий тимчасовий email.
                                     
                                        Адреса електронної пошти дійсна до тих пір, поки ви не видалите її або поки служба не видалить.
 Термін служби залежить від безлічі різних факторів.
 Ви можете мати тимчасові електронні листи з терміном служби до 1 року з зареєстрованими
 рахунок.
                                     
                                        Багато додатків і веб-сайтів зобов'язують вас надавати особисту адресу електронної пошти без причини
 крім збору особистої інформації про вас.
 Тимчасова адреса електронної пошти дозволяє відокремити вашу особисту адресу електронної пошти
 Ви використовуєте для банківських операцій, покупок кредитних карт та інших особистих ідентифікаційних
 угод з усіх інших ділянок там.
                                     
                                        Відправка електронної пошти повністю відключена і
 він ніколи не буде реалізований через проблеми з шахрайством і спамом.
                                     
                                        Вони відображаються під вашою одноразовою електронною поштою.
 Якщо ви не отримували вхідні листи занадто довго - натисніть кнопку «Оновити».
                                     
                                        Ви можете поділитися ним, використовуючи QR-код на головній сторінці.
                                     
                                    Ви можете мати максимум токенів %(max_tokens_allowed)s.
                                     
                                Ми не можемо доставити повідомлення електронної пошти на цю адресу. (Зазвичай через фірмові брандмауери
 або фільтрації.)
                                 
                                Навіщо потрібен обліковий запис %(BRAND_NAME)s?
                             
                                Ви випадково дали нам іншу адресу електронної пошти. (Зазвичай це робочий або особистий
 замість того, що ви мали на увазі.)
                                 
                            Якщо ви не бачите повідомлення електронної пошти від нас протягом декількох хвилин, кілька
 могли статися речі:
                             
                            Ми надіслали вам інструкції щодо встановлення пароля, якщо обліковий запис
 існує з введеним вами електронним листом. Ви
 повинні отримати їх найближчим часом.
                         
                        Якщо ви не отримали листа, будь ласка, переконайтеся, що ви ввели адресу, яку ви
 зареєстрований
 з, і перевірте
 папка спаму.
                         
 Платформа UpTimeMetrics призначена для моніторингу стану сайтів. Вона включає в себе такі
 особливості: наявність сайту,
 Доступність сервера,
 Перевірка достовірності SSL сертифіката веб-сайту та перевірки HTML.
  
                    %(BRAND_NAME)s — це одноразова служба електронної пошти. Коли ви відвідуєте %(BRAND_NAME)s новий електронний лист
 адреса генерується саме для вас. На згенеровану адресу електронної пошти можна відразу ж отримувати будь-які електронні листи.
 будь-яка електронна пошта, яка отримана, з'явиться на головній сторінці негайно. Ніхто крім вас не побачить
 електронні листи
 які отримані. Важливо відзначити, що адреса електронної пошти закінчується через 24 години. Коли
 електронна пошта
 Термін дії адреси закінчився, адреса електронної пошти та будь-які отримані електронні листи зникнуть. Нова адреса електронної пошти
 буде
 створюються при відвідуванні цього веб-сайту. Є багато причин для використання одноразового електронного листа.
 Можливо, ви захочете надати комусь електронну адресу, не розкриваючи вашу особу. Або ви можете захотіти
 підпишіться на веб-сайт або веб-сервіс, але ви стурбовані тим, що веб-сайт надішле вам спам
 майбутнє. <br/>
 Для використання цієї послуги ви ПОВИННІ включити файли cookie і javascript, файли cookie просто для запису вашого сеансу
 ID
 та мовні переваги, ваша конфіденційність поширюється на нашу Політику конфіденційності та файлів cookie.
                 
                    %(BRAND_NAME)s є одноразовою службою адрес електронної пошти. Коли ви відвідуєте %(BRAND_NAME)s нове повідомлення електронної пошти
 адреса генерується саме для вас. Згенерована адреса електронної пошти може відразу отримувати будь-які електронні листи.
 Будь-які отримані повідомлення електронної пошти відображатимуться на головній сторінці негайно. Ніхто, крім вас, не буде бачити електронні листи
 які отримані. Важливо відзначити, що адреса електронної пошти закінчується через 24 години. Коли повідомлення електронної пошти
 минув, адреса електронної пошти та всі отримані листи не будуть. Нова адреса електронної пошти буде
 бути згенеровані при відвідуванні цього веб-сайту. Існує безліч причин для використання одноразового електронного листа.
 Можливо, ви захочете надати комусь адресу електронної пошти, не розкриваючи вашу особу. Або ви можете захотіти
 зареєструйтеся на веб-сайт або веб-сервіс, але ви стурбовані тим, що веб-сайт надішле вам спам у майбутньому. <br />
 Для використання цієї служби ви повинні включити файли cookie і javascript, cookie-це просто для запису вашого ідентифікатора сеансу
 і мовних уподобань, ваша Конфіденційність поширюється на нашу політику конфіденційності та файлів cookie.
                 
                Будь ласка, натисніть на це посилання, щоб активувати свій обліковий запис:
             
            Дякуємо за реєстрацію!
 Ваш обліковий запис створено, ви можете увійти після активації облікового запису, натиснувши url
 нижче.
             Ключі API Про нас Токен доступу Адреса Всі електронні листи Всі права захищені. Все, що потрібно знати про тимчасову пошту Вже є обліковий запис? Під час спроби входу через обліковий запис соціальної мережі сталася помилка. Ви впевнені, що хочете видалити електронну пошту Ви впевнені, що хочете видалити цей маркер. Повернутися до списку Змінити Перевірте свою електронну пошту. Закрити Підтвердити пароль Зв'язатися з нами Політика щодо файлів cookie Політика щодо файлів cookie Копійовано! Копіювати Копіювання адреси електронної пошти з верхнього входу Створити Створити електронну пошту Дата Видалити Видалити електронну пошту Вилучити маркер Демо Вхідні Не отримали електронного листа? Одноразові email Домен У вас немає облікового запису? E-mail Активація пошти завершена Електронна пошта вже існує. Отримані листи Введіть адресу електронної пошти Сторінка записів Закінчується FAQ Відгуки Забули пароль? Часті запитання Від ЙТИ Генерувати Отримайте безкоштовну тимчасову пошту сьогодні! Отримайте тимчасову електронну пошту сьогодні Отримати мою адресу електронної пошти Перейти Привіт, я хотів би Як продовжити термін служби адреси електронної пошти? Як надати спільний доступ до тимчасової електронної пошти? Наскільки легко це було використовувати? Як перевірити отримані листи? Як видалити одноразовий email і отримати новий? Як відправити email? Як користуватися одноразовою тимчасовою електронною поштою? Як ним користуватися Я приймаю Вхідні Вхідні Поговоримо Завантаження тимчасової адреси електронної пошти Увійти Увійти Увійдіть до свого облікового запису Вихід з системи Більше старих записів Більше останніх записів Тепер ви можете увійти в свій аккаунт і почати користуватися сервісом. Наші контакти Наш надійний і безпечний сервіс дозволить вам отримати повністю анонімну і приватну
 адреса електронної пошти, яку ви
 можна використовувати відразу.
 Насправді ви можете створити власну тимчасову адресу електронної пошти та використовувати його, коли ви
 хочу. Наша надійна і безпечна служба дозволяє вам повністю анонімно і приватно отримати адресу електронної пошти, яку ви
 можна використовувати негайно.
 Насправді ви можете створити свою тимчасову адресу електронної пошти і використовувати її, коли захочете. Пароль Скидання пароля Будь ласка, перейдіть на наступну сторінку і виберіть новий пароль: Популярні статті Політика конфіденційності Проекти протестовані за допомогою фальшивих вхідних Сирий вміст Повторно відправити активаційний лист Читати вхідні повідомлення електронної пошти на цій сторінці в розділі «Вхідні» Оновити Реєстрація завершена Скинути пароль Посилання для скидання пароля буде надіслано на ваш email. Заощаджуйте свій час, ніколи не створюючи адресу електронної пошти. Сканувати QR-код за допомогою мобільного телефону Надіслати відгук Відправник Спільна папку «Вхідні Покажіть нам деяку любов, рекомендуючи нас улюбленій мережі чи спільноті! Зареєструватися Зареєструватися Підпишіться безкоштовно! Підписка Помилка входу в соціальну мережу Виявлено спам-листи Безкоштовний спам Почати зараз Припинити отримання сміттєвих листів і забути про кнопки «фільтри» папки «Вхідні» і «відписатися» Тема Розкажіть про нас Тимчасова пошта Служба тимчасової електронної пошти Правила та умови Умови використання Дякуємо за реєстрацію! Дякуємо за користування нашим сайтом! Команда %(site_name)s Обліковий запис, який ви намагалися активувати, недійсний. Обліковий запис, який ви намагалися активувати, вже активовано. Наданий вами ключ активації є недійсним. Введена вами адреса електронної пошти була помилкою або помилкою. (Буває з кращими з нас.) Електронна пошта знаходиться в папці зі спамом. (Іноді там пропадають речі.) Термін дії цього облікового запису минув. Ця тенденція доводить, що, використовуючи посилання, оператори шкідливих кампаній отримують більше кліків і інфекцій в порівнянні з класичною технікою приєднання файлів до електронних листів. Очевидно, що користувачі стали недовірливими до будь-яких вкладень в пошті. Використовуйте це для реєстрації на веб-сайтах, в соціальних мережах тощо Корисні посилання Переглянути Очікування вхідних листів Ми націлені на те, щоб бути сфокусованими на збереженні вас в безпеці в Інтернеті. Ми дуже цінуємо це, дякую! Що таке одноразова тимчасова електронна пошта Хто ви, і чим ми можемо допомогти? Навіщо потрібна тимчасова пошта? Чи рекомендували б ви наш сервіс? На даний момент у вас немає жетонів. Ви не хочете ділитися особисто електронною адресою? Ти втомився від
 спам? Ви не хочете надавати свою персональну адресу електронної пошти? Ви втомилися від спаму? Ви отримуєте це повідомлення електронної пошти, оскільки ви запитали скидання пароля для свого облікового запису користувача на сторінці
 %(site_name)s. Ваш Ваша безкоштовна тимчасова електронна пошта Ваші друзі та колеги можуть отримувати миттєвий перегляд вхідної електронної пошти. Ваша коробка «Вхідні» порожня Ваше ім'я користувача, якщо ви забули: тому мільярди поштових скриньок код покликаний вирішити всі ці проблеми. експерти підрахували, що 87%% всього спаму електронної пошти, надісланого у другому кварталі 2020 року (квітень, травень і червень місяці), містять посилання на скачування шкідливих файлів, але не вкладення з самими файлами. безкоштовний моніторинг сайту - це адреса, яка видається на короткий час: від декількох хвилин до цілого місяця, а потім він буде видалений з усіма вхідними листами і вкладеннями, або просто недоступний для подальшого використання. спеціально розроблена, щоб допомогти вам забути про спам, рекламних розсилках, зломах і атакуючих роботів. Зберігайте справжню поштову скриньку в чистоті надає тимчасову, захищену, анонімну, безкоштовну, одноразову адресу електронної пошти. підписатися 