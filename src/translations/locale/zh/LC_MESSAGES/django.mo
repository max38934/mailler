��    �      l  �   �
      H  �   I  x   �  �   E  �   �  �  j  :    �   =  �     }     }   �  �     c   �  �   .  �   �  �   �  �   �  L  �  .  �  �  #"  N   �&  �   >'     �'     �'     (     (  
   (     '(  )   <(     f(  L   (  %   �(  +   �(     )     +)     2)     D)     J)  
   [)     f)     t)     �)     �)  %   �)     �)     �)     �)     �)     �)     �)  
   �)     �)     *     #*     **     A*     G*     b*     x*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*  $   �*      +     ?+     T+     W+  2   q+     �+     �+     �+  3   �+     0,  &   C,     j,     x,     �,     �,  
   �,  $   �,     �,     �,     �,     �,     �,     �,  <   -     O-  #  \-  �   �.     t/     }/  :   �/     �/     �/  !   �/     	0     0  6   .0     e0     m0     �0  /   �0  :   �0     �0     1     *1     11  K   >1     �1     �1     �1     �1     �1     �1  	   �1  	   �1  V   �1     P2     X2     f2     v2     �2     �2     �2     �2     �2  1   �2  =   +3  +   i3  Q   �3  G   �3     /4  �   I4  2   =5     p5     }5     �5  A   �5      �5  "   6  !   $6     F6      d6  $   �6  g   �6  S   7  r   f7     �7     �7  C   �7     <8  *   P8     {8     8     �8  %   �8  �   �8     �9  �   �9  �   c:  F   ;     K;    R;  �   j<  t   �<  �   c=  �   �=    �>  |  �?  �   A  �   �A  �   �B  �   C  �   �C  i   HD  �   �D  �   cE  �   �E  �   �F  �   |G  �  0H  �  -L  E   P  z   cP  
   �P     �P     �P     Q     
Q     Q  *   -Q     XQ  0   kQ  !   �Q  !   �Q     �Q     �Q     �Q     R     R     $R     1R     ?R     LR     YR  '   `R     �R     �R     �R     �R     �R     �R     �R     �R     �R     �R     S     S     S     .S     GS     WS     jS     {S     �S     �S     �S     �S     �S     �S     �S  *   �S     �S     T     %T     )T  0   9T  $   jT  !   �T  $   �T  <   �T     U  *   /U     ZU  	   jU  	   tU  	   ~U     �U     �U     �U     �U     �U     �U     �U     �U  <   V     HV  �   XV  �   3W     �W     X  *   X     :X     GX  !   TX     vX     �X  <   �X     �X     �X     �X  6   �X  <   1Y     nY     �Y  	   �Y     �Y  K   �Y     �Y     Z     Z     Z     Z     8Z     NZ     ^Z  X   kZ     �Z     �Z     �Z     �Z     �Z     [     [  !   ([     J[  !   _[  $   �[  !   �[  ]   �[  W   &\     ~\  �   �\  3   |]     �]     �]     �]  ?   �]  !   #^     E^  $   d^  !   �^  !   �^     �^  M   �^  W   4_  Z   �_     �_     �_  ?   `     G`  $   ``     �`     �`     �`  !   �`  �   �`     {a  �   �a  �   +b  H   �b     �b     9   /   ,   \   �          h      l       �      �          ^       =      �   u   �       �       $      �   Z   �   �   w   �   U   k   �   7   T   3          6           F   �      f   &   �   O   �   �   �   �   �   E   !   c          �   ~   ]           j   J       8       o   �               0   �   D   �   �           5      B   �   
   |          -   4   <   �   a   �   �      g   {   �   R   p   r           A   >   �   I      Y   _   (   �   t   v           m   e   n          +   2   :   M           X   �   �   Q   �       W          �   `       "   s   [                  �              z           .          %       L              q   @       S   d       }   V   '   �                   P             H               y   �   �              �           ?   G          �       x   i       �   N   #      *   1   K   C      	   b       �       )       �   ;    
                                            100%% Private mailboxes with full ownership!
                                         
                                            Generate fake email with one click
                                         
                                            Hide yourself from spam with enhanced privacy and security
                                         
                                        Click "Delete" on the home page and you will get new temporary email.
                                     
                                        Email address is valid until you delete it or until the service will delete it.
                                        The lifetime depends on many different factors.
                                        You can have temporary emails with lifetime up to 1 YEAR with registered
                                        account.
                                     
                                        Many apps and websites obligate you to give personal email address for no reason
                                        other than to collect personal information about you.
                                        Temporary email address allows you to separate your personal email address
                                        you use for banking, credit card purchases and other personal identifying
                                        transactions from all the other sites out there.
                                     
                                        Sending email is completely disabled and
                                        it will never be implemented due to fraud and spam issues.
                                     
                                        They are displayed under your disposable email.
                                        If you did not get incoming emails for too long - click "Refresh" button.
                                     
                                        You can share it by using QR code on main page.
                                     
                                    You can have maximum %(max_tokens_allowed)s tokens.
                                     
                                We can’t deliver the email to this address. (Usually because of corporate firewalls
                                or filtering.)
                                 
                                Why do I need %(BRAND_NAME)s account?
                             
                                You accidentally gave us another email address. (Usually a work or personal one
                                instead of the one you meant.)
                                 
                            If you don’t see an email from us within a few minutes, a few
                            things could have happened:
                             
                            We’ve emailed you instructions for setting your password, if an account
                            exists with the email you entered. You
                            should receive them shortly.
                         
                        If you don’t receive an email, please make sure you’ve entered the address you
                        registered
                        with, and check your
                        spam folder.
                         
                        UptimeMetrics platform is designed to monitor the status of the websites. It includes such
                        features: Website’s availability,
                        Server availability,
                        Validity check of website’s SSL certificate and HTML validation.
                     
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see
                    the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an
                    email
                    address has expired, the email address and any received emails will be gone. A new email address
                    will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in
                    the future. <br/>
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session
                    id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                    %(BRAND_NAME)s is a disposable email address service. When you visit %(BRAND_NAME)s a new email
                    address is generated just for you. The generated email address can immediately receive any emails.
                    Any email that is received will show up on the main page immediately. No one other than you will see the emails
                    that are received. It is important to note that an email address expires after 24 hours. When an email
                    address has expired, the email address and any received emails will be gone. A new email address will
                    be generated upon visiting this website. There are many reasons for using a disposable email.
                    You may want to give someone an email address without revealing your identity. Or you may want to
                    sign up for a website or web service but you are concerned that the website will send you spam in the future. <br />
                    For using this service you MUST enable cookie and javascript, cookie is just to record your session id
                    and language preference, your Privacy is covered under our Privacy and Cookie policy.
                 
                Please click this link to activate your account:
             
            Thanks for signing up!
            Your account has been created, you can login after you have activated your account by pressing the url
            below.
             API keys About Us Access Token Address All Emails All Rights Reserved. All you need to know about temporary mail Already have an account? An error occurred while attempting to login via your social network account. Are you sure you want to delete email Are you sure you want to delete this token. Back to list Change Check your email. Close Confirm password Contact Us Cookie Policy Cookies Policy Copied! Copy Copy email address from the top input Create Create Email Date Delete Delete Email Delete Token Demo Inbox Didn’t get an email? Disposable email Domain Don't have an account? Email Email Activation Completed Email Already exists. Emails received Enter Email Address Entries page Expires FAQ Feedback Forgot password? Frequently Asked Questions From GO Generate Get Your Free Temporary Email Today! Get Your Temporary Email Today Get my email address Go Hi there, I would like to How do I extend the lifetime of the email address? How do I share temporary email? How easy was this to use? How to check received emails? How to delete disposable email and get the new one? How to send email? How to use disposable temporary email? How to use it I accept the Inbox Inboxes Let's Talk Loading your temporary email address Log In Login Login to your account Logout More old entries More recent entries Now you can login into your account and start using service. Our Contacts Our robust and safe service allow you to get completely anonymous and private an
                    email address which you
                    can use immediately.
                    Actually you can create you own temporary email address and use it whenever you
                    want. Our robust and safe service allow you to get completely anonymous and private an email address which you
                can use immediately.
                Actually you can create you own temporary email address and use it whenever you want. Password Password Reset Please go to the following page and choose a new password: Popular Articles Privacy Policy Projects tested with fake inboxes Raw Content Re-send activation email Read incoming emails on this page inside inbox section Refresh Registration Complete Reset Password Reset password link will be sent on your email. Save your time by never having to create an email address. Scan QR code with mobile phone Send Feedback Sender Shared Inbox Show us some love by recommending us to your favorite network or community! Sign Up Sign up Sign up for free! Signup Social Network Login Failure Spam Emails Detected Spam Free Start Now Stop receiving trash emails and forget about inbox "filters" and "unsubscribe" buttons Subject Tell about us Temporary Email Temporary Email Service Terms and Conditions Terms of Use Thanks for signing up! Thanks for using our site! The %(site_name)s team The account you attempted to activate is invalid. The account you tried to activate has already been activated. The activation key you provided is invalid. The email address you entered had a mistake or typo. (Happens to the best of us.) The email is in your spam folder. (Sometimes things get lost in there.) This account has expired. This trend proves that by using links, operators of malicious campaigns receive more clicks and infections compared to the classic technique of attaching files to emails. Obviously, users have become distrustful of any attachments in the mail. Use this to sign up on websites, social media, etc Useful Links View Waiting for incoming emails We aim high at being focused on keeping you safe on the internet. We really appreciate it, thanks! What is disposable temporary email Who are you, and how can we help? Why do I need temporary mail? Would you recommend our service? You don't have tokens at the moment. You don't want to share your personally email address? Are you tired from the
                    spam? You don't want to share your personally email address? Are you tired from the spam? You're receiving this email because you requested a password reset for your user account at
        %(site_name)s. Your Your Free Temporary Email Your friends and colleagues can get instant view of incoming email. Your inbox is empty Your username, in case you’ve forgotten: ago billions of inboxes code designed to solve all these problems. experts calculated that 87%% of all email spam sent in the second quarter of 2020 (April, May and June months) contained links to download malicious files, but not attachments with the files themselves. free website monitoring is an address that is issued for a short time: from a few minutes to a whole month, and then it will be deleted with all incoming letters and attachments, or simply unavailable for further use. platform is specifically designed to help you to forget about spam, advertising mailings, hacking and attacking robots. Keep your real mailbox clean and secure. provides temporary, secure, anonymous, free, disposable email address. signup Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
                                            100%% 拥有完全所有权的私人邮箱！
                                         
                                            一键生成虚假电子邮件
                                         
                                            增强隐私和安全性，让自己远离垃圾邮件
                                         
                                        点击主页上的 “删除”，你将收到新的临时电子邮件。
                                     
                                        电子邮件地址在您删除或服务将其删除之前有效。
 寿命取决于许多不同的因素。
 注册后，您可以拥有终身长达 1 年的临时电子邮件
 账户。
                                     
                                        许多应用程序和网站都要求你毫无理由地提供个人电子邮件地址
 除了收集关于你的个人信息之外。
 临时电子邮件地址允许您分开个人电子邮件地址
 你用于银行业务、信用卡购买和其他个人身份
 来自外面所有其他网站的交易。
                                     
                                        发送电子邮件已完全禁用，
 由于欺诈和垃圾邮件问题，它永远不会实施。
                                     
                                        它们显示在您的一次性电子邮件下。
 如果你没有收到传入的电子邮件太长时间-点击 “刷新” 按钮。
                                     
                                        你可以通过在主页上使用二维码来分享它。
                                     
                                    您最多可以拥有 %(max_tokens_allowed)s 个令牌。
                                     
                                我们无法将电子邮件发送到这个地址。（通常是因为公司防火墙
 或过滤。）
                                 
                                为什么我需要 %(BRAND_NAME)s 账户？
                             
                                你不小心给我们另一个电子邮件地址（通常是工作或个人工作
 而不是你的意思。）
                                 
                            如果你在几分钟内没有看到我们的电子邮件，
 事情可能会发生：
                             
                            我们已通过电子邮件向您发送设置密码的说明（如果有帐户）
 与您输入的电子邮件一起存在。你
 应该很快收到它们。
                         
                        如果你没有收到电子邮件，请确保你已经输入了你的地址
 已注册
 使用，然后检查
 垃圾文件夹。
                         
 uptimeMetrics 平台旨在监控网站的状态。它包括这样
 特点：网站的可用性，
 服务器可用性，
 网站 SSL 证书的有效性检查和 HTML 验证。
  
                    %(BRAND_NAME)s 是一次性电子邮件地址服务。当您访问 %(BRAND_NAME)s 一封新电子邮件时
 地址是专门为你生成的。生成的电子邮件地址可以立即接收任何电子邮件。
 收到的任何电子邮件都将立即显示在主页上。除了你以外，没有人会看见
 这些电子邮件
 已收到。请务必注意，电子邮件地址将在24小时后过期。当
 电子邮件
 地址已过期，电子邮件地址和所有收到的电子邮件都将消失。一个新的电子邮件地址
 将
 在访问本网站时生成。使用一次性电子邮件的原因有很多。
 你可能想给别人一个电子邮件地址而不透露你的身份。或者你可能想
 注册网站或网络服务，但您担心该网站会向您发送垃圾邮件
 未来。<br/>
 要使用此服务，你必须启用 cookie 和 javascript，cookie 只是为了记录你的会话
 id
 和语言偏好，我们的隐私和Cookie政策涵盖了您的隐私。
                 
                    %(BRAND_NAME)s 是一次性电子邮件地址服务。当你访问 %(BRAND_NAME)s 一封新电子邮件
 地址是专为你生成的。生成的电子邮件地址可以立即接收任何邮件。
 收到的任何电子邮件都将立即显示在主页上。除了你之外没有人会看到电子邮件
 已收到。值得注意的是，电子邮件地址在 24 小时后过期。当电子邮件
 地址已过期，电子邮件地址和所有收到的电子邮件都将消失。新的电子邮件地址将
 是在访问本网站时生成的。使用一次性电子邮件有很多原因。
 您可能希望在不透露身份的情况下为某人提供电子邮件地址。或者你可能想
 注册网站或网络服务，但您担心该网站将来会向您发送垃圾邮件。<br />
 要使用此服务，你必须启用 cookie 和 JavaScript，cookie 只是为了记录你的会话 ID
 以及语言偏好，您的隐私受我们的隐私和 Cookie 政策的保护。
                 
                请点击此链接激活您的账户：
             
            谢谢你注册！
 您的账户已创建，您可以在激活帐户后按 url 登录
 下面。
             API 密钥 关于我们 访问令牌 地址 所有邮件 保留所有权利。 所有你需要知道的关于临时邮件 已经有账户？ 尝试通过社交网络帐户登录时出错。 你确定要删除电子邮件吗 是否确实要删除此令牌。 返回列表 更改 检查你的电子邮件。 关闭 确认密码 联系我们 Cookie 政策 饼干政策 复制了！ 复制 从顶部输入复制电子邮件地址 创建 创建邮件 日期 删除 删除邮件 删除令牌 演示收件箱 没收到电子邮件？ 一次性电邮 域 没有账户？ 电邮 邮件激活已完成 电子邮件已存在。 收到的邮件 输入电子邮箱 “条目” 页 过期 FAQ 反馈 忘记密码？ 常见问题 来自 走 生成 立即获取免费的临时电子邮件！ 立即获取临时电子邮件 获取我的电子邮件地址 去 你好，我想 如何延长电子邮件地址的生命周期？ 我如何共享临时电子邮件？ 这个使用起来有多容易？ 如何查看收到的电子邮件？ 如何删除一次性电子邮件并获得新电子邮件？ 如何发送电子邮件？ 如何使用一次性临时电子邮件？ 如何使用它 我接受 收件箱 收件箱 让我们来谈谈 加载临时电子邮件地址 登录 登录 登录到你的账户 注销 更多旧条目 更多最近的条目 现在你可以登录你的帐户并开始使用服务了。 我们的联系 我们强大而安全的服务使您可以完全匿名和私密地获得
 您的电子邮件地址
 可以立即使用。
 实际上，你可以创建自己的临时电子邮件地址，随时随地使用它
 想要。 我们强大而安全的服务使您可以获得完全匿名和私密的电子邮件地址
 可以立即使用。
 实际上，你可以创建自己的临时电子邮件地址，并随时使用它。 密码 密码重置 请转到以下页面并选择新密码： 热门文章 隐私政策 使用假收件箱测试的项目 原始内容 重新发送激活邮件 在收件箱部分阅读此页面上的收到的电子邮件 刷新 注册完成 重置密码 重置密码链接将在您的电子邮件中发送。 无需创建电子邮件地址，从而节省您的时间。 用手机扫描二维码 发送反馈 发件人 共享收件箱 向我们推荐你最喜欢的网络或社区，向我们展示一些爱！ 注册 注册 免费注册！ 注册 社交网络登录失败 检测到垃圾邮件 无垃圾邮件 现在开始 停止接收垃圾邮件，忘记收件箱 “过滤器” 和 “取消订阅” 按钮 主题 说说我们 临时邮件 临时邮件服务 条款和条件 使用条款 谢谢你注册！ 感谢您使用我们的网站！ %(site_name)s 团队 您尝试激活的帐户无效。 您尝试激活的帐户已激活。 您提供的激活密钥无效。 您输入的电子邮件地址有错误或错字。（发生在我们最好的人身上。） 电子邮件位于垃圾邮件文件夹中。（有时候东西会迷失在里面。） 此账户已过期。 这种趋势证明，与将文件附加到电子邮件的传统技术相比，通过使用链接，恶意营销活动的运营商可以获得更多的点击和感染。显然，用户对邮件中的任何附件都变得不信任。 使用此功能在网站、社交媒体等上注册 有用的链接 查看 等待传入的电子邮件 我们的目标是专注于确保你在互联网上的安全。 我们真的很感激，谢谢！ 什么是一次性临时邮件 你是谁，我们该如何帮助？ 为什么我需要临时邮件？ 你会推荐我们的服务吗？ 你目前没有代币。 你不想分享你的个人电子邮件地址？你累了吗
 垃圾邮件？ 你不想分享你的个人电子邮件地址？你对垃圾邮件感到厌倦了吗？ 您收到此电子邮件是因为您请求重置用户帐户的密码：
 %(site_name)s。 你的 免费临时电子邮件 您的朋友和同事可以即时查看收到的电子邮件。 你的收件箱是空的 如果你忘记了你的用户名： 以前 数十亿个收件箱 代码 旨在解决所有这些问题。 专家计算，2020 年第二季度（4 月、5 月和 6 月）发送的所有垃圾邮件中有 87％ 包含下载恶意文件的链接，但不包含文件本身的附件。 免费网站监控 是一个短时间发布的地址：从几分钟到整个月，然后将与所有收到的信件和附件一起删除，或者根本无法进一步使用。 平台专为帮助您忘记垃圾邮件、广告邮件、黑客攻击和攻击机器人而设计。保持真实的邮箱清洁和安全。 提供临时、安全、匿名、免费的一次性电子邮件地址。 注册 