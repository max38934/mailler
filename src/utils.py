from django.conf import settings


def get_site_context(site):
    domain = site.domain
    c = {
        'domain': domain,
        'CONTACT_EMAIL': settings.CONTACT_EMAIL,  # TODO add multisite
        'ADDRESS': settings.ADDRESS,
        'BRAND_NAME': settings.BRAND_NAME,
        'site': site,
    }
    return c
